package com.cloudone.hotels;

/**
 * Created by Akala G56 on 31/03/14.
 */
import java.io.File;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
public class ConnectionString {

  private static String _datasource;
  private static String _sqluid;
  private static String _sqlpwd;
  private static String _dbdriver;
  private static ConnectionString Instance= new ConnectionString();
  private static File AppConfig=null;

  public static ConnectionString createInstance()
  {
        try
        {
            AppConfig = new File("AppConfig.xml");
            if(AppConfig!=null)
            {
              DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
              DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
              Document doc = dBuilder.parse(AppConfig);
              doc.getDocumentElement().normalize();
              NodeList nList = doc.getElementsByTagName("appSettings");
                for (int temp = 0; temp < nList.getLength(); temp++)
                {
                    Node nNode = nList.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE)
                    {
                        Element eElement = (Element) nNode;
                        setDbDriver(eElement.getElementsByTagName("dbdriver").item(0).getTextContent());
                        setSequelUserId(eElement.getElementsByTagName("sqluid").item(0).getTextContent());
                        setSequelPwd(eElement.getElementsByTagName("sqlpwd").item(0).getTextContent());
                    }
                }
                return Instance;
            }
            else {}

        }
        catch(Exception exp){java.lang.System.out.println("Error occured:"+exp.getMessage());}
        return null;
    }
    public static void setDbDriver(String dbdriver){
       _dbdriver=dbdriver;
    }
    public  String getDbDriver(){
      return _dbdriver;
    }
    public static void setDataSource(String datasource){
        _datasource=datasource;
    }
    public  String getDataSource(){
        return _datasource;
    }
    private static void setSequelUserId(String sqluid){
        _sqluid=sqluid;
        
    }
    public  String  getSequelUserId(){
        return _sqluid;
    }
    private static void setSequelPwd(String sqlpwd){
        _sqlpwd=sqlpwd;
    }
    public String getSequelPwd(){
        return _sqlpwd;
    }
    
}

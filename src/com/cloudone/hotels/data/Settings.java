/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;

@DatabaseTable(tableName="Settings")
public class Settings {
   @DatabaseField
   public String _version;
   @DatabaseField(dataType=DataType.DATE)
   public Date _expiration_date;
   @DatabaseField
   public String _expiration_status;
   @DatabaseField(dataType=DataType.BYTE_ARRAY)
   public byte[] _company_logo;
   @DatabaseField
   public String _company_name;
   @DatabaseField
   public String _company_website;
   @DatabaseField
   public String _renew_url;
   @DatabaseField
   public String _renew_message;
   
   public Settings(){ }
   public void setVersion(String version){
    _version=version;
   } 
   public String getVersion(){
    return _version;
   }
   public void setExpirationDate(Date expiration_date){
     _expiration_date=expiration_date; 
   }
   public Date getExpirationDate(){
     return _expiration_date;
   }
   public void setExpirationStatus(String expiration_status){
      _expiration_status=expiration_status;
   }
   public String getExxpirationStatus(){
     return _expiration_status;
   }
   public void setCompanyLogo(byte[] Image){
     this._company_logo=Image;
   }
   public byte[] getCompanyLogo(){
     return _company_logo;
   }
   public void setNewUrl(String renew_url){
     _renew_url=renew_url;
   }
   public String getNewUrl(){
    return _renew_url;
   }
   public void setMessage(String renew_message){
     _renew_message=renew_message;
   }
   public String getMessage(){
     return _renew_message;
   }
   
}


















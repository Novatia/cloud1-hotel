/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName ="Room")
public class Room {
   @DatabaseField (generatedId =true)
   private int _id;
   @DatabaseField
   public String _name;
   @DatabaseField 
   public boolean _status;
   @DatabaseField (foreign=true)
   public RoomTypes _room_type;
   public Room(){}
   public int getId(){return _id;}
   public void setName(String name){
     _name=name;
   }
   public String getName(){
     return _name;
   }
   public void setStatus(boolean status){
     _status = status;
   }
   public boolean getStatus(){
     return _status;
   }
   public void setRoomType(RoomTypes room_type){
     _room_type=room_type;
   }
   public RoomTypes getRoomType(){
     return _room_type;
   }

}

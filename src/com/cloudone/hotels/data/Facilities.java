/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="facilities")
public class Facilities {
 @DatabaseField(generatedId=true)   
 private int _id;
 @DatabaseField
 public String _name;
 
 public Facilities(){}
 public int getId(){
  return _id;
 }
 public void setName(String name){
  _name=name;
 }
 public String getName(){
  return _name;
 }
   
}

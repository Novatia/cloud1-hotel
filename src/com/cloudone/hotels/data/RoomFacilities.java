/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="roomfacilities") 
public class RoomFacilities {
    @DatabaseField (generatedId=true)
    int _id;
    @DatabaseField(foreign=true)
    Room _room;
    @DatabaseField(foreign=true)
    Facilities _facility;
    
    public RoomFacilities(){}
    public int getId()
    {
      return _id;
    }
    public void setRoom(Room room){
     _room=room;
    }
    public Room getRoom(){
     return _room;
    }
    public void setFacility(Facilities facility){
     _facility=facility;
    }
    public Facilities getFacility(){
     return _facility;
    }
            
}

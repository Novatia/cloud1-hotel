
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;
@DatabaseTable(tableName="RoomReservation")
public class RoomReservation {
    @DatabaseField(generatedId=true)
    private int _id;
    @DatabaseField(foreign=true)
    public Guests _guest;
    @DatabaseField(foreign=true)
    public Room _room;
    @DatabaseField(dataType=DataType.DATE)
    public Date _reserved_date;
    @DatabaseField
    public int _num_nights;
    @DatabaseField
    public int _num_guests;
    @DatabaseField
    public int _total_price;
    @DatabaseField(dataType= DataType.DATE)
    public Date _checkedIn;
    @DatabaseField(dataType= DataType.DATE)
    public Date _checkedOut;
    @DatabaseField
    public String _status;
    
    
    public RoomReservation(){}
    public int getId(){return _id;}
    public void setGuest(Guests guest){
      _guest=guest;
    }
    public Guests getGuest(){
      return _guest;
    }
    public void setRoom(Room room){
      _room=room;
    }
    public Room getRoom(){
      return _room;
    }
    public void setReservationDate(Date reserved_date){
      _reserved_date=reserved_date;
    }
    public Date getReservationDate(){
      return _reserved_date;
    }
    public void setNights(int num_nights){
      _num_nights=num_nights;
    }
    public int getNights(){
      return _num_nights;
    }
    public void setTotalPrice(int total_price){
     _total_price=total_price;
    }
    public int getTotalPrice(){
      return _total_price;
    }
    public void setCheckInDate(Date checkedIn){
     _checkedIn=checkedIn;
    }
    public Date getCheckInDate(){
      return _checkedIn;
    }
    public void setCheckOutDate(Date checkedout){
     _checkedOut=checkedout;
    }
    public Date getCheckOutDate(){
      return _checkedOut;
    }
    public void setStatus(String status)
    {
      _status=status;
    }
    public String getStatus()
    {
      return _status;
    }
}


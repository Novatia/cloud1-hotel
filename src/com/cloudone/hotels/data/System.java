package com.cloudone.hotels.data;

/**
 * Created by Akala G56 on 04/04/14.
 */
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;
@DatabaseTable(tableName ="System")
public class System {

  @DatabaseField (generatedId =true)
  private int _id;
  @DatabaseField
  public String _name;
  @DatabaseField
  public boolean _state;
  @DatabaseField(dataType= DataType.DATE)
  public Date _logindate;
  @DatabaseField(dataType=DataType.DATE)
  public Date _logoutdate;

  public System(){}
  public int getId(){return _id;}
  public void setIsFirstLogon(boolean state){_state=state;}
  public boolean getIsFirstLogon(){return _state;}
  public void setSystemLogonDate(Date date){_logindate=date;}
  public Date getSystemLogonDate(){return _logindate;}
  public void setSystemLogoutDate(Date date){_logoutdate=date;}
  public Date getSystemLogoutDate(){return _logoutdate;}

}

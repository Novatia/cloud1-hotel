/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;


@DatabaseTable(tableName="Future_Reservation")
public class FutureReservation {
      @DatabaseField(dataType=DataType.DATE)
      public Date _date;
      @DatabaseField
      public String _guest_name;
      @DatabaseField(dataType=DataType.DATE)
      public Date _checkIn;
      @DatabaseField(dataType=DataType.DATE)
      public Date _checkOut;
      @DatabaseField
      public String _agency;
      @DatabaseField
      public String _status;
  
       public FutureReservation(){}
       public void setDate(Date date){
        _date=date;
       }
       public Date getDate(){
        return _date;
       }
       public void setGuestName(String guest_name){
        _guest_name =guest_name;
       }
       public String getGuestName(){
        return _guest_name;
       }

       public void setCheckIn(Date checkIn){
        _checkIn=checkIn;
       }
       public Date getCheckIn(){
        return _checkIn;
       }
       public void setCheckOut(Date checkOut){
        _checkOut=checkOut;
       }
       public Date getCheckOut(){
        return _checkOut;
       }
       public void setAgency(String agency){
        _agency=agency;
       }
       public String getAgency(){
        return _agency;
       }
       public void setStatus(String status){
         _status=status;
       }
       public String getStatus(){
         return _status;
       }
}

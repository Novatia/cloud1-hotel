/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName ="RoomTypes")
public class RoomTypes {
   @DatabaseField (generatedId =true)
   public int _id;
   @DatabaseField 
   public String _name;
   @DatabaseField 
   public int _price;
   @DatabaseField 
   public int _max_guest_no;
   
   public RoomTypes(){}
   public int getId(){return _id;}
   public void setName(String name ){
   _name =name;
   }
   public String getName(){
    return _name;
   }
   public void setPrice(int price){
     _price=price;
   }
   public int getPrice(){
     return _price;
   }
   public void setMaxGuestNumber(int max_guest_no){
     _max_guest_no=max_guest_no;
   }
   public int getMaxGuestNumber(){
     return _max_guest_no;
   }
}

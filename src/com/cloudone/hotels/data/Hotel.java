/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName ="Hotel")
public class Hotel {
    @DatabaseField(generatedId = true)
    public int hotel_id;
    @DatabaseField
    public String _name;
    @DatabaseField
    public String _city;
    @DatabaseField
    public String _country;
    @DatabaseField
    public String _currency;
    @DatabaseField
    public String _currency_symbol;
    @DatabaseField
    public String _currency_abbr;
    @DatabaseField
    public String _phone;
    @DatabaseField
    public String _email;
    
    public Hotel()
    {
    
    }
    public int getId(){
     return hotel_id;
    }
    public void setName(String name) {
     _name=name;
    }
    public String getName(){
      return _name;
    }
    public void setCountry(String country){
     _country=country;
    }
    public String getCountry(){
      return _country;
    }
    public void setPhone(String phone){
      _phone=phone;
    }
    public String getPhone(){
     return _phone;
    }
    public void setCity(String city){
      _city=city;
    }
    public String getCity(){
      return _city;
    }
    public void setCurrency(String currency){
      _currency=currency;
    }
    public String getCurrency(){
      return _currency;
    }
    public void setCurrencySymbol(String currency_symbol){
       _currency_symbol=currency_symbol;
    }
    public String getCurrencySymboml(){
     return _currency_symbol;
    }
    public void setCurrencyAbbr(String currency_abbr){
      _currency_abbr=currency_abbr;
    }
    public String getCurrrencyAbbr(){
      return _currency_abbr;
    }
    public void setEmail(String email){
      _email=email;
    }
    public String getEmail(){
      return _email;
    }
    
}

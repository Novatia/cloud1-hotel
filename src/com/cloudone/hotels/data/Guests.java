/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;

@DatabaseTable(tableName="Guests")
public class Guests {
  @DatabaseField(generatedId =true)
  private int _id;
  @DatabaseField
  public String _firstname;
  @DatabaseField
  public String _lastname;
  @DatabaseField
  public String _address_line1;
  @DatabaseField
  public String _address_line2;
  @DatabaseField
  public String _phone1;
  @DatabaseField
  public String _phone2;
  @DatabaseField
  public String _city;
  @DatabaseField
  public String _country;
  @DatabaseField(dataType=DataType.DATE)
  public Date _dob;
  @DatabaseField
  public String _id_type;
  @DatabaseField
  public String _id_number;
  @DatabaseField
  public String _email;
  
  public String _idType[]={"passport","drivers","licenses","id card"};
  
  public Guests(){}
  public int getId(){return _id;}
  public void setFirstName(String firstname){ 
    _firstname=firstname;
  }
  public String getFirstName(){
    return _firstname;
  }
  public void setLastName(String lastname){
   _lastname=lastname;
  }
  public String getLastName(){
   return _lastname;
  }
  public void setAddressLine1(String address_line1){
   _address_line1=address_line1;
  }
  public String getAddressLine1(){
    return _address_line1;
  }
  public void setAddressLine2(String address_line2){
   _address_line2=address_line2;
  }
  public String getAddressLine2(){
    return _address_line2;
  }
  public void setPhone1(String phone1){
   _phone1=phone1;
  }       
  public String getPhone1(){
   return _phone1;
  }
  public void setPhone2(String phone2){
   _phone2=phone2;
  }       
  public String getPhone2(){
   return _phone2;
  }
  public void setCity(String city){
    _city=city;
  }
  public String getCity(){
    return _city;
  }
  public void setCountry(String country){
    _country=country;
  }
  public String getCountry(){
    return _country;
  }
  public void setDateOfBirth(Date dob){
    _dob=dob;
  }
  public Date getDate(){
    return _dob;
  }
  public void setIdentityType(String id_type){
   _id_type=id_type;
  }
  public String getIdentityType(){
    return _id_type;
  }
  public void setIdentityNo(String id_number){
   _id_number=id_number;
  }
  public String getIdentityNo(){
    return _id_number;
  }
  public void setEmail(String email){
   _email=email;
  }
  public String getEmail(){
   return _email;
  }
}

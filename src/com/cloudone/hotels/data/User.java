/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.data;

/**
 *
 * @author Akala G56
 */
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName ="User")
public class User {
    @DatabaseField(generatedId=true)
    public int _id;
    @DatabaseField
    public String _username;
    @DatabaseField
    public int _password;
    @DatabaseField
    public String _firstname;
    @DatabaseField
    public String _lastname;
    @DatabaseField
    public String _role;
    
    public User(){}
    public int getId(){
     return _id;
    }
    public void setUsername(String username){
    _username= username;
    }
    public String getUsername(){
     return _username;
    }
    public void setPassword(int password){
      _password=password;
    }
    public int getPassword(){
      return _password;
    }
    public void setFirstName(String firstname){
      _firstname=firstname;
    }
    public String getFirstName(){
      return _firstname;
    }
    
    public void setLastName(String lastname){
      _lastname=lastname;
    }
    public String getLastName(){
      return _lastname;
    }
    public void setRole(String role){
      _role=role;
    }
    public String  getRole(){
      return _role;
    }
}

package com.cloudone.hotels.exceptions;

/**
 * Created by Akala G56 on 03/04/14.
 */
public class ForeignKeyConstraintException extends RuntimeException{
    public ForeignKeyConstraintException(String message){
        super(message);
    }
}

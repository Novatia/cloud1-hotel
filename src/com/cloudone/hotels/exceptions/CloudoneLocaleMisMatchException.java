/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.exceptions;

/**
 *
 * @author Akala G56
 */
public class CloudoneLocaleMisMatchException extends RuntimeException{
     public CloudoneLocaleMisMatchException(String message){
        super(message);
    }  
}

package com.cloudone.hotels.exceptions;

/**
 * Created by Akala G56 on 01/04/14.
 */
public class CloudoneWatsonException extends RuntimeException{
    public CloudoneWatsonException(String message){
        super(message);
    }
}

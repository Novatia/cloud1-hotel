/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.exceptions;

/**
 *
 * @author Akala G56
 */
public class ReservationDateMatchException extends RuntimeException{
     public ReservationDateMatchException(String message){
        super(message);
        com.cloudone.hotels.commons.Logging.logToFile(this.getClass().getCanonicalName()+" "+message);
    }
}
   


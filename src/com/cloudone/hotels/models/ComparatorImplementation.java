/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.models;

/**
 *
 * @author Akala G56
 */
import com.cloudone.hotels.interfaces.Comparator;
import com.cloudone.hotels.interfaces.Observer;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.Color;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.TableColumnModel;
public class ComparatorImplementation  implements Comparator,Runnable,Observer{
    public String state;
    String[] roomType;
    private TableColumnModel columnmodel;  
    public ComparatorImplementation(TableColumnModel tablemodel )
    {
        
      columnmodel=tablemodel;
      
      List<com.cloudone.hotels.data.RoomTypes> list=this.getAllRoomType();
      roomType= new String[list.size()];
      for(int i=0;i<list.size();i++)
      {
        roomType[i]=list.get(i).getName();
      }
      com.cloudone.hotels.commons.TableStateEvent.subscribe(this);
    }
    @Override
    public boolean shouldHighlight(JTable tbl, Object value, int row, int column) {
    
     String temp=String.valueOf(tbl.getValueAt(row, column));
    // List<com.cloudone.hotels.data.RoomTypes> roomType=getAllRoomType();

     for(int i=0;i<roomType.length;i++) 
     {
       if(temp.equals(roomType[i]))
       {   
           setState("roomclass");   
           return true;
       }
     }
     if(temp.equals("1")||temp.equals("0"))
     {
        if((Integer.parseInt(temp)==0)||(Integer.parseInt(String.valueOf(temp))==1))
        {
          setState("logical");    
          return true;           
        } 
     }
     if(temp.startsWith("Mr")||temp.startsWith("Mrs")||temp.startsWith("Prof")||temp.startsWith("Dr")||temp.contains("\n"))
     {
         setState("occupied");   
         return true;
     }
        if(temp.startsWith("\t"))
     {
         setState("reserved");   
         return true;
     }
    
     
     //roomType.clear();                 
    return false;
  }

    @Override
    public String getState() 
    {
      return state;  
    }

    @Override
    public void setState(String str) {
       state=str;
    }

    @Override
    public void run() 
    {
      for(int count=0;count<8;count++){
      columnmodel.getColumn(count).setCellRenderer(new CTableCellRenderer(this,null,new Color(101,126,161),Color.white));
      }  
              
    }
    private List<com.cloudone.hotels.data.RoomTypes> getAllRoomType()
    {
       JdbcPooledConnectionSource connectionSource=null;
     try
     {
        connectionSource=com.cloudone.hotels.System.getConnectionSource();
        Dao<com.cloudone.hotels.data.RoomTypes,Integer> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
        return RoomTypeDao.queryForAll(); 
     }
     catch(Exception exp){exp.printStackTrace();}
     finally
     {
        try
        {
          connectionSource.close();
        }catch(Exception exp){exp.printStackTrace();}
     }
     return null;      
    
    }

    @Override
    public void update() {
     Thread thread= new Thread(new Runnable(){
     public void run() {
       List<com.cloudone.hotels.data.RoomTypes> list=getAllRoomType();
      roomType= new String[list.size()];
      for(int i=0;i<list.size();i++)
      {
        roomType[i]=list.get(i).getName();
      }
     }
     
     
     });     
     thread.start();   
    }
    
    
}

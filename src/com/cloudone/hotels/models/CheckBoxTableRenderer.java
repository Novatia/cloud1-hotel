/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.models;

/**
 *
 * @author Akala G56
 */
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import java.awt.*;

public class CheckBoxTableRenderer extends JCheckBox implements TableCellRenderer {
   public CheckBoxTableRenderer() 
   {
      this.border = getBorder();
      this.setOpaque(true);
   }

   public void setForeground(Color foreground) {
      this.foreground = foreground;
      super.setForeground(foreground);
   }

   public void setBackground(Color background) {
      this.background = background;
      super.setBackground(background);
   }

   public void setFont(Font font) {
      this.font = font;
      super.setFont(font);
   }

   public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected,boolean hasFocus,int row, int column) {
      Color cellForeground = foreground !=
                 null ? foreground : table.getForeground();
      Color cellBackground = background !=
                 null ? background : table.getBackground();
      setFont(font != null ? font : table.getFont());
      if (hasFocus) {
          setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
          if(table.isCellEditable (row, column)) {
         cellForeground = UIManager.getColor("Table.focusCellForeground");
         cellBackground = UIManager.getColor("Table.focusCellBackground");
          }
      } else { setBorder(border);}

      super.setForeground (cellForeground);
      super.setBackground(cellBackground);
   

      return this;
   }

   
   

   protected Color foreground;
   protected Color background;
   protected Font font;
   protected Border border;
}

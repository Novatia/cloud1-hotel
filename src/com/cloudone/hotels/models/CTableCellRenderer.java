/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.models;

import javax.swing.table.TableCellRenderer;
import com.cloudone.hotels.interfaces.Comparator;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;

/**
 *
 * @author Akala G56
 */
public class CTableCellRenderer implements TableCellRenderer {
    
    public CTableCellRenderer(){}
    public CTableCellRenderer(Comparator cmp,TableCellRenderer targetRenderer,Color categoryBack, Color categoryFore) {
      this.targetRenderer = targetRenderer;
      this.cateBack = categoryBack;
      this.cateFore = categoryFore;
      cm=cmp;
   }
   
   public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus,int row, int column) {
      TableCellRenderer renderer = targetRenderer;
      if (renderer == null) {
          renderer = table.getDefaultRenderer(table.getColumnClass(column));
      }
      Component comp = renderer.getTableCellRendererComponent(table, value,isSelected,hasFocus, row, column);
                
      if (isSelected == false && hasFocus == false) {
             if (cm.shouldHighlight(table, value, row, column)) 
             {
                if(cm.getState().equals("occupied"))
                  { 
                      comp.setForeground(cateFore);
                      comp.setBackground(new Color(148,188,171));
                      
                      cm.setState("");
                   }
                   if(cm.getState().equals("logical"))
                   { 
                     comp.setForeground(cateFore);
                     comp.setBackground(cateBack);
                     cm.setState("");
                   }
                  if(cm.getState().equals("roomclass"))
                   {               
                     comp.setForeground(cateFore);
                     comp.setBackground(cateBack);
                     cm.setState("");
                    }
                  if(cm.getState().equals("reserved"))
                   {               
                     comp.setForeground(Color.black);
                     comp.setBackground(Color.YELLOW);
                     cm.setState("");
                    }
              } 
             else 
             {
                   comp.setForeground(table.getForeground());
                   comp.setBackground(table.getBackground());
             }
      }
        return comp;
      
   }
   
    protected TableCellRenderer targetRenderer;
    protected Color cateBack;
    protected Color cateFore;
    protected Comparator cm;

}


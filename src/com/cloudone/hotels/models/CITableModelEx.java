package com.cloudone.hotels.models;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class CITableModelEx extends AbstractTableModel
{
    private List<String> columnNames = new ArrayList();
    private List<List> data = new ArrayList();
    private Color rowColor;
    private int rowIndex;

    public CITableModelEx(String col[]){
        for(int count=0;count<col.length;count++){
            columnNames.add(col[count]);
        }
    }

    public void setrowIndex(int row ){
        rowIndex=row;
        fireTableRowsUpdated(row, row);

    }
    public int getrowIndex()
    {
        return rowIndex;
    }
    public void addRow(List rowData)
    {
        data.add(rowData);

        fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }
    public void addRowInBackground(List rowData)
    {
          data.add(rowData);
    }
    public int getColumnCount()
    {
        return columnNames.size();
    }

    public int getRowCount()
    {
        return data.size();
    }

    public String getColumnName(int col)
    {
        try
        {
            return columnNames.get(col);
        }
        catch(Exception e)
        {
            return null;
        }
    }
    public void setValueAt(Object value, int row, int col) {
        List rowdata =data.get(row);
        rowdata.set(col, value);
        this.fireTableStructureChanged();
    
    }
    public void deleteRow(int row){
        data.remove(row);
        
    }
      public void deleteAllRows()
    {
      
      data.clear();
      
    }
    public Object getValueAt(int row, int col)
    {
        return data.get(row).get(col);
    }

    public boolean isCellEditable(int row, int col)
    {
      return col == 9;
    }

    public Class getColumnClass(int c)
    {
        return getValueAt(0, c).getClass();
    }
  

};


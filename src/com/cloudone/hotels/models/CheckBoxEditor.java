/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.models;

/**
 *
 * @author Akala G56
 */
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class CheckBoxEditor extends BasicCellEditor implements ItemListener,TableCellEditor,MouseListener {
    JCheckBox _button;
   public CheckBoxEditor(JCheckBox button) {
      super(button);
      _button=button;
      _button.addItemListener(this);
   }

   public void setForeground(Color foreground) {
      this.foreground = foreground;
      editor.setForeground(foreground);
   }

   public void setBackground(Color background) {
      this.background = background;
      editor.setBackground(background);
   }

   public void setFont(Font font) {
      this.font = font;
      editor.setFont(font);
   }

   public Object getCellEditorValue() {
      return value;
   }

   public void editingStarted(EventObject event) {
       
     if(event instanceof MouseEvent)
     {
       mouseClicked((MouseEvent)event);
     }
       
   }

   public Component getTableCellEditorComponent(JTable tbl,Object value, boolean isSelected,int row, int column) {
      editor.setForeground(foreground != null ? foreground :tbl.getForeground());
      editor.setBackground(background != null ? background :tbl.getBackground());
      editor.setFont(font != null ? font : tbl.getFont());

      this.value = value;
      
      _button.setSelected(true);
      return _button;
      //return editor;
   }


    @Override
    public void itemStateChanged(ItemEvent e) {
          _button.setSelected(true);
                   
    }


   protected Object value;
   protected Color foreground;
   protected Color background;
   protected Font font;

    @Override
    public void mouseClicked(MouseEvent e) {
       _button.setSelected(true); 
    }

    @Override
    public void mousePressed(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.models;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Akala G56
 */
public abstract class CheckBoxModel extends CITableModelEx{
    private List<String> columnNames = new ArrayList();
    private List<List> data = new ArrayList();
    private Color rowColor;
    private int rowIndex;

    
    
    public CheckBoxModel(String col[]){
        super(col);
    }
    public int getColumnCount() {
      return super.getColumnCount() + 1;
   }

   public Object getValueAt(int row, int column) {
      if (column == BUTTON_COLUMN) {
         return "Update"+row;
      }
      return super.getValueAt(row, column);
   }

   public Class getColumnClass(int column) {
      if (column == BUTTON_COLUMN) {
         return String.class;
      }
      return super.getColumnClass(column);
   }
   public String getColumnName(int column) {
      if (column == BUTTON_COLUMN) {
         return "";
      }
      return super.getColumnName(column);
   }

   public boolean isCellEditable(int row, int column) {
      return column == BUTTON_COLUMN ||
                    super.isCellEditable(row, column);
   }

   public void setValueAt(Object value, int row, int column) {
      if (column == BUTTON_COLUMN) {
         // Button press - do whatever is needed to update
         // the table source
         updateTable(value, row, column);
         return;
      }

      // Other columns - use superclass
      super.setValueAt (value, row, column);
   }

   // Used to implement the table update
   protected abstract void updateTable (
                           Object value, int row, int column);

   protected static final int BUTTON_COLUMN = 8;
}

   
    


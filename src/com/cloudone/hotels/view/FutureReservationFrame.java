/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

/**
 *
 * @author Akala G56
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.table.AbstractTableModel;
 
public class FutureReservationFrame extends JDialog implements WindowListener{
    Box horizontal;
    public FutureReservationFrame(){
     
        setTitle("Reservations");        
        JPanel jp=new JPanel();
        jp.setLayout(new BorderLayout());
        JButton jb=new JButton("Add Reservation");
        JButton jb2=new JButton("Reject Reservation");
        horizontal=Box.createHorizontalBox();
        horizontal.add(Box.createHorizontalGlue());
        horizontal.add(jb);
        horizontal.add(jb2);
        jp.add(horizontal,BorderLayout.EAST);
        
        this.add(jp,BorderLayout.NORTH);
        CTableModel model=new CTableModel();
        model.addRow(Arrays.asList("18/12/14", "John Record", "18/6/2014", "21/6/2014", "Hotels.ng","[Accept Reservation] [Reject Reservation]"));
        JTable table= new JTable(model);

        JTabbedPane tabs=new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);      
        tabs.addTab("Future Reservations",new JScrollPane(table));   
        this.add(tabs,BorderLayout.CENTER);    
        setSize(700,700);
 
        com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);  
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
      
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {
//      JOptionPane.showMessageDialog(this,"closing the dialog means all other frames would be closed ", JOptionPane.WARNING_MESSAGE, WIDTH);
    }
    
}
class CTableModel extends AbstractTableModel
{
    private java.util.List<String> columnNames = new ArrayList();
    private java.util.List<java.util.List> data = new ArrayList();
    private Color rowColor;
    private int rowIndex;
    public CTableModel()
    {
        columnNames.add("Date");
        columnNames.add("Name");
        columnNames.add("Check-In");
        columnNames.add("Check-Out");
        columnNames.add("Agency");
        columnNames.add("Actions");
        
    }
    public void setrowIndex(int row )
    {
       rowIndex=row;
       fireTableRowsUpdated(row, row);
    }
    public int getrowIndex()
    {
      return rowIndex;
    }
    public void addRow(java.util.List rowData)
    {
        data.add(rowData);
        
        fireTableRowsInserted(data.size() - 1, data.size() - 1);
    }

    public int getColumnCount()
    {
        return columnNames.size();
    }

    public int getRowCount()
    {
        return data.size();
    }

    public String getColumnName(int col)
    {
        try
        {
            return columnNames.get(col);
        }
        catch(Exception e)
        {
            return null;
        }
    }

    public Object getValueAt(int row, int col)
    {
        return data.get(row).get(col);
    }

    public boolean isCellEditable(int row, int col)
    {
        return false;
    }

    public Class getColumnClass(int c)
    {
        return getValueAt(0, c).getClass();
    }
};



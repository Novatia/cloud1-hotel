/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

import com.cloudone.hotels.models.CITableModelEx;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Akala G56
 */
public class UsersSpreadSheet  extends JDialog{
    JdbcPooledConnectionSource connectionSource=null;
    public UsersSpreadSheet(){
    connectionSource=com.cloudone.hotels.System.getConnectionSource();    
    setTitle("Users List");
    setLayout(new BorderLayout());
    try
    {

     String headers[]={"Username","Password","Firstname","Lastname","Role"};
     CITableModelEx modelex=new CITableModelEx(headers);
     
     List<com.cloudone.hotels.data.User> UserList= getUsersList();
     if(UserList!=null)
     {
        for(int i=0;i<UserList.size();i++)
        {
         com.cloudone.hotels.data.User UserObj= UserList.get(i);     
         modelex.addRow(Arrays.asList(UserObj.getUsername(),hideHash(String.valueOf(UserObj.getPassword())),UserObj.getFirstName(),UserObj.getLastName(),UserObj.getRole()));   
        
        }
     
     }
     JTable table = new JTable(modelex);     
     add(table,BorderLayout.CENTER);
     
     setSize(500,400);
     setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
     com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
     
    }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
    }
    
    }
    public List<com.cloudone.hotels.data.User> getUsersList()
    {
              try
        {
        
            Dao<com.cloudone.hotels.data.User,Integer> UserDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.User.class);
            return UserDao.queryForAll();

        }
        catch(Exception exp)
        {
            com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
        
        return null;

    
    }
    public String hideHash(String hashcode)
    {
       
      StringBuffer buffer= new StringBuffer(); 
      for(int i=0;i<hashcode.length();i++)
      {
        buffer.append("*");
      
      }
      return buffer.toString();
       
    }
}




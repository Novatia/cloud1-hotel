/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

/**
 *
 * @author Akala G56
 */
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.*;
import org.jbundle.thin.base.screen.jcalendarbutton.JCalendarButton;
import org.jdesktop.swingx.JXDatePicker;
//import org.jdesktop.swingx.JXDatePicker;
import sun.util.calendar.Gregorian;
public class ReservationFrame extends JDialog implements ItemListener,Runnable{
    JPanel jpGuest;
    JPanel jpStayInfo;
    JPanel jpContactInfo;
    JPanel jpOtherInfo;
    JPanel jpBillingInfo;
    private String jcbTitles[]={"Mr","Mrs","Dr","Prof"};
    private String jcbCountries[]={"Nigeria","United States","United Kingdom","China"};
    private String jcbRoomTypes[]={"Delux","King","Queens"};
    private String jcbRoomRate[]={"--Select-",""};
    private String jcbRoomPlan[]={"Dialy","Monthly","Yearly"};
    private String jcbAdult[]={"2","1","0"};
    private String jcbChild[]={"2","1","0"};
    private String jcbIden[]={"Driving License","Passport"};
    private String jcbBooking[]={"Confirm Booking"};
    private String jcbVipStatus[]={"Regular"};
    private String jcbVisitor[]={"Guests"};
    private String jcbPay[]={"Cash","Credit"};
    private String jcbCompVal[]={"Global"};
    private String jcbMarket[]={"Travel Agent"};
    private String jcbBussiness[]={"Global"};
    private String jcbRmNo[]={"104","105",""};

    private JTitlePane jtp;
    private JTextField jtfGuestName;
    private JTextArea jtaAddress;
    private JTextField jtfadd2;
    private JComboBox jcbCountry;
    private JTextField jtfPhone;
    private JTextField jtfEmail;
    private JTextField jtfMobile;
    private JComboBox jcbIdentityType;
    private JTextField jtfIdentityNo;
    private JComboBox jtfRoomNo;
    private JComboBox jcbRoomType;
    private JTextField jtfArrDate;
    private JTextField jtfArrTime;
    private JTextField jtfDayZone;
    private JComboBox jcbDepartureZone;
    private JComboBox jcbChildren;
    private JTextField jtfDepartureDate;
    private JTextField jtfDepartureTime;
    private JTextField jtfPrice;
    private JButton jbReserve;
    private JComboBox jcbTitle;
    private JCheckBox jcbClose;
    private JButton jbcheck;
    JTextField jtfUnitPrice;
    private JFrame parentComponent;
    JdbcPooledConnectionSource connectionSource=null; 
        
    public ReservationFrame(JFrame parentcomponent){
     connectionSource=com.cloudone.hotels.System.getConnectionSource();   
     parentComponent=parentcomponent;
    }
    private void initComponents(){
      try
      {  
       setLayout(null);
      setSize(1250,550);
      setTitle("Reservation/Check In Mastersheet");
      parentComponent.getGlassPane().setVisible(true);
      this.getContentPane().setBackground(Color.WHITE);
      createGuestPanel();
      this.createStayInfoPanel();
      this.createContactInfoPanel();
      this.createOtherInfoPanel();
      this.createBillingInfoPanel();
      this.createCompanyInfo();
      this.createUtilitiesPanel();
      this.createRemarkPanel();
      this.createSouthPanel();
      //this.setResizable(false);
        com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
      setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      
      setVisible(true);      
     }catch(Exception exp)
     {
       setVisible(false);
       JOptionPane.showMessageDialog(this,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
       parentComponent.getGlassPane().setVisible(false);
     }
      
    }
    private void createGuestPanel(){
      try
      {
       jpGuest=new JPanel();
       jpGuest.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
       jpGuest.setLayout(null);
       
       jpGuest.setSize(400,200);
       jpGuest.setLocation(0,0);
       jpGuest.setBackground(Color.WHITE);
       
       jtp= new JTitlePane("Guest Information");
       jpGuest.add(jtp);
       
       JLabel jlName= new JLabel("Name");
       jlName.setSize(60,20);
       jlName.setLocation(20,40);
       jpGuest.add(jlName);
       
       
       jcbTitle= new JComboBox(jcbTitles);
       jcbTitle.setSize(50,25);
       jcbTitle.setLocation(100,40);
       jpGuest.add(jcbTitle);
       
       jtfGuestName=new JTextField();
       jtfGuestName.setSize(200,30);
       jtfGuestName.setLocation(155,40);
       jpGuest.add(jtfGuestName);
       
       
       JLabel jlAddress= new JLabel("Address");
       jlAddress.setSize(60,20);
       jlAddress.setLocation(20,80);
       jpGuest.add(jlAddress);
       
       jtaAddress=new JTextArea();
       jtaAddress.setSize(250,50);
       jtaAddress.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
       jtaAddress.setLocation(100,80);
       jpGuest.add(jtaAddress);
       
       jtfadd2=new JTextField();
       jtfadd2.setSize(70,25);
       jtfadd2.setLocation(100,140);
       jpGuest.add(jtfadd2);
       
        JTextField jtfadd3=new JTextField();
       jtfadd3.setSize(70,25);
       jtfadd3.setLocation(190,140);
       jpGuest.add(jtfadd3);
       
       JTextField jtfadd4=new JTextField();
       jtfadd4.setSize(70,25);
       jtfadd4.setLocation(280,140);
       jpGuest.add(jtfadd4);
       
       
       JLabel jlCountry= new JLabel("Country");
       jlCountry.setSize(60,20);
       jlCountry.setLocation(20,170);
       jpGuest.add(jlCountry);
       
       String[] locales = java.util.Locale.getISOCountries();
       jcbCountry = new javax.swing.JComboBox();
        for (String countryCode : locales)
        {
            java.util.Locale obj = new java.util.Locale("", countryCode);
            jcbCountry.addItem(obj.getDisplayCountry());
        }

       jcbCountry.setSize(250,25);
       jcbCountry.setLocation(100,170);
       jpGuest.add(jcbCountry);
       
       add(jpGuest);
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
      }
    }
    private void createStayInfoPanel() throws Exception{
      try
      {
       jpStayInfo=new JPanel();
       jpStayInfo.setLayout(null);
       jpStayInfo.setSize(450,200);
       jpStayInfo.setLocation(410,0);
       jpStayInfo.setBackground(Color.WHITE);
       jpStayInfo.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
       JTitlePane jtp= new JTitlePane("Stay Information");
       jpStayInfo.add(jtp);
       
       
       
       JLabel jlRoom=new JLabel("Room(s)");
       jlRoom.setSize(60,20);
       jlRoom.setLocation(20,40);
       jpStayInfo.add(jlRoom);
       
       List<com.cloudone.hotels.data.RoomTypes> roomtypes= this.getRoomTypes();
       
       jcbRoomType= new JComboBox();
       if(roomtypes.isEmpty())
       {
        
       // throw new Exception("com.cloudone.hotels.exceptions.RoomTypeNotFoundException");
        
       
       }
       if(!roomtypes.isEmpty())
       {
         for(int i=0; i<roomtypes.size();i++)
         { 
           com.cloudone.hotels.data.RoomTypes type=roomtypes.get(i);  
           jcbRoomType.addItem(type.getName()); 
         }
       }
       com.cloudone.hotels.data.RoomTypes type=roomtypes.get(0);
       List<String[]> resultset= getAllRoom(type.getId());
       jtfRoomNo=new JComboBox();
       if(resultset.isEmpty())
       {
        //throw new Exception("com.cloudone.hotels.exceptions.RoomNotFoundException");
        
       }
       else
       {
            for(int i=0; i<resultset.size();i++)
              { 
               String[] result=resultset.get(i);
                jtfRoomNo.addItem(result[0]); 
              }
       }
       jtfRoomNo.setSize(80,25);
       jtfRoomNo.setLocation(100,40);
       jtfRoomNo.addItemListener(this);
       jpStayInfo.add(jtfRoomNo);
       
       jcbRoomType.addItemListener(this);
       jcbRoomType.setSize(200,25);
       jcbRoomType.setLocation(210,40);
       jcbRoomType.addItemListener(this);
       jpStayInfo.add(jcbRoomType);       
       
       JComboBox jcbRoomRate= new JComboBox(this.jcbRoomRate);
       jcbRoomRate.setSize(80,25);
       jcbRoomRate.setLocation(290,40);
       //jpStayInfo.add(jcbRoomRate);   
       
       JComboBox jcbRoomPlan= new JComboBox(this.jcbRoomPlan);
       jcbRoomPlan.setSize(60,25);
       jcbRoomPlan.setLocation(380,40);
       //jpStayInfo.add(jcbRoomPlan);   
       
       JLabel jlArrival= new JLabel("Arrival");
       jlArrival.setSize(60,20);
       jlArrival.setLocation(20,80);
       jpStayInfo.add(jlArrival);
       
       
       jtfArrDate=new JTextField("DD/MM/YY");
       jtfArrDate.setEnabled(false);
       jtfArrDate.setSize(100,25);
       jtfArrDate.setLocation(100,80);
       jpStayInfo.add(jtfArrDate);
    final   de.javasoft.swing.DateComboBox calendar= new de.javasoft.swing.DateComboBox();
       
    //final JXDatePicker calendar = new JXDatePicker();
    calendar.setSize(30,25);
    calendar.setLocation(200,80);
    calendar.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) 
        {
	jtfDayZone.setText("");	
         Calendar now =Calendar.getInstance();
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
         now.setTime(calendar.getDate());
         jtfArrDate.setText(format.format(now.getTime()));
         
         
         
         if(!jtfDepartureDate.getText().equals("DD/MM/YY")&&!jtfArrDate.getText().equals("DD/MM/YY"))
           {
              String[] array= jtfArrDate.getText().split("-");
              String[] arrays=jtfDepartureDate.getText().split("-");
              
              
              
              Calendar c1=Calendar.getInstance();
              Calendar c2=Calendar.getInstance();
              c1.set(Integer.parseInt(array[0]),Integer.parseInt(array[1]) ,Integer.parseInt(array[2]));
              c2.set(Integer.parseInt(arrays[0]),Integer.parseInt(arrays[1]) ,Integer.parseInt(arrays[2]));
              
              if(com.cloudone.hotels.commons.DateHelper.compare2Dates(c1, c2)==com.cloudone.hotels.commons.Date.DATE_GREATER)
              {
                  String temp=String.valueOf(com.cloudone.hotels.commons.DateHelper.daysBetween2Dates(c1, c2));
                 if(temp.startsWith("-"))
                 {
                     jtfDayZone.setText(temp.substring(1, temp.length()));                   
                 }
                 else{jtfDayZone.setText(temp);}
              }
              if(com.cloudone.hotels.commons.DateHelper.compare2Dates(c1, c2)==com.cloudone.hotels.commons.Date.DATE_LESS_THAN)
              {
               String temp=String.valueOf(com.cloudone.hotels.commons.DateHelper.daysBetween2Dates(c2, c1));
                 if(temp.startsWith("-"))
                 {
                     jtfDayZone.setText(temp.substring(1, temp.length()));                   
                 }
                 else{jtfDayZone.setText(temp);}
              }
              
           }
           if(!jtfDepartureDate.getText().isEmpty() && !jtfDepartureDate.getText().equals("DD/MM/YY"))
         {
           String  price=getRoomTypePrice((String)jcbRoomType.getSelectedItem());
          jtfPrice.setText(price);
          jtfPrice.setEditable(false);
           if(!jtfDayZone.getText().isEmpty())       
            {
             int unitprice= Integer.parseInt(price);
             int no= Integer.parseInt(jtfDayZone.getText());
             int total= unitprice * no;   
             jtfUnitPrice.setText(String.valueOf(total));
             jtfUnitPrice.setEditable(false);
           } 
       
         
         
         }
	}
        
       });
    
       jpStayInfo.add(calendar);
       jtfArrTime=new JTextField("12:00 PM");
       jtfArrTime.setSize(70,25);
       jtfArrTime.setLocation(240,80);
       jpStayInfo.add(jtfArrTime);
       
       JLabel jlDayZone= new JLabel("Night");
       jlDayZone.setSize(60,20);
       jlDayZone.setLocation(320,80);
       jpStayInfo.add(jlDayZone);
       
       
       jtfDayZone=new JTextField();
       jtfDayZone.setSize(70,25);
       jtfDayZone.setEnabled(false);
       jtfDayZone.setLocation(360,80);
       jpStayInfo.add(jtfDayZone);
       
               
       JLabel jlDeparture= new JLabel("Departure");
       jlDeparture.setSize(60,20);
       jlDeparture.setLocation(20,120);
       jpStayInfo.add(jlDeparture);
       
       jtfDepartureDate=new JTextField("DD/MM/YY");
       jtfDepartureDate.setSize(100,25);
       jtfDepartureDate.setEnabled(false);
       jtfDepartureDate.setLocation(100,120);
       
       
       
    final   de.javasoft.swing.DateComboBox calendar2= new de.javasoft.swing.DateComboBox();   
   // final JXDatePicker calendar2 = new JXDatePicker();
    calendar2.setSize(30,25);
    calendar2.setLocation(200,120);
    calendar2.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) 
        {
		
         jtfDayZone.setText("");
         Calendar nowc =Calendar.getInstance();
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
         nowc.setTime(calendar2.getDate());
         jtfDepartureDate.setText(format.format(nowc.getTime()));
         
         if(!jtfDepartureDate.getText().equals("DD/MM/YY")&&!jtfArrDate.getText().equals("DD/MM/YY"))
           {
              String[] array= jtfArrDate.getText().split("-");
              String[] arrays=jtfDepartureDate.getText().split("-");
              
              Calendar c3=Calendar.getInstance();
              Calendar c4=Calendar.getInstance();
              c3.set(Integer.parseInt(array[0]),Integer.parseInt(array[1]) ,Integer.parseInt(array[2]));
              c4.set(Integer.parseInt(arrays[0]),Integer.parseInt(arrays[1]) ,Integer.parseInt(arrays[2]));
              
              if(com.cloudone.hotels.commons.DateHelper.compare2Dates(c3, c4)==com.cloudone.hotels.commons.Date.DATE_GREATER)
              {
                  String temp=String.valueOf(com.cloudone.hotels.commons.DateHelper.daysBetween2Dates(c3, c4));
                 if(temp.startsWith("-"))
                 {
                     jtfDayZone.setText(temp.substring(1, temp.length()));                   
                 }
                 else{jtfDayZone.setText(temp);}
              }
              if(com.cloudone.hotels.commons.DateHelper.compare2Dates(c3, c4)==com.cloudone.hotels.commons.Date.DATE_LESS_THAN)
              {
               String temp=String.valueOf(com.cloudone.hotels.commons.DateHelper.daysBetween2Dates(c3, c4));
                 if(temp.startsWith("-"))
                 {
                     jtfDayZone.setText(temp.substring(1, temp.length()));                   
                 }
                 else{jtfDayZone.setText(temp);}
              }
          
           }
             if(!jtfArrDate.getText().isEmpty() && !jtfArrDate.getText().equals("DD/MM/YY"))
         {
           String  price=getRoomTypePrice((String)jcbRoomType.getSelectedItem());
          jtfPrice.setText(price);
          jtfPrice.setEditable(false);
           if(!jtfDayZone.getText().isEmpty())       
            {
             int unitprice= Integer.parseInt(price);
             int no= Integer.parseInt(jtfDayZone.getText());
             int total= unitprice * no;   
             jtfUnitPrice.setText(String.valueOf(total));
             jtfUnitPrice.setEditable(false);
           } 
       
         
         
         }
         
	}
       });
    jpStayInfo.add(calendar2);
    jpStayInfo.add(jtfDepartureDate);   
       jtfDepartureTime=new JTextField("12:00 PM");
       jtfDepartureTime.setSize(70,25);
       jtfDepartureTime.setLocation(240,120);
       jpStayInfo.add(jtfDepartureTime);
       
       
       JLabel jlReservation= new JLabel("Reservation Type");
       jlReservation.setSize(100,20);
       jlReservation.setLocation(20,165);
       jpStayInfo.add(jlReservation);
       
       JComboBox jcbConfirm=new JComboBox(this.jcbBooking);
       jcbConfirm.setSize(130,25);
       jcbConfirm.setLocation(140,165);
       jpStayInfo.add(jcbConfirm);
       
       
       JLabel jlDepartureZone= new JLabel("Adult");
       jlDepartureZone.setSize(60,20);
       jlDepartureZone.setLocation(320,165);
       jpStayInfo.add(jlDepartureZone);
       
       
       jcbDepartureZone=new JComboBox(this.jcbAdult);
       jcbDepartureZone.setSize(70,25);
       jcbDepartureZone.setLocation(360,165);
       jpStayInfo.add(jcbDepartureZone);
       
       
       add(jpStayInfo);
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
      }
    }
    private void createContactInfoPanel(){
     
      try
      {
       jpContactInfo=new JPanel();
       jpContactInfo.setLayout(null);
       jpContactInfo.setSize(350,200);
       jpContactInfo.setLocation(880,0);
       jpContactInfo.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
       jpContactInfo.setBackground(Color.WHITE);
       JTitlePane jtp= new JTitlePane("Contact Information");
       jpContactInfo.add(jtp);
       
       
       JLabel jlEmail= new JLabel("Email");
       jlEmail.setSize(60,20);
       jlEmail.setLocation(20,40);
       jpContactInfo.add(jlEmail);
       
       
       jtfEmail=new JTextField("");
       jtfEmail.setSize(200,25);
       jtfEmail.setLocation(100,40);
       jpContactInfo.add(jtfEmail);
       
       JLabel jlPhone= new JLabel("Phone");
       jlPhone.setSize(60,20);
       jlPhone.setLocation(20,80);
       jpContactInfo.add(jlPhone);
       
       
       jtfPhone=new JTextField("");
       jtfPhone.setSize(200,25);
       jtfPhone.setLocation(100,80);
       jpContactInfo.add(jtfPhone);
       
       
       JLabel jlMobile= new JLabel("Mobile");
       jlMobile.setSize(60,20);
       jlMobile.setLocation(20,120);
       jpContactInfo.add(jlMobile);
       
       
       jtfMobile=new JTextField("");
       jtfMobile.setSize(200,25);
       jtfMobile.setLocation(100,120);
       jpContactInfo.add(jtfMobile);
       
       JLabel jlFax= new JLabel("Fax");
       jlFax.setSize(60,20);
       jlFax.setLocation(20,165);
       jpContactInfo.add(jlFax);
       
       
       JTextField jtfFax=new JTextField("");
       jtfFax.setSize(200,25);
       jtfFax.setLocation(100,165);
       jpContactInfo.add(jtfFax);
       
       add(jpContactInfo);
     }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
      }
    }
    private void createOtherInfoPanel()
    {
       try
       {
       jpOtherInfo=new JPanel();
       jpOtherInfo.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
       jpOtherInfo.setLayout(null);
       JTitlePane jtp= new JTitlePane("Other Information");
       jpOtherInfo.add(jtp);
       
       jpOtherInfo.setSize(400,200);
       jpOtherInfo.setLocation(0,220);
       jpOtherInfo.setBackground(Color.WHITE);
        
       JLabel jlIdentity= new JLabel("Identity");
       jlIdentity.setSize(60,20);
       jlIdentity.setLocation(20,40);
       jpOtherInfo.add(jlIdentity);
       
       
       jcbIdentityType=new JComboBox(this.jcbIden);
       jcbIdentityType.setSize(150,25);
       jcbIdentityType.setLocation(100,40);
       jpOtherInfo.add(jcbIdentityType);
       
       JTextField jtfIdentityNo=new JTextField();
       jtfIdentityNo.setSize(100,25);
       jtfIdentityNo.setLocation(270,40);
       jpOtherInfo.add(jtfIdentityNo);
       
       
       JLabel jlNationality= new JLabel("Nationality");
       jlNationality.setSize(60,20);
       jlNationality.setLocation(20,80);
       jpOtherInfo.add(jlNationality);
       
       
       JComboBox jcbNationality=new JComboBox();
       String[] locales = java.util.Locale.getISOCountries();
       jcbCountry = new javax.swing.JComboBox();
        for (String countryCode : locales)
        {
            java.util.Locale obj = new java.util.Locale("", countryCode);
            jcbNationality.addItem(obj.getDisplayCountry());
        }

       jcbNationality.setSize(265,25);
       jcbNationality.setLocation(100,80);
       jpOtherInfo.add(jcbNationality);
       
       JLabel jlGender= new JLabel("Gender");
       jlGender.setSize(60,20);
       jlGender.setLocation(20,120);
       jpOtherInfo.add(jlGender);
      
       JRadioButton jrbMale=new JRadioButton("Male",true);
       jrbMale.setSize(60,20);
       jrbMale.setLocation(100,120);
       jpOtherInfo.add(jrbMale);
       
       JRadioButton jrbFemale=new JRadioButton("Female",false);
       jrbFemale.setSize(70,20);
       jrbFemale.setLocation(170,120);
       jpOtherInfo.add(jrbFemale);
       
       JRadioButton jrbOther=new JRadioButton("Other",false);
       jrbOther.setSize(60,20);
       jrbOther.setLocation(250,120);
       jpOtherInfo.add(jrbOther);
       
       ButtonGroup radioGroup=new ButtonGroup();
       radioGroup.add(jrbMale);
       radioGroup.add(jrbFemale);
       radioGroup.add(jrbOther);

       
       JLabel jlVip= new JLabel("Vip Status");
       jlVip.setSize(60,20);
       jlVip.setLocation(20,165);
       jpOtherInfo.add(jlVip);
      
       
       JComboBox jcbVip=new JComboBox(this.jcbVipStatus);
       jcbVip.setSize(265,25);
       jcbVip.setLocation(100,165);
       jpOtherInfo.add(jcbVip);
       
       
       add(jpOtherInfo);
       }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
      }
    }
    private void createBillingInfoPanel()
    {
       try
       {
       jpBillingInfo=new JPanel();
       jpBillingInfo.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
       jpBillingInfo.setLayout(null);
       
       JTitlePane jtp= new JTitlePane("Billing Information");
       jpBillingInfo.add(jtp);
       jpBillingInfo.setSize(450,200);
       jpBillingInfo.setLocation(410,220);  
       jpBillingInfo.setBackground(Color.WHITE);
        
        
       JLabel jlRates= new JLabel("Rates");
       jlRates.setSize(60,20);
       jlRates.setLocation(20,40);
       jpBillingInfo.add(jlRates);
       add(jpBillingInfo);
    
       
       JRadioButton jrbNormal=new JRadioButton("Normal",true);
       jrbNormal.setSize(70,20);
       jrbNormal.setLocation(100,40);
       jpBillingInfo.add(jrbNormal);
       
       JRadioButton jrbContract=new JRadioButton("Contract",false);
       jrbContract.setSize(80,20);
       jrbContract.setLocation(170,40);
       jpBillingInfo.add(jrbContract);
       
       JRadioButton jrbManual=new JRadioButton("Manual",false);
       jrbManual.setSize(70,20);
       jrbManual.setLocation(250,40);
       jpBillingInfo.add(jrbManual);
       
       ButtonGroup radioGroup=new ButtonGroup();
       radioGroup.add(jrbNormal);
       radioGroup.add(jrbContract);
       radioGroup.add(jrbManual);
      
               
       JLabel jlBill= new JLabel("Bill To");
       jlBill.setSize(60,20);
       jlBill.setLocation(20,80);
       jpBillingInfo.add(jlBill);
       
       
       JComboBox jcbGuests=new JComboBox(this.jcbVisitor);
       jcbGuests.setSize(150,25);
       jcbGuests.setLocation(100,80);
       jpBillingInfo.add(jcbGuests);
       
       JCheckBox jcbExempt =new JCheckBox("Exempt Id");
       jcbExempt.setSize(100,20);
       jcbExempt.setLocation(250,80);
       jpBillingInfo.add(jcbExempt);
       
       JTextField jtf=new JTextField();
       jtf.setSize(100,30);
       jtf.setLocation(340,80);
       jtf.setEnabled(false);
       jpBillingInfo.add(jtf);
       
       
       JLabel jlPayMode= new JLabel("Pay Mode");
       jlPayMode.setSize(100,20);
       jlPayMode.setLocation(20,120);
       jpBillingInfo.add(jlPayMode);
       
       
       JRadioButton jrbCash=new JRadioButton("Cash",true);
       jrbCash.setSize(70,20);
       jrbCash.setLocation(100,120);
       jpBillingInfo.add(jrbCash);
       
       JRadioButton jrbCredit=new JRadioButton("Credit",false);
       jrbCredit.setSize(80,20);
       jrbCredit.setLocation(170,120);
       jpBillingInfo.add(jrbCredit);
       
       JComboBox jcbPayMode= new JComboBox(this.jcbPay);
       jcbPayMode.setSize(190,25);
       jcbPayMode.setLocation(250,120);
       jpBillingInfo.add(jcbPayMode);
       
       
       
       ButtonGroup radioGroup2=new ButtonGroup();
       radioGroup2.add(jrbCash);
       radioGroup2.add(jrbCredit);
       
       
       JLabel jl =new JLabel("Unit Price");
       jl.setSize(100,20);
       jl.setLocation(20,165);
       jpBillingInfo.add(jl);
       
       jtfUnitPrice=new JTextField();
       jtfUnitPrice.setEnabled(false);
       jtfUnitPrice.setSize(100,25);
       jtfUnitPrice.setLocation(100,165);
       jtfUnitPrice.setText(getRoomTypePrice((String)jcbRoomType.getSelectedItem()));
       jpBillingInfo.add(jtfUnitPrice);

       JLabel jlTerm= new JLabel("Total Price");
       jlTerm.setSize(100,20);
       jlTerm.setLocation(250,165);
       jpBillingInfo.add(jlTerm);
       
       jtfPrice=new JTextField();
       jtfPrice.setText(getRoomTypePrice((String)jcbRoomType.getSelectedItem()));
       jtfPrice.setEnabled(false);
       jtfPrice.setSize(100,25);
       jtfPrice.setLocation(320,165);
       jpBillingInfo.add(jtfPrice);
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
      }
      
    }
    private void createCompanyInfo()
    {
       JPanel jpCompanyInfo=new JPanel();
       jpCompanyInfo.setLayout(null);
       jpCompanyInfo.setSize(350,200);
       jpCompanyInfo.setLocation(880,220);
       jpCompanyInfo.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
       jpCompanyInfo.setBackground(Color.WHITE);
       JTitlePane jtp= new JTitlePane("Company Information");
       jpCompanyInfo.add(jtp);
       
       JLabel jlCompany= new JLabel("Company");
       jlCompany.setSize(60,20);
       jlCompany.setLocation(20,40);
       jpCompanyInfo.add(jlCompany);
    
       JComboBox jcbCompany= new JComboBox(this.jcbCompVal);
       jcbCompany.setSize(180,25);
       jcbCompany.setLocation(100,40);
       jpCompanyInfo.add(jcbCompany);
      
        
       JLabel jlMarket= new JLabel("Market");
       jlMarket.setSize(60,20);
       jlMarket.setLocation(20,80);
       jpCompanyInfo.add(jlMarket);
    
       JComboBox jcbMarkt= new JComboBox(this.jcbMarket);
       jcbMarkt.setSize(180,25);
       jcbMarkt.setLocation(100,80);
       jpCompanyInfo.add(jcbMarkt);
       
       JLabel jlBuss= new JLabel("Bussiness");
       jlBuss.setSize(140,20);
       jlBuss.setLocation(20,120);
       jpCompanyInfo.add(jlBuss);
    
       JComboBox jcbBuss= new JComboBox(this.jcbBussiness);
       jcbBuss.setSize(180,25);
       jcbBuss.setLocation(100,120);
       jpCompanyInfo.add(jcbBuss);
       add(jpCompanyInfo);
     
    }
    private void createUtilitiesPanel()
    {
      JPanel jpRemarks= new JPanel();
      jpRemarks.setLayout(null);
      jpRemarks.setBackground(Color.WHITE);
      jpRemarks.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
      jpRemarks.setSize(1230,30);
      jpRemarks.setLocation(0,430);
      
      JCheckBox jcbPrintRegCard=new JCheckBox("Print Guest Registration Card");
      jcbPrintRegCard.setSize(200,20);
      jcbPrintRegCard.setLocation(5,5);
      jpRemarks.add(jcbPrintRegCard);
      
      
      JCheckBox jcbPrintFolio=new JCheckBox("Print Portofolio");
      jcbPrintFolio.setSize(180,20);
      jcbPrintFolio.setLocation(210,5);
      jpRemarks.add(jcbPrintFolio);
      
      JCheckBox jcbReceipt=new JCheckBox("Print Receipt");
      jcbReceipt.setSize(100,20);
      jcbReceipt.setLocation(390,5);
      jpRemarks.add(jcbReceipt);
      
      add(jpRemarks);
     
    }
    private void createRemarkPanel()
    {
      JPanel jpComments= new JPanel();
      jpComments.setLayout(null);
      jpComments.setBackground(Color.WHITE);
      jpComments.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
      jpComments.setSize(1230,100);
      jpComments.setLocation(0,470);
      
      JCommentPane pane= new JCommentPane();
      jpComments.add(pane);
      
      
       JComboBox jcbRmTypes= new JComboBox();
       jcbRmTypes.setSize(130,26);
       jcbRmTypes.setLocation(20,35);
       jpComments.add(jcbRmTypes);
       
       
       JComboBox jcbRm= new JComboBox();
       jcbRm.setSize(100,26);
       jcbRm.setLocation(220,35);
       jpComments.add(jcbRm);
       
       
       JComboBox jcbRmRate= new JComboBox(this.jcbRoomPlan);
       jcbRmRate.setSize(130,26);
       jcbRmRate.setLocation(320,35);
       jpComments.add(jcbRmRate);
       
       
       JComboBox jcbAdult=new JComboBox(this.jcbAdult);
       jcbAdult.setSize(70,25);
       jcbAdult.setLocation(450,35);
       jpComments.add(jcbAdult);
       
       
       JComboBox jcbChd=new JComboBox(this.jcbChild);
       jcbChd.setSize(70,25);
       jcbChd.setLocation(520,35);
       jpComments.add(jcbChd);
       
       
       JComboBox jcbTitle= new JComboBox(jcbTitles);
       jcbTitle.setSize(50,25);
       jcbTitle.setLocation(590,35);
       jpComments.add(jcbTitle);
       
       JTextField jtfGuestName=new JTextField();
       jtfGuestName.setSize(200,25);
       jtfGuestName.setLocation(640,35);
       jpComments.add(jtfGuestName);
       
       
       JComboBox jcbIdentityType=new JComboBox(this.jcbIden);
       jcbIdentityType.setSize(150,25);
       jcbIdentityType.setLocation(860,35);
       jpComments.add(jcbIdentityType);
       
       jtfIdentityNo=new JTextField();
       jtfIdentityNo.setSize(100,25);
       jtfIdentityNo.setLocation(1020,35);
       jpComments.add(jtfIdentityNo);
       
       //---
       
       JComboBox jcbRmTypes2= new JComboBox();
       jcbRmTypes2.setSize(130,26);
       jcbRmTypes2.setLocation(20,70);
       jpComments.add(jcbRmTypes2);
       
       JComboBox jcbRm2= new JComboBox();
       jcbRm2.setSize(100,26);
       jcbRm2.setLocation(220,70);
       jpComments.add(jcbRm2);
       
       
       JComboBox jcbRmRate2= new JComboBox(this.jcbRoomPlan);
       jcbRmRate2.setSize(130,26);
       jcbRmRate2.setLocation(320,70);
       jpComments.add(jcbRmRate2);
       
       
       JComboBox jcbAdult2=new JComboBox(this.jcbAdult);
       jcbAdult2.setSize(70,25);
       jcbAdult2.setLocation(450,70);
       jpComments.add(jcbAdult2);
       
       
       JComboBox jcbChd2=new JComboBox(this.jcbChild);
       jcbChd2.setSize(70,25);
       jcbChd2.setLocation(520,70);
       jpComments.add(jcbChd2);
       
       
       JComboBox jcbTitle2= new JComboBox(jcbTitles);
       jcbTitle2.setSize(50,25);
       jcbTitle2.setLocation(590,70);
       jpComments.add(jcbTitle2);
       
       JTextField jtfGuestName2=new JTextField();
       jtfGuestName2.setSize(200,25);
       jtfGuestName2.setLocation(640,70);
       jpComments.add(jtfGuestName2);
       
       
       JComboBox jcbIdentityType2=new JComboBox(this.jcbIden);
       jcbIdentityType2.setSize(150,25);
       jcbIdentityType2.setLocation(860,70);
       jpComments.add(jcbIdentityType2);
       
       JTextField jtfIdentityNo2=new JTextField();
       jtfIdentityNo2.setSize(100,25);
       jtfIdentityNo2.setLocation(1020,70);
       jpComments.add(jtfIdentityNo2); 
       //add(jpComments);
     
    }
    private void createSouthPanel()
    {
      JPanel jpSouth= new JPanel();
      jpSouth.setLayout(null);
      jpSouth.setBackground(Color.WHITE);
      //jpSouth.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
      jpSouth.setSize(1230,30);
      jpSouth.setLocation(0,470);
      
      jcbClose= new JCheckBox("Close when finished");
      jcbClose.setSize(170,25);
      jcbClose.setLocation(750,5);
      //jpSouth.add(jcbClose);
      
      
      jbcheck=new JButton("Check In");
      jbcheck.setSize(80,25);
      jbcheck.setLocation(945,5);
      jpSouth.add(jbcheck);
      
      
      jbReserve=new JButton("Reserve");
      jbReserve.setSize(80,25);
      jbReserve.setLocation(1045,5);
      jpSouth.add(jbReserve);
      add(jpSouth);
      InstallHandler();
      
    }
       
    private List<com.cloudone.hotels.data.RoomTypes> getRoomTypes()
    {
      
     try
     {
        
        Dao<com.cloudone.hotels.data.RoomTypes,Integer> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
        return RoomTypeDao.queryForAll();
     }
     catch(Exception exp){exp.printStackTrace();}
     
        return null;
      
    }  
    public List<String[]> getAllRoom(int Id)
    {
      
        
        try
        {
           
            Dao<com.cloudone.hotels.data.Room,String> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
            GenericRawResults<String[]> rawOutput =RoomDao.queryRaw("select _name from room where _room_type_id ='"+String.valueOf(Id) +"'");
            
            return rawOutput.getResults();

        }
        catch(Exception exp)
        {
            java.lang.System.out.println(exp.getMessage());
        }
        
        return null;
    
    }
    public String getRoomTypePrice(String name)
    {            
      
        try
        {
            
            Dao<com.cloudone.hotels.data.RoomTypes,String> UserDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
            GenericRawResults<String[]> rawOutput =UserDao.queryRaw("select _price from roomtypes where _name ='"+name+"'");
            List<String[]>  resultset= rawOutput.getResults();
            String[] output = resultset.get(0);
            return output[0];
        }
        catch(Exception exp)
        {
            java.lang.System.out.println(exp.getMessage());
        }
        
        return null;
    }
    public int getRoomTypeId(String name)
    {            
      
        try
        {
      
            Dao<com.cloudone.hotels.data.RoomTypes,String> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
            GenericRawResults<String[]> rawOutput =RoomTypeDao.queryRaw("select _id from roomtypes where _name ='"+name+"'");
            List<String[]>  resultset= rawOutput.getResults();
            String[] output = resultset.get(0);
            return Integer.parseInt(output[0]);
        }
        catch(Exception exp)
        {
            java.lang.System.out.println(exp.getMessage());
        }
        
        return -1;
    }
    
    
    private void InstallHandler(){
        jbReserve.addActionListener(new com.cloudone.hotels.controllers.ReservationListener(
        (TransactionGlassPane) getGlassPane(),        
        jcbTitle,
        jtfGuestName,
        jtaAddress,
        jtfadd2,
        jcbCountry,
        jtfPhone,
        jtfEmail,
        jtfMobile,
        jcbIdentityType,
        jtfIdentityNo,
        jtfRoomNo,
        jcbRoomType,
        jtfArrDate,
        jtfArrTime,
        jtfDayZone,
        jcbDepartureZone,
        jcbChildren,
        jtfDepartureDate,
        jtfDepartureTime,
        jtfPrice,
        jcbClose,
        this)
        );
        jbcheck.addActionListener(new com.cloudone.hotels.controllers.ReservationListener(
        (TransactionGlassPane) getGlassPane(),        
        jcbTitle,
        jtfGuestName,
        jtaAddress,
        jtfadd2,
        jcbCountry,
        jtfPhone,
        jtfEmail,
        jtfMobile,
        jcbIdentityType,
        jtfIdentityNo,
        jtfRoomNo,
        jcbRoomType,
        jtfArrDate,
        jtfArrTime,
        jtfDayZone,
        jcbDepartureZone,
        jcbChildren,
        jtfDepartureDate,
        jtfDepartureTime,
        jtfPrice,
        jcbClose,
        this)
        );

    }
    
    @Override
    public void itemStateChanged(ItemEvent e) 
    {
      
      if(e.getSource().equals(this.jcbRoomType))
      {
       try
       {
       String RoomType= String.valueOf(jcbRoomType.getSelectedItem());   
       int Id=getRoomTypeId(RoomType);  
       List<String[]> resultset= this.getAllRoom(Id);
       jtfRoomNo.removeAllItems();
       if(resultset.isEmpty())
       {
         
       }
       else
       {
            for(int i=0; i<resultset.size();i++)
              { 
               String[] result=resultset.get(i);
                jtfRoomNo.addItem(result[0]); 
              }
       }
       String  price=getRoomTypePrice((String)jcbRoomType.getSelectedItem());
       jtfPrice.setText(price);
       jtfPrice.setEditable(false);
       if(!jtfDayZone.getText().isEmpty())       
       {
          int unitprice= Integer.parseInt(price);
          int no= Integer.parseInt(jtfDayZone.getText());
          int total= unitprice * no;   
          jtfUnitPrice.setText(String.valueOf(total));
          jtfUnitPrice.setEditable(false);
       } 
       
      
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
    }
    }
    @Override
    public void run() 
    {
        initComponents();        
    }
}


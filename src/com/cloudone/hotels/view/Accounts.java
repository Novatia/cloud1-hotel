/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

/**
 *
 * @author Akala G56
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.lang.reflect.Field;
import java.util.*;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import com.cloudone.hotels.data.*;
import com.cloudone.hotels.data.RoomTypes;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import org.jbundle.thin.base.screen.jcalendarbutton.*;
import com.cloudone.hotels.models.CITableModel;
import com.j256.ormlite.dao.GenericRawResults;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
//import org.jdesktop.swingx.JXDatePicker;
public class Accounts extends JDialog implements ActionListener{
    Box horizontal;
    double TotalCost=0;
    CITableModel model;
    Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao=null;
    Dao<com.cloudone.hotels.data.Room,Integer> RoomDao;
    Dao<com.cloudone.hotels.data.RoomTypes,Integer> RoomTypeDao;
    de.javasoft.swing.DateComboBox calendar;
    JdbcPooledConnectionSource connectionSource=null;
    
    public Accounts(){
        try
        {
        connectionSource=com.cloudone.hotels.System.getConnectionSource();
        setTitle("Reservations");        
        JPanel jp=new JPanel();
        jp.setLayout(new BorderLayout());

         horizontal=Box.createHorizontalBox();
         horizontal.add(Box.createHorizontalGlue());
         
       calendar = new de.javasoft.swing.DateComboBox ();
       calendar.addActionListener(this);
    
         horizontal.add(new JLabel("Select Date"));
         horizontal.add(calendar);
         jp.add(horizontal,BorderLayout.WEST);

        String header[]={"Guest Name","Check In Date ","Check Out Date","Reservation Date","Total Cost"};
        model=new CITableModel(header);
        com.cloudone.hotels.commons.Logging.logToFile("Retrieving Accounting data");
        SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
        Calendar today= Calendar.getInstance();
        List<RoomReservation> selfList=this.getTableData();
        
        if(!selfList.isEmpty())
        {
        for(int count=0;count<selfList.size();count++)
        {

             try
             {
    
                 Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao=DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
                 Dao<com.cloudone.hotels.data.Room,Integer> RoomDao=DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);

                 com.cloudone.hotels.data.RoomReservation busObj=selfList.get(count);
                 Guests guest=busObj.getGuest();
                 guest=GuestDao.queryForId(guest.getId());
                 com.cloudone.hotels.data.Room room=RoomDao.queryForId(busObj.getId());
                 
                 busObj.getTotalPrice();
                 busObj.getReservationDate();
                 model.addRow(Arrays.asList(guest.getFirstName(),String.valueOf(busObj.getCheckInDate()), String.valueOf(busObj.getCheckOutDate()),String.valueOf(busObj.getReservationDate()) , String.valueOf(busObj.getTotalPrice())));

             }
             catch(Exception exp)
             {
                 com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
             }
             
             
        }
        selfList.clear();
        }
        

        JTable table= new JTable(model);



        JTabbedPane tabs=new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);      
        tabs.addTab("Account Details",new JScrollPane(table));   
        this.add(tabs,BorderLayout.CENTER);  
        
        Thread thcost= new Thread(new Runnable(){
           public void run(){
              TotalCost= getTotalCost();
              java.lang.System.out.println(TotalCost);
              JTitlePaneEx jtp= new JTitlePaneEx("Total Cost (N): "+String.valueOf(TotalCost),getWidth(),30);
              add(jtp,BorderLayout.SOUTH);
           }
        }        
         );
        thcost.start();
       
        
        setSize(700,700);
        add(jp,BorderLayout.NORTH);
        com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);  
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        
        }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
    }
    public synchronized double getTotalCost()
    {     
       for(int count=0;count<model.getRowCount();count++){
           
           TotalCost=TotalCost+Double.parseDouble(String.valueOf(model.getValueAt(count, 4)));
       }
       return TotalCost;
    }
    
    private java.util.List<com.cloudone.hotels.data.RoomReservation> getTableData()
    {
        
        try
        {
        
            Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            //GuestDao=DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
            return ReservationDao.queryForAll();

        }
        catch(Exception exp)
        {
            com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
        
        return null;
    }

    @Override
    public void actionPerformed(ActionEvent e) 
    {
        
     try
     {   
       Calendar now =Calendar.getInstance();
       SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
       now.setTime(calendar.getDate());
       java.lang.System.out.println(format.format(now.getTime()));
         List<RoomReservation> selfList=this.getReservation(format.format(now.getTime())+" 00:00:00");
        
        if(!selfList.isEmpty())
        {
           model.deleteAllRows();  
        for(int count=0;count<selfList.size();count++)
        {
        
             try
             {
                 
                 Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao=DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
                 Dao<com.cloudone.hotels.data.Room,Integer> RoomDao=DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);

                 com.cloudone.hotels.data.RoomReservation busObj=selfList.get(count);
                 Guests guest=busObj.getGuest();
                 guest=GuestDao.queryForId(guest.getId());
                 com.cloudone.hotels.data.Room room=RoomDao.queryForId(busObj.getId());
                 
                 busObj.getTotalPrice();
                 busObj.getReservationDate();
            
                 model.addRow(Arrays.asList(guest.getFirstName(),String.valueOf(busObj.getCheckInDate()), String.valueOf(busObj.getCheckOutDate()),String.valueOf(busObj.getReservationDate()) , String.valueOf(busObj.getTotalPrice())));

             }
             catch(Exception exp)
             {
                 com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
             }
             
             
        }
        selfList.clear();
     }
     }catch(Exception exp){JOptionPane.showMessageDialog(this,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);}   
     
    java.lang.System.gc(); 
    }
    private List<com.cloudone.hotels.data.RoomReservation> getReservation(String date) throws Exception
    {
         
                Dao<com.cloudone.hotels.data.RoomReservation,Integer> RoomReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);                
                GenericRawResults<String[]> rawOutput =RoomReservationDao.queryRaw("select _id from roomreservation where _reserved_date>='"+date+"'");
                List<String[]>  resultset= rawOutput.getResults();
                

                if(resultset.isEmpty())
                {
                    throw new Exception("No reservation on this date");
                } 
                if(!resultset.isEmpty())
                {
                   List<com.cloudone.hotels.data.RoomReservation> list= new ArrayList();
                   for(int i=0;i<resultset.size();i++)
                   {
                      String[] Id= resultset.get(i); 
                      list.add(RoomReservationDao.queryForId(Integer.parseInt(Id[0]))); 
                   }
                    return list;   
                }
                
            
            
         return null;
    
    }
    private void getWeekDays()
    {
     try
     {
     Calendar now = Calendar.getInstance();
     SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
     int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 1; //add 2 if your week start on monday
     now.add(Calendar.DAY_OF_MONTH, delta );
       for (int i = 0; i < 7; i++)
        {
        days[i] = format.format(now.getTime());
        now.add(Calendar.DAY_OF_MONTH, 1);
        }
      
     }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
    }
    String[] days= new String[7];
}
  
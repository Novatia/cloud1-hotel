/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

/**
 *
 * @author Akala G56
 */
public class RoomProperties extends JDialog implements ActionListener{
      private String jcbPropNames[];
      private JLabel jlGuest;
      private JTextField jtf;
      private int _prevwidth;
      private int prevheight=20;
      private int xloc;
      private int yloc=10;
      private JCheckBox[] jcb;
      private JButton jbUpdate;
      private JButton jbCancel;
      private Box vertical;     
      private Box horizontal;
      private String name;
      JdbcPooledConnectionSource connectionSource=null; 
      TransactionGlassPane _pane;  
   public RoomProperties(String roomName,TransactionGlassPane  pane){
      setTitle("Room Status Information ");
      name= roomName;
      connectionSource=com.cloudone.hotels.System.getConnectionSource();
      this.setAlwaysOnTop(true);
      setResizable(false);
      setSize(500,320);
      _pane=pane;
      getContentPane().setBackground(Color.WHITE);
      
      
      JPanel jp= new JPanel();
      jp.setLayout(new BorderLayout());
      jp.setSize(this.getWidth(),150);
      jp.setLocation(0,50);
      jp.setBackground(Color.WHITE);
      jp.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
      java.util.List<com.cloudone.hotels.data.Facilities> list= getFacilities();
      if(!list.isEmpty())
      {
      
      
            jcbPropNames= new String[list.size()];
            jcb= new JCheckBox[jcbPropNames.length];
            for(int count=0;count<list.size();count++)
            {
              com.cloudone.hotels.data.Facilities facility=list.get(count);  
              jcbPropNames[count]=facility.getName();

            }

            vertical= Box.createVerticalBox();
             for(int i=0;i<jcbPropNames.length;i++){
                 vertical.add(Box.createVerticalStrut(10));
                 jcb[i]=new JCheckBox(jcbPropNames[i]);
                 vertical.add(jcb[i]);
             }
             jp.add(vertical,BorderLayout.CENTER); 
      }
      horizontal=Box.createHorizontalBox();
      horizontal.add(Box.createHorizontalGlue());
      jbUpdate= new JButton("Update");
      //jbUpdate.addActionListener();
      //horizontal.add(jbUpdate);
      
      jbCancel= new JButton("Cancel");
      jbCancel.addActionListener(this);
      horizontal.add(jbCancel); 
      
      add(jp,BorderLayout.CENTER);
      add(horizontal,BorderLayout.SOUTH);
      setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
       com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
   }
   private java.util.List<com.cloudone.hotels.data.Facilities> getFacilities()
   {
     try
     {
        java.util.List<com.cloudone.hotels.data.Facilities> busObj= new ArrayList<com.cloudone.hotels.data.Facilities>(); 
        Dao<com.cloudone.hotels.data.Facilities,Integer> FacilitiesDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Facilities.class);
        java.util.List<Integer> Id= this.getFacilitiesId();       
        for(int i=0;i<Id.size();i++)
        {        
           busObj.add(FacilitiesDao.queryForId(Id.get(i)));
        }
        return busObj;
     }
     catch(Exception exp){exp.printStackTrace();}
     
        return null;
      
   
   }
   private int getRoomId()
   {
      try
      {
        Dao<com.cloudone.hotels.data.User,String> UserDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.User.class);
        GenericRawResults<String[]> rawOutput =UserDao.queryRaw("select _id from room where _name ='"+name+"'");
        java.util.List<String[]>  resultset= rawOutput.getResults();
        if(!resultset.isEmpty())
        {
          String[] output = resultset.get(0); 
          return Integer.parseInt(output[0]);     
        }
        
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
      return -1;
   }
   private java.util.List<Integer> getFacilitiesId()
   {
     
     try
     {
        
        Dao<com.cloudone.hotels.data.User,String> UserDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.User.class);
        if(getRoomId()!=-1)
        {
         GenericRawResults<String[]> rawOutput =UserDao.queryRaw("select _facility_id from RoomFacilities where _room_id ='"+getRoomId()+"'");
         java.util.List<String[]>  resultset= rawOutput.getResults();
        
          if(!resultset.isEmpty())        
          {
           String[] output; 
           java.util.List<Integer> Id= new ArrayList<Integer>(); 
          for(int i=0;i<resultset.size();i++)
          {
            output= resultset.get(i);
            Id.add(Integer.parseInt(output[0]));
          }
          return Id;
          }
        }
        
     }
     catch(Exception exp){exp.printStackTrace();}
     
        return null;
      
   
   }

    @Override
    public void actionPerformed(ActionEvent e) {
      _pane.setVisible(false);
        dispose();
      
    }
   

   }


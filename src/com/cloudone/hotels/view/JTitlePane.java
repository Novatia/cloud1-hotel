/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

/**
 *
 * @author Akala G56
 */
import java.awt.*;
import javax.swing.*;
public class JTitlePane extends JPanel{

  private String m_title;  
  public JTitlePane(String title) 
  {
    m_title=title;
    setLayout(null);
    setSize(500,30);
    setLocation(0,0);
    setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
  }
    
 protected void paintComponent(Graphics g) {

Graphics2D g2d = (Graphics2D)g.create();

g2d.setBackground(Color.WHITE);

g2d.setFont(new Font("Calibri", Font.PLAIN, 16));
//g2d.setFont(g.getFont().
//deriveFont(Font.PLAIN, 14f));
g2d.setColor(Color.BLACK);
g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND,
BasicStroke.JOIN_MITER));

g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
RenderingHints.VALUE_ANTIALIAS_ON);

g2d.drawString(m_title, 15, 22);
g2d.dispose();
}
}

package com.cloudone.hotels.view;

/**
 * Created by Akala G56 on 30/03/14.
 */
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JComponent;

public class LoginFrame extends JFrame implements KeyListener{
    private JButton _jbLogin;
    private JTextField _jtfUsername;
    private JPasswordField _jpfPassword;
    private JLabel _jlUsername;
    private JLabel _jlPassword;

    public LoginFrame(){
        
       initComponents();
    }
    private void createUI(){
        this.setLayout(null);
        getContentPane().setBackground(Color.WHITE);
       _jlUsername=new JLabel("Username:");
       _jlUsername.setSize(100,20);
       _jlUsername.setLocation(10,20);
       add(_jlUsername);

       _jtfUsername=new JTextField();
       _jtfUsername.setSize(250,30);
       _jtfUsername.setLocation(130,20);
       add(_jtfUsername);

       _jlPassword =new JLabel("Password:");
       _jlPassword.setSize(100,20);
        _jlPassword.setLocation(10,55);
       add(_jlPassword);

       _jpfPassword=new JPasswordField();
       _jpfPassword.setSize(250,30);
       _jpfPassword.setLocation(130,55);
       add(_jpfPassword);

       _jbLogin= new JButton("Login");
       _jbLogin.setSize(70,30);
       _jbLogin.setLocation(300,100);
       _jbLogin.addActionListener(new com.cloudone.hotels.controllers.LoginListener(_jtfUsername,_jpfPassword,this));
       _jtfUsername.addKeyListener(this);
       _jpfPassword.addKeyListener(this);
       add(_jbLogin);
       
      
              

    }
    private void initComponents(){
      createUI();
      setTitle("Add Login Credentials");
      setSize(400,175);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
      setResizable(false);
      setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {
      
      try
      {
      if(!_jtfUsername.getText().isEmpty()&&_jpfPassword.getPassword().length!=0)  
      {
       if(e.getKeyCode()==KeyEvent.VK_ENTER)
       {
         _jbLogin.doClick();   
       }
      }
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
      }
    }

    @Override
    public void keyPressed(KeyEvent e) {
     try
     {
      if(!_jtfUsername.getText().isEmpty()&&_jpfPassword.getPassword().length!=0)  
      {
       if(e.getKeyCode()==KeyEvent.VK_ENTER)
       {
         _jbLogin.doClick();   
       }
      }
     }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
   
}

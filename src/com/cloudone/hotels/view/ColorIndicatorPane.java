/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author Akala G56
 */
public class ColorIndicatorPane extends JPanel{
        private  int xloc=10;
        private  int yloc=5;
        private  int gap=30;
        private  int swidth=30;
        private  int sheight=30;
        private Color colors[]={new Color(28,251,131),new Color(148,188,171),
                                new Color(240,43,1),Color.YELLOW
                                
                                };
        private String cnames[]={"Available","Unavailable",
                                 "Blocked","Reserved"};
        public ColorIndicatorPane(){
          setLayout(null);
          
        }
        public void drawIndicator( Graphics g)   {
            Graphics2D g2d = (Graphics2D)g.create(); 
           for(int i=0;i<4;i++){
                g2d.setFont(new Font("Calibri", Font.PLAIN, 16));
                g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND,
                BasicStroke.JOIN_MITER));
                
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
                g2d.drawRect(xloc, yloc,swidth, sheight);
                g2d.setColor(colors[i]);
                g2d.fillRect(xloc, yloc,swidth, sheight);
                g2d.setColor(Color.BLACK);
                g2d.drawString(cnames[i], gap+xloc+swidth, sheight);
                g2d.setBackground(Color.WHITE);
                xloc =((xloc))+gap+swidth+(cnames[i].length()*10);
            }
                   g2d.dispose();
        }
        public void paintComponent(Graphics g){
          
                xloc=10;
                yloc=5;
                gap=30;
                swidth=30;
                sheight=30;
               drawIndicator(g);
            
          
        }

}

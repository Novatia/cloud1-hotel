/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

/**
 *
 * @author Akala G56
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.*;
import com.cloudone.hotels.models.CITableModel;

public class Update extends JDialog{
   private JPanel jpNorthPane;
   private JPanel jpCenter;
   private String jbNames[]={"Check Updates","Install Updates"};
   private Box horizontal;
   private JTable table;
   private JPanel jpSouth;
   private JProgressBar jprogress;
   private static final int MAX_DELAY = 300;
   private CITableModel model;
   private JButton jbCheck;
   private JButton jbInstall;
   public Update(){
      
        setTitle("Available Updates");
        jpCenter = new JPanel();
        jpCenter.setLayout(new BorderLayout());
       
        jpNorthPane= new JPanel();
        jpNorthPane.setLayout(new BorderLayout());
        jpNorthPane.setPreferredSize(new Dimension(this.getWidth(),30));
        
        
        horizontal=Box.createHorizontalBox();
       for(int i=0;i<this.jbNames.length;i++){
           horizontal.add(Box.createHorizontalGlue());
           if(jbNames[i].equalsIgnoreCase("Install Updates"))
           {
            
              jbInstall =new JButton(this.jbNames[i]);
            jbInstall.addActionListener(new ButtonHandler());
             jbInstall.setEnabled(false);
             horizontal.add(jbInstall);
           }
           else
           {
           jbCheck =new JButton(this.jbNames[i]);
           jbCheck.addActionListener(new ButtonHandler());
           horizontal.add(jbCheck);
           }
           
       }
       
       jpSouth= new JPanel();
       jpSouth.setLayout(new BorderLayout());
       
       jprogress= new JProgressBar();
       jpSouth.add(jprogress,BorderLayout.EAST);
       
       jpNorthPane.add(horizontal,BorderLayout.EAST);
       add(jpNorthPane,BorderLayout.NORTH);
       add(jpSouth,BorderLayout.SOUTH);
       createTables();
       jpCenter.add(new JScrollPane(table));
       add(jpCenter,BorderLayout.CENTER);
               
       setSize(650,650);
       setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);       
       com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);      
       setAlwaysOnTop(true);
   
    
   }
    private class ButtonHandler implements ActionListener{

        
        public void actionPerformed(ActionEvent evt) {
           JButton jbAction= (JButton) evt.getSource();
            
           if(jbAction.getText().equalsIgnoreCase("Check Updates")){        
               Thread downloader = new Thread(new Runnable() {
            public void run() {
                int i = 0;
                do {
                    try {
                        Thread.sleep(30 + (int) (Math.random() * MAX_DELAY));
                    } catch (InterruptedException ex) {
                
                    }
                    i += (int) (Math.random() * 5);
                    model.addRow(Arrays.asList("Network Patch","1.0.0.12","26/03/2014"));
                    if(jprogress.getValue()==100)
                    {
                      jprogress.setVisible(false);
                    }
                    jprogress.setValue(i);
                          
                } while (i < 100);
                
            }
        });
        downloader.start();
        jbCheck.setEnabled(false);   
        jbInstall.setEnabled(true);
           
           }
           
       }
    }
   private void createTables(){
   
     String col[]={"Name","Version","Date"}; 
     model= new CITableModel(col);
     table = new JTable(model);
     
   }
    
}

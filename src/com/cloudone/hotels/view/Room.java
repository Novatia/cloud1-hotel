/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

/**
 *
 * @author Akala G56
 */
import com.cloudone.hotels.interfaces.Observer;
import com.cloudone.hotels.models.CITableModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;






public class Room extends JDialog  implements Observer{
    private JPanel jpNorthPane;
    private JPanel viewPane;
    private JPanel jpNorthWestPane;
    private JPanel jpSearchSouth;
    private JPanel jpNorthWestSouthPane;
    private JPanel jpSouthPane;
    private JPanel jpCenter;
    private JPanel jpSearch;
    private JTable jtbDash;
    private JTable jtbListOfRooms;
    JdbcPooledConnectionSource connectionSource=null;
    private TransactionGlassPane transactionpane;
    private JSplitPane	splitPaneH;
    private JSplitPane splitPaneV;
    private JComboBox jcbSorts;
    private String jcbValues[]={"Guest Name","Arrival Time"}; 
    private String jbNames[]={"Add","Remove"};
    private Box horizontal;
    JTabbedPane tabs;
    JPanel jptable;
     com.cloudone.hotels.models.CITableModel citlistofRooms;
    List<String> roomNames= new ArrayList();
    com.cloudone.hotels.models.CITableModel model;
    public Room(){
     connectionSource=com.cloudone.hotels.System.getConnectionSource();
      setTitle("Room View");                     
     InitComponents();
     this.setGlassPane(transactionpane=new TransactionGlassPane() );
     
   }
    
    private void InitComponents(){
       createTables();
       createPanels();
       setSize(1250,650);
       setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);       
       com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
       com.cloudone.hotels.commons.TableStateEvent.subscribe(this);
    }
    
    private  void createPanels(){
       
       JPanel contentPanel = new JPanel();
       contentPanel.setLayout( new BorderLayout() );
       getContentPane().add( contentPanel );
        
        jpCenter = new JPanel();
        jpCenter.setLayout(new BorderLayout());
       
        jpNorthPane= new JPanel();
        jpNorthPane.setLayout(new BorderLayout());
        jpNorthPane.setPreferredSize(new Dimension(this.getWidth(),30));
        
        
        
        horizontal=Box.createHorizontalBox();
       for(int i=0;i<this.jbNames.length;i++){
           horizontal.add(Box.createHorizontalGlue());
           JButton jb =new JButton(this.jbNames[i]);
           jb.addActionListener(new com.cloudone.hotels.controllers.RoomListener());
           horizontal.add(jb);
       }
       jpNorthPane.add(horizontal,BorderLayout.EAST);
      
       jpSouthPane= new JPanel();
       jpSouthPane.setLayout(new BorderLayout());
       jpSouthPane.setBackground(Color.WHITE);
       jpSouthPane.setPreferredSize(new Dimension(jpCenter.getWidth(),40));
       Border loweredBorder = BorderFactory.createLoweredBevelBorder();
       jpSouthPane.setBorder(loweredBorder);
       
       ColorIndicatorPane pane=new ColorIndicatorPane();
       pane.setBackground(Color.WHITE);
       //jpSouthPane.add(pane,BorderLayout.CENTER);
       JPanel jpSpreadSheet= new JPanel();
       jpSpreadSheet.setLayout(new BorderLayout());
       tabs=new JTabbedPane(JTabbedPane.TOP, 
                        JTabbedPane.SCROLL_TAB_LAYOUT);
       jptable= new JPanel();
       jptable.setLayout(new BorderLayout());
       jptable.add(jtbDash,BorderLayout.CENTER);
       
       tabs.addTab("Room View",jptable);
       jpSpreadSheet.add(tabs);
       jpCenter.add(jpSpreadSheet,BorderLayout.CENTER);
      // jpCenter.add(jpSouthPane,BorderLayout.SOUTH);
        
       jpNorthWestPane= new JPanel();
       jpNorthWestPane.setLayout(new BorderLayout());
       jpNorthWestPane.setPreferredSize(new Dimension(350,getHeight()));
      
       jpSearch= new JPanel();
       jpSearch.setLayout(new FlowLayout());
       jpSearch.setPreferredSize(new Dimension(300,jpNorthWestPane.getHeight()/2));
       jcbSorts=new JComboBox(jcbValues);
       //jpNorthWestPane.add(jcbSorts,BorderLayout.NORTH);
       jpNorthWestPane.add(new JScrollPane(this.jtbListOfRooms),BorderLayout.CENTER);
       
      
       splitPaneH = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT );
       splitPaneV = new JSplitPane( JSplitPane.VERTICAL_SPLIT );
       splitPaneH.setLeftComponent( jpNorthWestPane );
       splitPaneH.setRightComponent( jpCenter ); 
       splitPaneV.setTopComponent( jpNorthPane);
       splitPaneV.setBottomComponent( splitPaneH );
       contentPanel.add( splitPaneV, BorderLayout.CENTER );
       
    }
    
    
    
    private  void createTables(){ 
        
      
     String header[]={"Room Category"};
     List<com.cloudone.hotels.data.RoomTypes> selfList=getRoomCategory();
     if(selfList.isEmpty())
     {
      citlistofRooms= new com.cloudone.hotels.models.CITableModel(header);    
     jtbListOfRooms =new JTable(citlistofRooms);
   
     
     
     jtbListOfRooms.setRowHeight(50);
     
     String col[]={"floor 1","floor2"," floor 3","floor 4","floor 5"}; 
     model = new com.cloudone.hotels.models.CITableModel(col);
     jtbDash = new JTable(model);
     jtbDash.setRowSelectionAllowed(true);
     jtbDash.setColumnSelectionAllowed(true);
     jtbDash.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
     jtbDash.setCellSelectionEnabled(true);
     jtbDash.setTableHeader(null);
     jtbDash.setRowHeight(50);
     jtbListOfRooms.addMouseListener(new com.cloudone.hotels.controllers.RoomListener(this.jtbListOfRooms,jtbDash,citlistofRooms,model,this,jpCenter));      
     }
     if(!selfList.isEmpty())
     {
       citlistofRooms= new com.cloudone.hotels.models.CITableModel(header);   
       com.cloudone.hotels.data.RoomTypes bus=selfList.get(0);
       
     for(int count=0;count<selfList.size();count++)
     {
       com.cloudone.hotels.data.RoomTypes busObj=selfList.get(count);
       citlistofRooms.addRow(Arrays.asList(busObj.getName()));
     }
     
     jtbListOfRooms =new JTable(citlistofRooms);
     jtbListOfRooms.setRowHeight(50);
     
     List<String[]>  resultset=this.getAllRoomNames(bus.getId());
     if(!resultset.isEmpty())
     {     
        for(int i=0;i<resultset.size();i++)
        {
           String[] output=resultset.get(i);
           roomNames.add(output[0]);
        }
        
     String col[]={"floor 1","floor2"," floor 3","floor 4","floor 5"}; 
     model = new com.cloudone.hotels.models.CITableModel(col);
     jtbListOfRooms.addMouseListener(new com.cloudone.hotels.controllers.RoomListener(this.jtbListOfRooms,jtbDash,citlistofRooms,model,this,jptable));
     List<com.cloudone.hotels.data.RoomTypes> types= this.getRoomCategory();
     //addAllRoomNames(this.getRoomCategoryId(types.get(0).getName()));  
     presentData();
     roomNames.clear();
     jtbDash = new JTable(model);
     jtbDash.setRowSelectionAllowed(true);
     jtbDash.setColumnSelectionAllowed(true);
     jtbDash.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
     jtbDash.setCellSelectionEnabled(true);
     jtbDash.setTableHeader(null);
     jtbDash.setRowHeight(50);
     com.cloudone.hotels.util.Utilities.TableUtilities.setColumnWidths(jtbDash, new Insets(10, 10, 10, 10),true, false);
    jtbDash.addMouseListener(new MouseAdapter() {
     
     public void mouseClicked(MouseEvent e) {
      if (e.getClickCount() == 2) {
         JTable target = (JTable)e.getSource();
         int row = target.getSelectedRow();
         int column = target.getSelectedColumn();
        
         String value =   (String) jtbDash.getValueAt(row, column);    
         if(!value.isEmpty())
         {
            getGlassPane().setVisible(true);
            
            RoomProperties rp = new RoomProperties(value,(TransactionGlassPane)getGlassPane());
            rp.addWindowListener(new WindowAdapter(){         
                 public void windowClosing(WindowEvent e) 
                 {
                   getGlassPane().setVisible(false);

                 }
            });
            rp.setVisible(true);                           
         }
        }
   }
      }); 
    
    }
     
   if(resultset.isEmpty())
   {
      String col[]={"floor 1","floor2"," floor 3","floor 4","floor 5"}; 
     model = new com.cloudone.hotels.models.CITableModel(col);       
     jtbDash = new JTable(model);
     jtbDash.setRowSelectionAllowed(true);
     jtbDash.setColumnSelectionAllowed(true);
     jtbDash.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
     jtbDash.setCellSelectionEnabled(true);
     jtbDash.setTableHeader(null);
     jtbDash.setRowHeight(50);
   
   
    }
  }
 }
    
    private void presentData()
    {
      int j=0,i=0;
        if(roomNames.size()%5==0)
        {
          for(;;)
          {
            model.addRow(Arrays.asList(roomNames.get(j),roomNames.get(++j),roomNames.get(++j),roomNames.get(j++),roomNames.get(++j)));
            if(roomNames.size()==j+1)
              {
                j=0;
                break;
              }
            ++j;
          }
        }
       if(roomNames.size()%5!=0)
           
        {
          int x=this.findMultiple(roomNames.size());
          
          for(int y=0;y<x;y++)
          {roomNames.add("");
          }
          for(;;)
          {
           model.addRow(Arrays.asList(roomNames.get(i),roomNames.get(++i),roomNames.get(++i),roomNames.get(++i),roomNames.get(++i)));
           
           if(roomNames.size()==i+1)
           {
             i=0;
             break;
            }
           ++i;
          }
        }
    
    }
    private List<com.cloudone.hotels.data.RoomTypes>  getRoomCategory()
    {
             
             try
             {
             
                 Dao<com.cloudone.hotels.data.RoomTypes,String> RoomTypesDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
                 return RoomTypesDao.queryForAll(); 
                 
             }
             catch(Exception exp)
             {
                 exp.printStackTrace();
             }
             
     return null;        
 
    }    
    
    private int findMultiple(int size)
    { 
       int i=0;
       for(;;)
        {
          int sum=size+i;    
          if((sum%5)==0)
          {
            return i;              
          }    
           i=i+1;
        }
       
    }
    private List<String[]>  getAllRoomNames(int id)
    {
             
             try
             {
             
               Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
               GenericRawResults<String[]> rawOutput =RoomDao.queryRaw("select _name from room where _room_type_id ='"+id+"'");
                return rawOutput.getResults();
             }
             catch(Exception exp)
             {
                 exp.printStackTrace();
             }
             
     return null;        
 
    }    

    private void addAllRoomNames(int categoryId)
    {
             
             try
             {
              
                 Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
                 GenericRawResults<String[]> rawOutput =RoomDao.queryRaw("select _name from room where _room_type_id ='"+String.valueOf(categoryId)+"'");
                 List<String[]>  resultset= rawOutput.getResults();
                 if(!resultset.isEmpty())
                 {
                 for(int i=0;i<resultset.size();i++)
                 {
                    String[] output = resultset.get(i);
                    if(output[0]==null)
                    {
                      
                      
                      com.cloudone.hotels.commons.Logging.logToFile("Invalid room name");
                    }
                    else
                     {
                       roomNames.add(output[0]);  
                     }
                 }
                 }
             }                 
             catch(Exception exp)
             {
                      com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
             }
             
        
        
    }
   private int  getRoomCategoryId(String name)
    {
             
             try
             {
             
                 Dao<com.cloudone.hotels.data.RoomTypes,Integer> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
                 GenericRawResults<String[]> rawOutput =RoomTypeDao.queryRaw("select _id from roomtypes where _name ='"+name+"'");
                 List<String[]>  resultset= rawOutput.getResults();
                 if(!resultset.isEmpty())
                 {
                   String[] output = resultset.get(0);
                  if(output[0]==null)
                  {
                    com.cloudone.hotels.commons.Logging.logToFile("roomtype id not found");
                  }
               else
                {
                  return Integer.parseInt(output[0]);
                }
             }
             }
             catch(Exception exp)
             {
                com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
             }
             
      return -1;
    }    
       @Override
    public void update() 
    {
       Thread thread= new Thread(new Runnable() {

            @Override
            public void run() {
             List<com.cloudone.hotels.data.RoomTypes> types= getRoomCategory();     
        if(!types.isEmpty())
          {
           addAllRoomNames(getRoomCategoryId(types.get(0).getName()));  
           model.deleteAllRows();
           presentData();
           roomNames.clear();
      
          }
      List<com.cloudone.hotels.data.RoomTypes> selfList=getRoomCategory();
      if(!selfList.isEmpty())
      {
        com.cloudone.hotels.data.RoomTypes bus=selfList.get(0);      
        citlistofRooms.deleteAllRows();
        for(int count=0;count<selfList.size();count++)
         {
           com.cloudone.hotels.data.RoomTypes busObj=selfList.get(count);
           citlistofRooms.addRow(Arrays.asList(busObj.getName()));
           
         }
      }
      model.fireTableStructureChanged();        
      citlistofRooms.fireTableStructureChanged();     
      
      jtbDash.invalidate();
      jpCenter.repaint();             
      jptable.invalidate();
      repaint();
            }
        }
               
               
        ); thread.start();
        
      
        
     
       
    }
    
}



 

package com.cloudone.hotels.view;

/**
 * Created by Akala G56 on 31/03/14.
 */
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class NewUserSetup extends JDialog {

  private JLabel _jlfirstname;
  private JLabel _jllastname;
  private JLabel _jlusername;
  private JLabel _jlpassword;
  private JLabel _jlconfirm;
  private JLabel _jlrole;
  private JTextField _jtffirstname;
  private JTextField _jtflastname;
  private JTextField _jtfusername;
  private JPasswordField _jpfpassword;
  private JPasswordField _jpfconfirm;
  private JComboBox _jcbrole;
  private String[] _jcbstring={"administrator","clerk"};
  private JButton _jbSubmit;
  

    public NewUserSetup(){
        initComponents();
    }
    private void initComponents(){
       createUI();
    }
    private void createUI(){
        setLayout(null);
        setTitle("Add New User");
        getContentPane().setBackground(java.awt.Color.WHITE);
       _jlfirstname= new JLabel("FirstName");
       _jlfirstname.setSize(100,20);
       _jlfirstname.setLocation(10,10);
       add(_jlfirstname);

       _jtffirstname= new JTextField();
       _jtffirstname.setSize(250,30);
       _jtffirstname.setLocation(130, 10);
       add(_jtffirstname);

       _jllastname= new JLabel("LastName");
       _jllastname.setSize(100,20);
       _jllastname.setLocation(10,55);
       add(_jllastname);

        _jtflastname= new JTextField();
        _jtflastname.setSize(250,30);
        _jtflastname.setLocation(130, 55);
        add(_jtflastname);


        _jlusername= new JLabel("Username");
        _jlusername.setSize(100, 20);
        _jlusername.setLocation(10,95);
        add(_jlusername);

        _jtfusername= new JTextField();
        _jtfusername.setSize(250,30);
        _jtfusername.setLocation(130, 95);
        add(_jtfusername);



        _jlpassword= new JLabel("Password");
        _jlpassword.setSize(100,20);
        _jlpassword.setLocation(10, 135);
        add(_jlpassword);

        _jpfpassword= new JPasswordField();
        _jpfpassword.setSize(250, 30);
        _jpfpassword.setLocation(130, 135);
        add(_jpfpassword);

        _jlconfirm= new JLabel("Confirm Password");
        _jlconfirm.setSize(170,20);
        _jlconfirm.setLocation(10, 175);
        add(_jlconfirm);

        _jpfconfirm= new JPasswordField();
        _jpfconfirm.setSize(250,30);
        _jpfconfirm.setLocation(130, 175);
        add(_jpfconfirm);

        _jlrole= new JLabel("Role");
        _jlrole.setSize(100,20);
        _jlrole.setLocation(10,210);
        add(_jlrole);

        _jcbrole= new JComboBox(_jcbstring);
        _jcbrole.setSize(250,30);
        _jcbrole.setLocation(130,210);
        add(_jcbrole);

        _jbSubmit=new JButton("Create");
        _jbSubmit.setSize(80,30);
        _jbSubmit.setLocation(300,250);
        _jbSubmit.addActionListener(new com.cloudone.hotels.controllers.UserSetupListener(_jtffirstname,_jtflastname,_jtfusername,_jpfpassword,_jpfconfirm,_jcbrole,this));
        add(_jbSubmit);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setSize(400,340);
        com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
        setVisible(true);

    }
}

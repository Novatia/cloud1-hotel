/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

/**
 *
 * @author Akala G56
 */
import java.awt.*;
import javax.swing.*;
public class JTitlePaneEx extends JPanel{

  private String m_title;  
  public JTitlePaneEx(String title, int width, int height) 
  {
    m_title=title;
    setLocation(0,0);
    this.setPreferredSize(new Dimension(width,height));
    setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
  }
    
 protected void paintComponent(Graphics g) {

Graphics2D g2d = (Graphics2D)g.create();

g2d.setBackground(Color.WHITE);

g2d.setFont(new Font("Calibri", Font.PLAIN, 16));
//g2d.setFont(g.getFont().
//deriveFont(Font.PLAIN, 14f));
g2d.setColor(Color.BLACK);
g2d.setStroke(new BasicStroke(2f, BasicStroke.CAP_ROUND,
BasicStroke.JOIN_MITER));

g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
RenderingHints.VALUE_ANTIALIAS_ON);

g2d.drawString(m_title, getWidth()-250, 22);
g2d.dispose();
}
}

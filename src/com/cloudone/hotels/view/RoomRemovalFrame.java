package com.cloudone.hotels.view;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.util.List;
import javax.swing.*;

/**
 * Created by Akala G56 on 04/04/14.
 */

public class RoomRemovalFrame extends JDialog{
    private JLabel _jlroomname;
    private JLabel _jlcategory;
    private JComboBox _jcbcategory;
    private String[] _jcbstring={"King"};
    private JTextField _jtfroomname;
    private JButton _jbSubmit;
    private JTextField _jtfrmprop;

    public RoomRemovalFrame(){
        
        addMouseListener(new MouseAdapter() { });
        addMouseMotionListener(new MouseMotionAdapter() { });
        addKeyListener(new KeyAdapter() { });
        
        addComponentListener(new ComponentAdapter() {
        public void componentShown(ComponentEvent evt) {
         requestFocusInWindow();
        }
       });
       setFocusTraversalKeysEnabled(false);
        initComponents();
    }
    private void initComponents(){
        createUI();
    }
    private void createUI(){
        setLayout(null);
        setTitle("Remove Room");
        getContentPane().setBackground(java.awt.Color.WHITE);
        _jlroomname= new JLabel("Room Name");
        _jlroomname.setSize(100,20);
        _jlroomname.setLocation(10,10);
        add(_jlroomname);

        
        _jtfroomname= new JTextField();
        _jtfroomname.setSize(250,30);
        _jtfroomname.setLocation(130, 10);
        add(_jtfroomname);

        
        
        
        _jlcategory= new JLabel("Room Category");
        _jlcategory.setSize(100,20);
        _jlcategory.setLocation(10,55);
        add(_jlcategory);

          
        _jcbcategory= new JComboBox();
        List<com.cloudone.hotels.data.RoomTypes> roomTypes=getRoomCategory();
        for(int i=0;i<roomTypes.size();i++)
        {
          com.cloudone.hotels.data.RoomTypes roomtype=roomTypes.get(i);  
         _jcbcategory.addItem(roomtype.getName());
        }
        _jcbcategory.setSize(250,30);
        _jcbcategory.setLocation(130,55);
        add(_jcbcategory);


        _jbSubmit=new JButton("Remove");
        _jbSubmit.setSize(80,30);
        _jbSubmit.setLocation(300,125);
        _jbSubmit.addActionListener(new com.cloudone.hotels.controllers.RoomUpdateListener(_jtfroomname,_jcbcategory,this));
        add(_jbSubmit);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setSize(400,200);
        com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
        setVisible(true);

    }
      private List<com.cloudone.hotels.data.RoomTypes>  getRoomCategory()
    {
             JdbcPooledConnectionSource connectionSource=null;
             try
             {
                 connectionSource=com.cloudone.hotels.System.getConnectionSource();
                 Dao<com.cloudone.hotels.data.RoomTypes,String> RoomTypesDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
                 return RoomTypesDao.queryForAll(); 
                 
             }
             catch(Exception exp)
             {
                 exp.printStackTrace();
             }
             finally
             {
                 try
                 {
                     connectionSource.close();
                 }catch(Exception exp){exp.printStackTrace();}
             }
     return null;        
 
    }    

}

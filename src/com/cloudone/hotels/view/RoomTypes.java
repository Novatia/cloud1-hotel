/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

/**
 *
 * @author Akala G56
 */
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import javax.swing.*;
import javax.swing.table.TableColumnModel;
import com.cloudone.hotels.models.CITableModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

public class RoomTypes  extends JDialog implements MouseListener,EventListener,WindowListener,ComponentListener,FocusListener{
    private JPanel jpNorthPane;
    private JPanel jpCenter;
    JButton jbAdd;
    private Box horizontal;
    private JTable table;
    private JTextField jtfRoomName;
    private JTextField jtfRoomPrice;
    private JTextField jtfMaxGuest;
    private CITableModel model;
    private com.cloudone.hotels.controllers.RoomTypeListener listener;
    private List<Integer> typeId =new ArrayList<Integer>();
    JdbcPooledConnectionSource connectionSource=null;
    public RoomTypes(){
        connectionSource=com.cloudone.hotels.System.getConnectionSource();
        setTitle("Add or Remove Room Category");
        jpCenter = new JPanel();
        jpCenter.setLayout(new BorderLayout());

        jpNorthPane= new JPanel();
        jpNorthPane.setLayout(new BorderLayout());
        jpNorthPane.setPreferredSize(new Dimension(this.getWidth(),30));
        jtfRoomName= new JTextField();
        jtfRoomName.setPreferredSize(new Dimension(150,25));
        jtfRoomPrice= new JTextField();
        jtfRoomPrice.setPreferredSize(new Dimension(150,25));
        jtfMaxGuest= new JTextField();
        jtfMaxGuest.setPreferredSize(new Dimension(150,25));
        horizontal=Box.createHorizontalBox();
        horizontal.add(Box.createHorizontalGlue());
        horizontal.add(new JLabel("Maximum Guest No"));
        horizontal.add(jtfMaxGuest);
        horizontal.add(new JLabel("Room Type Name"));
        horizontal.add(jtfRoomName);
        horizontal.add(new JLabel("Room Type Price"));
        horizontal.add(jtfRoomPrice);
       jbAdd =new JButton("Add");
       horizontal.add(jbAdd);
       jpNorthPane.add(horizontal,BorderLayout.EAST);
       add(jpNorthPane,BorderLayout.NORTH);
       createTables();
       jbAdd.addActionListener(listener);
       jpCenter.add(new JScrollPane(table));
       add(jpCenter, BorderLayout.CENTER);
       setSize(850,650);
       setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
       com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
       setAlwaysOnTop(true);
   }

   private void createTables(){

     String col[]={"Maximum Guest No","Name","Price"};
     model= new CITableModel(col);
     table = new JTable(model);
     List<com.cloudone.hotels.data.RoomTypes> selfList=getTableData();
     for(int count=0;count<selfList.size();count++)
     {
       com.cloudone.hotels.data.RoomTypes busObj=selfList.get(count);
       typeId.add(busObj.getId());
       model.addRow(Arrays.asList(String.valueOf(busObj.getMaxGuestNumber()),busObj.getName(),String.valueOf(busObj.getPrice())));
     }
     
     listener= new com.cloudone.hotels.controllers.RoomTypeListener(jtfMaxGuest,jtfRoomName,jtfRoomPrice,model,table,this);
     table.setRowSelectionAllowed(true);
     table.setColumnSelectionAllowed(true);
     table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
     table.setCellSelectionEnabled(true);   
     table.addMouseListener(listener);
}
 private List<com.cloudone.hotels.data.RoomTypes> getTableData()
 {
       
             try
             {
       
                 Dao<com.cloudone.hotels.data.RoomTypes,String> RoomTypesDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
                 return RoomTypesDao.queryForAll(); 
                 
             }
             catch(Exception exp)
             {
                 exp.printStackTrace();
             }
             
     return null;        
 }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.setFocusableWindowState(true);        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.setFocusableWindowState(true);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.setFocusableWindowState(true);
        this.setFocusable(true);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        this.setFocusableWindowState(true);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        this.setFocusableWindowState(true);
    }

    @Override
    public void windowOpened(WindowEvent e) {
        this.setFocusableWindowState(true);
    }

    @Override
    public void windowClosing(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void windowClosed(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void windowIconified(WindowEvent e) {
        this.setFocusableWindowState(true);
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        
    }

    @Override
    public void windowActivated(WindowEvent e) {
        this.setFocusableWindowState(true);
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void componentResized(ComponentEvent e) {
        
    }

    @Override
    public void componentMoved(ComponentEvent e) {
        
    }

    @Override
    public void componentShown(ComponentEvent e) {
        this.setFocusableWindowState(true);
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        
    }

    @Override
    public void focusGained(FocusEvent e) {
        this.setFocusableWindowState(true);
    }

    @Override
    public void focusLost(FocusEvent e) {
        
        
        this.setFocusable(true);
        this.setFocusableWindowState(true);
    }
}


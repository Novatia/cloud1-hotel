package com.cloudone.hotels.view;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.List;
import javax.swing.*;

/**
 * Created by Akala G56 on 04/04/14.
 */

public class RoomUpdateFrame extends JDialog implements MouseListener,MouseMotionListener,ComponentListener{
    private JLabel _jlroomname;
    private JLabel _jlcategory;
    private JComboBox _jcbcategory;
    private String[] _jcbstring={"King"};
    private JTextField _jtfroomname;
    private JTextField _jtfrmprop;
    private JButton _jbSubmit;
    JdbcPooledConnectionSource connectionSource=null;

    public RoomUpdateFrame(){
        
        connectionSource=com.cloudone.hotels.System.getConnectionSource();
        addMouseListener(this);
        addMouseMotionListener(this);
        addKeyListener(new KeyAdapter() { });
        
        addComponentListener(this);
       setFocusTraversalKeysEnabled(false);
        initComponents();
    }
    private void initComponents(){
        createUI();
    }
    private void createUI(){
        setLayout(null);
        setTitle("Add Room");
        getContentPane().setBackground(java.awt.Color.WHITE);
        _jlroomname= new JLabel("Room Name");
        _jlroomname.setSize(100,20);
        _jlroomname.setLocation(10,10);
        add(_jlroomname);

        
        _jtfroomname= new JTextField();
        _jtfroomname.setSize(250,30);
        _jtfroomname.setLocation(130, 10);
        add(_jtfroomname);

        JLabel _jlprop= new JLabel("Room Property");
        _jlprop.setSize(100,20);
        _jlprop.setLocation(10,55);
        add(_jlprop);

        
        _jtfrmprop= new JTextField();
        _jtfrmprop.setSize(250,30);
        _jtfrmprop.setLocation(130, 55);
        add(_jtfrmprop);

        
        
        
        _jlcategory= new JLabel("Room Category");
        _jlcategory.setSize(100,20);
        _jlcategory.setLocation(10,100);
        add(_jlcategory);

          
        _jcbcategory= new JComboBox();
        List<com.cloudone.hotels.data.RoomTypes> roomTypes=getRoomCategory();
        for(int i=0;i<roomTypes.size();i++)
        {
          com.cloudone.hotels.data.RoomTypes roomtype=roomTypes.get(i);  
         _jcbcategory.addItem(roomtype.getName());
        }
        _jcbcategory.setSize(250,30);
        _jcbcategory.setLocation(130,100);
        add(_jcbcategory);


        _jbSubmit=new JButton("Save");
        _jbSubmit.setSize(80,30);
        _jbSubmit.setLocation(300,135);
        _jbSubmit.addActionListener(new com.cloudone.hotels.controllers.RoomUpdateListener(_jtfroomname,_jtfrmprop,_jcbcategory,this));
        add(_jbSubmit);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setSize(400,200);
        com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
        setVisible(true);

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        requestFocusInWindow();
        this.setAlwaysOnTop(true);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        requestFocusInWindow();
        this.setAlwaysOnTop(true);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        requestFocusInWindow();
        this.setAlwaysOnTop(true);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        requestFocusInWindow();
        this.setAlwaysOnTop(true);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        requestFocusInWindow();
        this.setAlwaysOnTop(true);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        requestFocusInWindow();
        this.setAlwaysOnTop(true);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        requestFocusInWindow();
        this.setAlwaysOnTop(true);
    }

    @Override
    public void componentResized(ComponentEvent e) {

    }

    @Override
    public void componentMoved(ComponentEvent e) {

    }

    @Override
    public void componentShown(ComponentEvent e) {
        requestFocusInWindow();
        this.setAlwaysOnTop(true);
    }

    @Override
    public void componentHidden(ComponentEvent e) {
        this.setAlwaysOnTop(true);
    }
     private List<com.cloudone.hotels.data.RoomTypes>  getRoomCategory()
    {
             
             try
             {
                 
                 Dao<com.cloudone.hotels.data.RoomTypes,String> RoomTypesDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
                 return RoomTypesDao.queryForAll(); 
                 
             }
             catch(Exception exp)
             {
                 exp.printStackTrace();
             }
             
     return null;        
 
    }    
    
   
}

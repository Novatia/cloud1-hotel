/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.view;

/**
 *
 * @author Akala G56
 */
import com.cloudone.hotels.commons.Date;
import com.cloudone.hotels.data.RoomReservation;
import com.cloudone.hotels.interfaces.Observer;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class ReservationDashBoard extends JFrame implements Observer{
    private JMenu jmRoom;
    private JMenuItem jmiRoomType;
    private JMenuItem jmiRoom;
    private JMenu jmSettings;
    private JMenu jmUser;
    private JMenu jmReservation;
    private JPanel jpNorthPane;
    private JMenuBar jmb;
    private JPanel viewPane;
    private JPanel jpNorthWestPane;
    private JLabel jlStatus;
    private JComboBox jcbSorts;
    private JPanel jpNorthWestSouthPane;
    private String jcbValues[]={"Guest Name"}; 
    JTable tbr;
    JPanel jpSearchSouth;
    private String jbNames[]={"Check In","Logout","Check Out","Cancel","Check In Reservation"};
    Object[][] tableHeaders;
    JButton jb[]= new JButton[5];
    private Box horizontal;
    private Box horizontalpane;
    private JPanel jpSouthPane;
    private JSplitPane	splitPaneH;
    private JSplitPane splitPaneV;
    private JTable jtbDash;
    private JTable jtbSearch;
    private JPanel jpCenter;
    private JPanel jpSearch;
    private List<String> typestatus;
    private com.cloudone.hotels.models.MultiLineHeaderRenderer headerRenderer;
    TransactionGlassPane transactionpane;
    private boolean IsStateChanged=false; 
    private List<Integer> rowId= new ArrayList<Integer>();
    private Map<Integer,String[]> map;
    private List<String> roomstatus= new ArrayList<String>();
    private String days[]= new String[7];
    private com.cloudone.hotels.controllers.CheckInListener checkstatuslistener;
    private com.cloudone.hotels.models.CITableModel _model; 
    private com.cloudone.hotels.models.CITableModelEx mo;
    private com.cloudone.hotels.models.CITableModel models;    
    private JCheckBox checkBox;
    JPanel contentPanel;
    private TableColumnModel tcm;
    private JTableHeader header;
    private List<com.cloudone.hotels.data.RoomReservation> weeksReservation;
    Thread thread,tablethread,panelthread;
    private SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
   JdbcPooledConnectionSource connectionSource=null;
    public ReservationDashBoard(){
       
       super("CloudOne Hotel Reservation System");               
      
       this.InitComponents();
    }
    
    private void InitComponents()
    {
                
      try
      {
         
          connectionSource=com.cloudone.hotels.System.getConnectionSource();        
         createMenus();
         createTableHeaders();
         weeksReservation=getWeeksReservation(days[0], days[6]);      
         createTables();
         createReservationTable();       
         createPanels();
          setSize(1350,750);
        setDefaultCloseOperation(3);
        setGlassPane(transactionpane = new TransactionGlassPane());
        com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);
       com.cloudone.hotels.commons.TableStateEvent.subscribe(this);
         setVisible(true);
      }catch(Exception exp){System.out.println(exp.getClass()+""+exp.getStackTrace()); }
      
    }
    private  void createPanels() 
    {
      try
      {
        connectionSource=com.cloudone.hotels.System.getConnectionSource();  
        contentPanel = new JPanel();
        
        contentPanel.setLayout(new BorderLayout());
        contentPanel.setBackground(Color.WHITE);
        getContentPane().add( contentPanel );
        
        jpCenter = new JPanel();
        jpCenter.setLayout(new BorderLayout());
       
        jpNorthPane= new JPanel();
        jpNorthPane.setLayout(new BorderLayout());
        jpNorthPane.setPreferredSize(new Dimension(this.getWidth(),30));
        
        
        
        horizontal=Box.createHorizontalBox();
        checkstatuslistener= new com.cloudone.hotels.controllers.CheckInListener(this,tbr,rowId,mo);
        tbr.addMouseListener(checkstatuslistener);
       for(int i=0;i<this.jbNames.length;i++){
           horizontal.add(Box.createHorizontalGlue());
           jb[i]=new JButton(this.jbNames[i]);
           horizontal.add(jb[i]);
       }
       jb[0].addActionListener(checkstatuslistener);
       jb[2].addActionListener(checkstatuslistener);
       jb[3].addActionListener(checkstatuslistener);
       jb[4].addActionListener(checkstatuslistener);
       com.cloudone.hotels.controllers.LogoutListener logout= new com.cloudone.hotels.controllers.LogoutListener(this);
      jb[1].addActionListener(logout);
        
       jpNorthPane.setBackground(Color.WHITE);
       jpNorthPane.add(horizontal,BorderLayout.EAST);
       
       jpSouthPane= new JPanel();
       jpSouthPane.setLayout(new BorderLayout());
       jpSouthPane.setBackground(Color.WHITE);
       jpSouthPane.setPreferredSize(new Dimension(jpCenter.getWidth(),40));
       Border loweredBorder = BorderFactory.createLoweredBevelBorder();
       jpSouthPane.setBorder(loweredBorder);
       
       ColorIndicatorPane pane=new ColorIndicatorPane();
       pane.setBackground(Color.WHITE);
       jpSouthPane.add(pane,BorderLayout.CENTER);
       JPanel jpSpreadSheet= new JPanel();
       jpSpreadSheet.setLayout(new BorderLayout());
       JTabbedPane tabs=new JTabbedPane(JTabbedPane.TOP, 
                        JTabbedPane.SCROLL_TAB_LAYOUT);

       JPanel jptxn= new JPanel();
       jptxn.setLayout(new BorderLayout());
       //java.lang.System.out.println(jptxn.isRequestFocusEnabled());
       jptxn.add( new JScrollPane(tbr),BorderLayout.CENTER);
  
       tabs.addTab("Reservation Calendar",new JScrollPane(jtbDash));
       tabs.addTab("Reservation Transactions",jptxn);    
       jpSpreadSheet.add(tabs);
       jpSpreadSheet.setBackground(Color.WHITE);
       jpCenter.add(jpSpreadSheet,BorderLayout.CENTER);
       jpCenter.setBackground(Color.WHITE);
       jpCenter.add(jpSouthPane,BorderLayout.SOUTH);
        
       jpNorthWestPane= new JPanel();
       jpNorthWestPane.setLayout(new BorderLayout());
       jpNorthWestPane.setPreferredSize(new Dimension(350,getHeight()));
      
      
       
       jpSearch= new JPanel();
       jpSearch.setBackground(Color.WHITE);
       jpSearch.setLayout(new FlowLayout());
       jpSearch.setPreferredSize(new Dimension(300,jpNorthWestPane.getHeight()/2));
       jcbSorts=new JComboBox(jcbValues);
       jpNorthWestPane.add(jcbSorts,BorderLayout.NORTH);
       jpNorthWestPane.add(new JScrollPane(jtbSearch),BorderLayout.CENTER);
       
       jpSearchSouth=new JPanel();
       jpSearchSouth.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
       jpSearchSouth.setBackground(Color.WHITE);
       jpSearchSouth.setLayout(null);
       jpSearchSouth.setPreferredSize(new Dimension(300,220));
       JTitlePane jtp= new JTitlePane("Search");
       jtp.setBackground(Color.WHITE);
       //jpSearchSouth.add(jtp);
       
       JLabel jlFilter=new JLabel("Filter By");
       jlFilter.setSize(50, 20);
       jlFilter.setLocation(20,40);

       JComboBox jcb=new JComboBox(jcbValues);
       jcb.setSize(200, 30);
       jcb.setLocation((jlFilter.getX()+(jcb.getWidth()/2)),40);
       //jpSearchSouth.add(jlFilter);
       //jpSearchSouth.add(jcb);
       
       JLabel jlTerm=new JLabel("Term");
       jlTerm.setSize(50, 20);
       jlTerm.setLocation(20,85);

       JTextField jtfSearch = new JTextField("Search");
       jtfSearch.setSize(400, 30);
       com.cloudone.hotels.controllers.SearchCaretListener listener =new com.cloudone.hotels.controllers.SearchCaretListener(jtfSearch,this.jtbSearch,models);
       jtfSearch.addCaretListener(listener);
       jtfSearch.addKeyListener(listener);
       jtfSearch.setLocation(0,40);
       
      
       JButton jbSearch=new JButton("Search");
       jbSearch.setSize(80, 30);
       jbSearch.setLocation((40+(jcb.getWidth())),120);
      // jpSearchSouth.add(jlTerm);
       jpSearchSouth.add(jtfSearch);
      // jpSearchSouth.add(jbSearch);
       
       jpNorthWestPane.add(jpSearchSouth,BorderLayout.SOUTH); 
       splitPaneH = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT );
       splitPaneH.setBackground(Color.WHITE);
       splitPaneV = new JSplitPane( JSplitPane.VERTICAL_SPLIT );
       splitPaneV.setBackground(Color.WHITE);     
       splitPaneH.setLeftComponent( jpNorthWestPane );
       
       
       splitPaneH.setRightComponent( jpCenter ); 
       splitPaneV.setTopComponent( jpNorthPane);
       splitPaneV.setBottomComponent( splitPaneH );
       contentPanel.add( splitPaneV, BorderLayout.CENTER );
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
      }
    }
    public void createReservationTable() throws Exception
    {
//    
        try
        {
        String headers[]={"Guest Name","Room Name","Reserved Date","No of Nights","No of Guests","Total Price","Check In","Check Out Date","Status","Boolean"};
        mo=new com.cloudone.hotels.models.CITableModelEx(headers);
        
       List<com.cloudone.hotels.data.RoomReservation> reserve=getAllReservations();
        
       if(!reserve.isEmpty())
        {
         for(int count=reserve.size()-1;count>=0;count--)
           {
             com.cloudone.hotels.data.RoomReservation  busObj=reserve.get(count);
             com.cloudone.hotels.data.Guests guest=busObj.getGuest();
             guest=getGuest(guest.getId());
             com.cloudone.hotels.data.Room room=busObj.getRoom();
             room= getRoom(room.getId());
             if(guest==null)
              {
             com.cloudone.hotels.commons.Logging.logToFile("com.cloudone.hotels.exceptions.GuestNotFoundException");
                  
              }
            if(room==null)
              {
                com.cloudone.hotels.commons.Logging.logToFile("Room Not Found Exception");
              }
              rowId.add(busObj.getId());
              String name;
              String roomname;
              String reserveddate;
              String nights;                  
              String total;
              String checkIn;
              String checkOut;
              if(guest.getFirstName().isEmpty()){name="";}else{name=guest.getFirstName();}
              if(room.getName().isEmpty()){roomname="";}else{roomname=room.getName();}
              if(formatter.format(busObj.getReservationDate()).isEmpty()){reserveddate="";}else{reserveddate=formatter.format(busObj.getReservationDate());}
              if(String.valueOf(busObj.getNights()).isEmpty()){nights="";}else{nights=String.valueOf(busObj.getNights());}
              if(String.valueOf(busObj.getTotalPrice()).isEmpty()){total="";}else{total=String.valueOf(busObj.getTotalPrice());}
              if(formatter.format(busObj.getCheckInDate()).isEmpty()){checkIn="";}else{checkIn=formatter.format(busObj.getCheckInDate());}
              if(formatter.format(busObj.getCheckOutDate()).isEmpty()){checkOut="";}else{checkOut=formatter.format(busObj.getCheckOutDate());}
              mo.addRow(Arrays.asList(name,roomname,reserveddate,nights,nights,total,checkIn,checkOut,busObj.getStatus(),Boolean.FALSE));
           }
        
            reserve.clear();
         
        
        }
       
       else{com.cloudone.hotels.commons.Logging.logToFile("Empty Reservation List");}
       tbr = new JTable(mo);     
       tbr.setRowHeight(20);
       tbr.setRowSelectionAllowed(true);
       tbr.setColumnSelectionAllowed(true);
       tbr.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
       tbr.setCellSelectionEnabled(true);
       
       }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
    }
    
    private void createMenus()
    {
      jmRoom= new JMenu("Rooms");
      jmRoom.setMnemonic('R');
      
      
      jmiRoomType = new JMenuItem("Room Types");
      jmiRoomType.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
              java.awt.EventQueue.invokeLater(new Runnable() {
               public void run() {
                  new RoomTypes().setVisible(true);
                }
           });       
            }
      }); 
       
      jmiRoom = new JMenuItem("Rooms");
      jmiRoom.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
              java.awt.EventQueue.invokeLater(new Runnable() {
               public void run() {
                  new Room().setVisible(true);
                }
           });       
              
       
       
            }
      });
      
      
      jmRoom.add(jmiRoomType);
      jmRoom.add(jmiRoom);
              
      jmSettings= new JMenu("Settings");
      jmSettings.setMnemonic('S');
      JMenuItem jmiUpdate = new JMenuItem("Updates");
      jmiUpdate.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
              java.awt.EventQueue.invokeLater(new Runnable() {
               public void run() {
                  new Update().setVisible(true);
                }
           });       
            }
      });
      
      jmSettings.add(jmiUpdate);
      JMenuItem jmiPermissions = new JMenuItem("Permissions");
      jmSettings.add(jmiPermissions);
      JMenuItem jmiGeneral = new JMenuItem("General");
      jmiGeneral.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
              java.awt.EventQueue.invokeLater(new Runnable() {
               public void run() {
                 new GeneralFrame().setVisible(true);
                }
           });       
            }
      });
      
      jmSettings.add(jmiGeneral);
      
      
      jmUser= new JMenu("User");
      jmUser.setMnemonic('U');
      JMenuItem jmiUser= new JMenuItem("Setup User");
      jmiUser.addActionListener(new ActionListener(){
         public void actionPerformed(ActionEvent e) {
              java.awt.EventQueue.invokeLater(new Runnable() {
               public void run() {
                   NewUserSetup userframe=new NewUserSetup();
                              userframe.addWindowListener(new WindowListener(){
                              @Override
                               public void windowOpened(WindowEvent e) { }
                               @Override
                               public void windowClosing(WindowEvent e) {
                                   getGlassPane().setVisible(false);
                               }

                               @Override
                               public void windowClosed(WindowEvent e) {getGlassPane().setVisible(false);}

                               @Override
                               public void windowIconified(WindowEvent e) {}

                               @Override
                               public void windowDeiconified(WindowEvent e) {getGlassPane().setVisible(false);}

                               @Override
                               public void windowActivated(WindowEvent e) {}

                               @Override
                               public void windowDeactivated(WindowEvent e) {getGlassPane().setVisible(false);}
                           });
                 userframe.setVisible(true);
          
                 
                }
           });       
         }
       });
      
      JMenuItem jmiUserList= new JMenuItem("All Users");
      jmiUserList.addActionListener(new ActionListener(){
         public void actionPerformed(ActionEvent e) {
            new UsersSpreadSheet().setVisible(true);         
         }
       });
      jmUser.add(jmiUser);
      jmUser.add(jmiUserList);
      
      jmReservation=new JMenu("Reservation");
      JMenuItem jmiFReservation= new JMenuItem("Future Reservation");
      jmiFReservation.addActionListener(new ActionListener(){
         public void actionPerformed(ActionEvent e) {
           java.awt.EventQueue.invokeLater(new Runnable() {
               public void run() {
                  new FutureReservationFrame().setVisible(true);
                }
           });       
         }
       });
      jmReservation.add(jmiFReservation);
      JMenu jmAccounts= new JMenu("Accounts");
      JMenuItem jmiAccounts= new JMenuItem("Account Details");
      jmiAccounts.addActionListener(new ActionListener(){
         public void actionPerformed(ActionEvent e) {
           java.awt.EventQueue.invokeLater(new Runnable() {
               public void run() {
                  new Accounts().setVisible(true);
                }
           });       
         }
       });
      jmAccounts.add(jmiAccounts);
       jmb = new JMenuBar();    
       setJMenuBar( jmb );         
       jmb.add( this.jmRoom ); 
       jmb.add(this.jmUser);
       jmb.add(this.jmReservation);
       jmb.add(jmAccounts);
       jmb.add(this.jmSettings);
       
    }
    
    private  void createTables() throws Exception
    { 
     
      try
      {
     
     String col[]={"M","M","M","M","M","M","M","M"};     
     _model= new com.cloudone.hotels.models.CITableModel(col);
     
      processData();
      renderModel();
     jtbDash = new JTable(_model);
     jtbDash.setRowHeight(30);
     jtbDash.setShowGrid(true);
     tcm=jtbDash.getColumnModel();
     HighlightTable(tcm); 
     
     header= jtbDash.getTableHeader();
     
     headerRenderer=new com.cloudone.hotels.models.MultiLineHeaderRenderer(SwingConstants.CENTER,SwingConstants.BOTTOM);
     headerRenderer.setBackground(Color.WHITE);
     headerRenderer.setForeground(Color.BLACK);
      
     
     int columns=tableHeaders.length; 
     for (int i = 0 ; i < columns ; i++) {
        tcm.getColumn(i).setHeaderRenderer(headerRenderer);
        tcm.getColumn(i).setHeaderValue(tableHeaders[i]);
      }
     String[] colheader={"Results"};
     models=new com.cloudone.hotels.models.CITableModel(colheader);
     List<com.cloudone.hotels.data.Guests> selfList=getSearchTableData();
     if(selfList.isEmpty())
       {
    
           com.cloudone.hotels.commons.Logging.logToFile("No Guest Made Reservation Yet");
     
       }
     else
     {
       for(int count=0;count<selfList.size();count++)
        {
        com.cloudone.hotels.data.Guests guests=selfList.get(count);
        models.addRow(Arrays.asList(guests.getFirstName()));
        }
     }
    
    jtbSearch=new JTable(models);
    jtbSearch.setRowHeight(30);
    jtbSearch.setTableHeader(null);
    selfList.clear();
    this.freeUpMemoryCache();
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
      }
   }
    
   private void HighlightTable(TableColumnModel colmodel)
   {
     com.cloudone.hotels.models.ComparatorImplementation Implementation= new com.cloudone.hotels.models.ComparatorImplementation(colmodel);
     Thread thread= new Thread(Implementation);
     thread.start();
   }
    private  java.util.List<com.cloudone.hotels.data.Guests> getSearchTableData()
    {
     
     
     try
     {
     
        Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
        GuestDao=DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
        return GuestDao.queryForAll();

     }
     catch(Exception exp)
     {
         com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
     }
        return null;
    }   
    private void createTableHeaders()
    {
     try
     {
     Calendar now = Calendar.getInstance();
     SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
     int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 1; //add 2 if your week start on monday
     now.add(Calendar.DAY_OF_MONTH, delta );
       for (int i = 0; i < 7; i++)
        {
        days[i] = format.format(now.getTime());
        now.add(Calendar.DAY_OF_MONTH, 1);
        }
      String title[] ={"","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
      int cursor=0;
      String titledata[][]={{title[0],""},{title[1],days[0]},{title[2],days[1]},{title[3],days[2]},{title[4],days[3]},{title[5],days[4]},{title[6],days[5]},{title[7],days[6]}};
      tableHeaders=new Object[8][2];
      for(int row=0;row<tableHeaders.length;row++)
       {
        for(int col=0;col<tableHeaders[ row ].length;col++)
        {
           tableHeaders[row][col]=titledata[row][col];          
        }
       } 
     }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
    }
    private com.cloudone.hotels.data.RoomReservation getReservation(int Id)
    {
     
     try
     {
     
        Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
        return ReservationDao.queryForId(Id); 
     }
     catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
     
     return null;      
    }        
    private com.cloudone.hotels.data.RoomReservation getReservationOnDay(String date)
    {
      try
      {
      SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
       for(int count=0;count<weeksReservation.size();count++)
       {
         com.cloudone.hotels.data.RoomReservation Obj= weeksReservation.get(count);
        // com.cloudone.hotels.commons.Logging.logToFile(format.format(Obj.getCheckInDate())); 
         if(format.format(Obj.getCheckInDate()).equals(date))
         {
           com.cloudone.hotels.commons.Logging.logToFile("Found Day");  
           return Obj;
         } 
         
       }
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
       return null;
      
    }
    
    private com.cloudone.hotels.data.Room getRoom(int Id)
    {
     
     try
     {
     
        Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
        return RoomDao.queryForId(Id); 
     }
     catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
     
     return null;      
    }        
    private com.cloudone.hotels.data.RoomTypes getRoomType(int Id)
    {
      
     try
     {
      
        Dao<com.cloudone.hotels.data.RoomTypes,Integer> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
        return RoomTypeDao.queryForId(Id); 
     }
     catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
     
     return null;      
    }        
  
    
    private com.cloudone.hotels.data.Guests getGuest(int Id)
    {
     
     try
     {
     
        Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
        return GuestDao.queryForId(Id);        
     }
     catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
     return null;
      
    }  
    
    private List<com.cloudone.hotels.data.RoomTypes> getAllRoomType()
    {
     
     try
     {
     
        Dao<com.cloudone.hotels.data.RoomTypes,Integer> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
        return RoomTypeDao.queryForAll(); 
     }
     catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
     
     return null;      
    
    }
    
    private List<com.cloudone.hotels.data.RoomReservation> getAllReservations()
    {
      
     try
     {
        
        Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
        return ReservationDao.queryForAll();
     }
     catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
     return null;
      
    }        
    private void processData() 
    {
        try
        {
        List<com.cloudone.hotels.data.RoomTypes> roomtypes=this.getAllRoomType();                
        map=new  HashMap<Integer,String[]>(roomtypes.size());
        for(int i=0;i<roomtypes.size();i++)
        {  
           String[] arr=new String[7]; 
          //typestatus=new ArrayList<String>();  
            
          for(int j=0;j<days.length;j++)
          {
              com.cloudone.hotels.data.RoomReservation reserve= this.getReservationOnDay(days[j]);  
              if(reserve==null)
              {
           
                   //typestatus.add("1");
                  com.cloudone.hotels.commons.Logging.logToFile("No Reservation Found in this week yet");
                  arr[j]="1";
              }
              else
              {
              
                 com.cloudone.hotels.data.Room r=reserve.getRoom();
                 if(r==null)
                   {
                    com.cloudone.hotels.commons.Logging.logToFile("Room is Null");      
                 
                   }
                 com.cloudone.hotels.data.Room room= this.getRoom(r.getId());
                 if(room==null)
                   {
                     
                    com.cloudone.hotels.commons.Logging.logToFile("Room Id not found");
                    continue; 
                   }
                 com.cloudone.hotels.data.RoomTypes rtype=room.getRoomType();
                if(rtype==null)
                  {
                    com.cloudone.hotels.commons.Logging.logToFile("Room Type not found");
                  }
                  com.cloudone.hotels.data.RoomTypes type= this.getRoomType(rtype.getId());
                  if(type==null)
                    {
                      com.cloudone.hotels.commons.Logging.logToFile("Room Type Id not found");  
                    }
                    if(type.getName().equals(roomtypes.get(i)))
                    {
                       //typestatus.add("1");
                       arr[j]="1" ;
                    }
                    else
                    {
                       //typestatus.add("0");
                        arr[j]="0" ;
                    }
                    
              //}catch(Exception exp){ System.out.println("error caught here line 822");JOptionPane.showMessageDialog(this,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);}
              }
          }
          map.put(roomtypes.get(i).getName().hashCode(),arr);
          System.gc();   
        }
        }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
    }
    private List<com.cloudone.hotels.data.RoomReservation> getWeeksReservation(String date,String date2)
    {
     
        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            
            Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReserveDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            GenericRawResults<String[]> rawOutput =ReserveDao.queryRaw("select _id from roomreservation where _checkedIn >='"+date+"' AND _checkedIn <='"+date2+"' AND _status='checked in'");
            List<String[]>  resultset= rawOutput.getResults();
            if(resultset.isEmpty())
            {
               com.cloudone.hotels.commons.Logging.logToFile("weeks reservation empty "); 
               return null;
            }
            if(!resultset.isEmpty())
            {
                List<com.cloudone.hotels.data.RoomReservation> reservation= new ArrayList<com.cloudone.hotels.data.RoomReservation>();
                for(int i=0;i<resultset.size();i++)
                  {
                    String[] output=resultset.get(i);  
                    reservation.add(ReserveDao.queryForId(Integer.parseInt(output[0])));
                  }
                
                return reservation;
            }
          
        }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
     
      return null; 
    }
    private void freeUpSpace(List<String> typestatus)
    {
      for(int count=0;count<typestatus.size();count++)
      {
         typestatus.remove(typestatus.get(count));
        
      }
      System.gc();
    }
    private List<String[]>  getAllGuestID(int Id)
    {
     
        try
        {
     
            Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReserveDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            GenericRawResults<String[]> rawOutput =ReserveDao.queryRaw("select _guest_id from roomreservation where _room_id ='"+String.valueOf(Id)+"' AND _status='checked in'");
            List<String[]>  resultset= rawOutput.getResults();
            if(resultset.isEmpty())
            {
               com.cloudone.hotels.commons.Logging.logToFile("all guests Id is empty"); 
               return null;
            }
            if(!resultset.isEmpty())
            {
                return resultset;
            }
          
        }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
     
      return null;
    }
    private List<com.cloudone.hotels.data.Guests> getAllGuests(int Id)
    {
      try
        {
     
            Dao<com.cloudone.hotels.data.Guests,Integer> GuestsDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
            List<com.cloudone.hotels.data.Guests> guestList=new ArrayList<com.cloudone.hotels.data.Guests>();
            List<String[]> resultset= this.getAllGuestID(Id);
            if(resultset!=null)
            {
              for(int i=0;i<resultset.size();i++)
               {
                String output[]= resultset.get(i);   
                if(output[0].isEmpty())
                {
                
                }
                else{guestList.add(GuestsDao.queryForId(Integer.parseInt(output[0])));}
               }
               return guestList;
            } else{com.cloudone.hotels.commons.Logging.logToFile("all guests are empty");return null;}
            }
        catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
        
        return null;
       
    
    }
    private List<com.cloudone.hotels.data.Room> getAllRoomsInCategory(com.cloudone.hotels.data.RoomTypes type)
    {
       
        try
        {
       
            Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
            GenericRawResults<String[]> rawOutput =RoomDao.queryRaw("select _id from room where _room_type_id ='"+type.getId()+"'");
            List<String[]>  resultset= rawOutput.getResults();
            List<com.cloudone.hotels.data.Room> roomList=new ArrayList<com.cloudone.hotels.data.Room>();
              for(int i=0;i<resultset.size();i++)
               {
                String output[]= resultset.get(i);   
                com.cloudone.hotels.data.Room room=RoomDao.queryForId(Integer.parseInt(output[0]));
                if(room==null)
                {
                  com.cloudone.hotels.commons.Logging.logToFile("could not find room in category");
                }
                else
                {
                 roomList.add(room);
                }
                
               }
            return roomList;                           
        }
        catch(Exception exp)
        {
            com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
        
        return null;
       
    
    }
    private int getReservedGuestStayDuration(String guest_Id)
    {
           
         try
          {
            
            Dao<com.cloudone.hotels.data.RoomReservation,String> RoomReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            GenericRawResults<String[]> rawOutput =RoomReservationDao.queryRaw("select _num_nights from roomreservation where _guest_id='"+guest_Id+"' AND _status='reserved'");
            List<String[]>  resultset= rawOutput.getResults();
            if(resultset.isEmpty())
            {
              return -1;
            }
            if(!resultset.isEmpty())
            {
               String[] data= resultset.get(0);
               return Integer.parseInt(data[0]);
            }
            
         }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
        
         return -1;
    }
    private int getGuestStayDuration(String guest_Id)
    {
           
            for(int count=0;count<weeksReservation.size();count++)
               {
                com.cloudone.hotels.data.RoomReservation Obj= weeksReservation.get(count);
                if(Obj.getGuest().getId()==Integer.parseInt(guest_Id))
                  {
                     
                     return Obj.getNights();
                  } 
              }
         return -1;
    }
    private com.cloudone.hotels.data.Guests ReservedGuest(String Id,String date)
    {
   
        try
          {
            
            Dao<com.cloudone.hotels.data.RoomReservation,String> RoomReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            GenericRawResults<String[]> rawOutput =RoomReservationDao.queryRaw("select _id from roomreservation where _room_id='"+Id+"' AND _status='reserved' AND _checkedIn='"+date+"'");
            List<String[]>  resultset= rawOutput.getResults();
            if(resultset.isEmpty())
            {
              return null;
            }
            if(!resultset.isEmpty())
            {
               String[] data= resultset.get(0);
               Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
               return GuestDao.queryForId(Integer.parseInt(data[0]));
            }
            
         }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
        return null;   
    }
    
    
    private com.cloudone.hotels.data.Guests GuestList(String Id,String date)
    {
   
        try
          {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
            List<com.cloudone.hotels.data.Guests> list= new ArrayList<com.cloudone.hotels.data.Guests>();
            for(int count=0;count<weeksReservation.size();count++)
             {
                com.cloudone.hotels.data.RoomReservation Obj= weeksReservation.get(count);
                if(Obj.getRoom().getId()==Integer.parseInt(Id)&&format.format(Obj.getCheckInDate()).equals(date))
                  {
                   return GuestDao.queryForId(Obj.getGuest().getId());
                  } 
            }
          
         }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
        return null;   
    }
    
    private boolean isReserved(String Id,String date)
    {

        try
        {
           SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
           
            for(int i=0;i<weeksReservation.size();i++)
            {
               if((format.format(weeksReservation.get(i).getCheckInDate()).equals(date))&& (weeksReservation.get(i).getRoom().getId()==Integer.parseInt(Id)))
                  {
                    return true;
                  } 
                          
            }
        }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
   
        return false;
      
    
    }
    private boolean IsReservedFuture(String Id,String date)
    {
         try
        {
            
            String querydate=date+" 00:00:00";
            Dao<com.cloudone.hotels.data.RoomReservation,Integer> RoomReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            GenericRawResults<String[]> raw =RoomReservationDao.queryRaw("select _checkedIn,_checkedOut from roomreservation where _room_id='"+Id+"' AND _checkedIn='"+date+"' AND _status='reserved'");
            List<String[]>  result= raw.getResults();
            if(result.isEmpty())
            {
                com.cloudone.hotels.commons.Logging.logToFile("xxEmpty");
                return true;
            }
            if(!result.isEmpty())
            {
                com.cloudone.hotels.commons.Logging.logToFile("Not Empty");
               try
               {
                SimpleDateFormat dateformatter= new SimpleDateFormat("yyyy-MM-dd");        
                String[] rawdata=result.get(0);
                String checkIn= rawdata[0];
                String checkOut=rawdata[1];
                com.cloudone.hotels.commons.Logging.logToFile(checkIn);
                com.cloudone.hotels.commons.Logging.logToFile(checkOut);
                com.cloudone.hotels.commons.Logging.logToFile(querydate);        
                    
                java.util.Date checkdate =dateformatter.parse(querydate.substring(0,10));
                java.util.Date startdate=dateformatter.parse(checkIn.substring(0,10));
                java.util.Date enddate=dateformatter.parse(checkOut.substring(0,10));
                if(com.cloudone.hotels.commons.DateHelper.betweenEx(checkdate, startdate, enddate))
                {                
                  com.cloudone.hotels.commons.Logging.logToFile("Valid Decision");  
                  return false;
                }
                
               }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
                
               } 
            }

        }
        catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
        
        return true;
 
    }
    private int getReservationGuestId(String Id)
    {
   
        try
        {
   
            Dao<com.cloudone.hotels.data.RoomReservation,String> RoomReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            GenericRawResults<String[]> rawOutput =RoomReservationDao.queryRaw("select _guest_id from roomreservation where _room_id='"+Id+"'");
            List<String[]>  resultset= rawOutput.getResults();
            if(resultset.isEmpty())
            {
               return -1;
            }
            if(!resultset.isEmpty())
            {
                String output[]=resultset.get(0);
                return Integer.parseInt(output[0]); 
            }
        }
        catch(Exception exp)
        {
            com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
        
        return -1;
    
    }
    private void procData(com.cloudone.hotels.data.RoomTypes type) throws Exception
    {  
        List<com.cloudone.hotels.data.Room> room=null;
        room=getAllRoomsInCategory(type);       
        if(room.isEmpty())
        {
          com.cloudone.hotels.commons.Logging.logToFile("Null room list");
        }
           for(int i=0;i<room.size();i++)
           {
             com.cloudone.hotels.data.Room rm= room.get(i);  
             roomstatus.add(rm.getName());  
             for(int j=0;j<days.length;j++)
             {
           
               if(isReserved(String.valueOf(rm.getId()),days[j]))
                {
                  com.cloudone.hotels.data.Guests guest= GuestList(String.valueOf(rm.getId()),days[j]);
                  if(guest==null)
                  {
                    com.cloudone.hotels.commons.Logging.logToFile("Guest not found");   
                  }
                  else
                  {
                    roomstatus.add("\n"+guest.getFirstName());
                    int d=getGuestStayDuration(String.valueOf(guest.getId()));
                    if(d==1){}
                    else if (d%2!=0){d=d-1;}
                    for(int f=0;f<d;f++)
                    {
                        this.roomstatus.add("\n");  
                        j++;
                    }  
                  }
               }
               else 
               {
                   if(!IsReservedFuture(String.valueOf(rm.getId()),days[j]))
                   {
                       
                       com.cloudone.hotels.data.Guests guest=this.ReservedGuest(String.valueOf(rm.getId()),days[j]);
                       if(guest==null)
                       {
                        com.cloudone.hotels.commons.Logging.logToFile("Guest not found");   
                       }
                       else
                       {
                        roomstatus.add("\t"+guest.getFirstName());
                        int d=this.getReservedGuestStayDuration(String.valueOf(guest.getId()));
                        if(d==1){}
                        else if (d%2!=0){d=d-1;}
                        for(int f=0;f<d;f++)
                        {
                          this.roomstatus.add("\t");  
                          j++;
                          
                        }  
                       }
                     
                   }
                   else
                   {roomstatus.add("");}
               }
               
        }
         int k=0;
        _model.addRow(Arrays.asList(roomstatus.get(k),roomstatus.get(++k),roomstatus.get(++k),roomstatus.get(++k),roomstatus.get(++k),roomstatus.get(++k),roomstatus.get(++k),roomstatus.get(++k)));
         roomstatus.clear();
         
        }
       
       room.clear();
       
       
       
    }
    private void renderModel() throws Exception
    {
      try
      {
      List<com.cloudone.hotels.data.RoomTypes> roomtypes= this.getAllRoomType();  
      String[] listdata=null;
      if(roomtypes.isEmpty())
      {
       
       com.cloudone.hotels.commons.Logging.logToFile("There no room type created yet");   
      }
      for(int i=0;i<roomtypes.size();i++)
      {
         com.cloudone.hotels.data.RoomTypes roomtype=roomtypes.get(i); 
         if(roomtype==null)
         {
           com.cloudone.hotels.commons.Logging.logToFile("Null room type");   
         
         }
        
         for(int j=0;j<map.size();j++)
         { 
           if(map.isEmpty())
           {
             //System.out.println("you lied");
           
           }
           listdata=map.get(roomtype.getName().hashCode());
           
           
         }
         
         int k=0;
         try
             
         {
          _model.addRow(Arrays.asList(roomtype.getName(),listdata[k],listdata[++k],listdata[++k],listdata[++k],listdata[++k],listdata[++k],listdata[++k]));
         this.procData(roomtype);
         
         }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());   }
      }
      roomtypes.clear();
     // listdata.clear();
      System.gc();
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
    }

   @Override
    public void update() 
   { 
               
            try
            {
           mo.deleteAllRows();   
         List<com.cloudone.hotels.data.RoomReservation> reserve=getAllReservations();
          
          
        
       if(!reserve.isEmpty())
        {
         for(int count=reserve.size()-1;count>=0;count--)
           {
             com.cloudone.hotels.data.RoomReservation  busObj=reserve.get(count);
             com.cloudone.hotels.data.Guests guest=busObj.getGuest();
             guest=getGuest(guest.getId());
             com.cloudone.hotels.data.Room room=busObj.getRoom();
             room= getRoom(room.getId());
             if(guest==null)
              {
             com.cloudone.hotels.commons.Logging.logToFile("com.cloudone.hotels.exceptions.GuestNotFoundException");
                  
              }
            if(room==null)
              {
                com.cloudone.hotels.commons.Logging.logToFile("Room Not Found Exception");
              }
              rowId.add(busObj.getId());
              String name;
              String roomname;
              String reserveddate;
              String nights;                  
              String total;
              String checkIn;
              String checkOut;
              if(guest.getFirstName().isEmpty()){name="";}else{name=guest.getFirstName();}
              if(room.getName().isEmpty()){roomname="";}else{roomname=room.getName();}
              if(formatter.format(busObj.getReservationDate()).isEmpty()){reserveddate="";}else{reserveddate=formatter.format(busObj.getReservationDate());}
              if(String.valueOf(busObj.getNights()).isEmpty()){nights="";}else{nights=String.valueOf(busObj.getNights());}
              if(String.valueOf(busObj.getTotalPrice()).isEmpty()){total="";}else{total=String.valueOf(busObj.getTotalPrice());}
              if(formatter.format(busObj.getCheckInDate()).isEmpty()){checkIn="";}else{checkIn=formatter.format(busObj.getCheckInDate());}
              if(formatter.format(busObj.getCheckOutDate()).isEmpty()){checkOut="";}else{checkOut=formatter.format(busObj.getCheckOutDate());}
              mo.addRow(Arrays.asList(name,roomname,reserveddate,nights,nights,total,checkIn,checkOut,busObj.getStatus(),Boolean.FALSE));
           }
        
            reserve.clear();
        }
       else{com.cloudone.hotels.commons.Logging.logToFile("Empty Reservation List");}
         _model.deleteAllRows();
         weeksReservation=getWeeksReservation(days[0], days[6]);      
         processData();
         renderModel();
         
         HighlightTable(tcm);      
        int columns=tableHeaders.length; 
        for (int i = 0 ; i < columns ; i++) {
        tcm.getColumn(i).setHeaderRenderer(headerRenderer);
        tcm.getColumn(i).setHeaderValue(tableHeaders[i]);
        
        }
        models.deleteAllRows();
         List<com.cloudone.hotels.data.Guests> selfList=getSearchTableData();
         if(selfList.isEmpty())
          {
           com.cloudone.hotels.commons.Logging.logToFile("No Guest Made Reservation Yet");
     
         }
        else
         {
           
           for(int count=0;count<selfList.size();count++)
            {
               com.cloudone.hotels.data.Guests guests=selfList.get(count);
               models.addRow(Arrays.asList(guests.getFirstName()));
             }
         } 
        
    }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
      
    
    }
     private  void createEmptyTables()
    { 
     String col[]={"M","M","M","M","M","M","M","M"};     
     _model= new com.cloudone.hotels.models.CITableModel(col);
     this.createTableHeaders();
     jtbDash = new JTable(_model);
     jtbDash.setRowHeight(30);
     tcm=jtbDash.getColumnModel();
     HighlightTable(tcm); 
     
     header= jtbDash.getTableHeader();
     headerRenderer=new com.cloudone.hotels.models.MultiLineHeaderRenderer(SwingConstants.CENTER,SwingConstants.BOTTOM);
     headerRenderer.setBackground(Color.WHITE);
     headerRenderer.setForeground(Color.BLACK);
      
     
     int columns=tableHeaders.length; 
     for (int i = 0 ; i < columns ; i++) {
        tcm.getColumn(i).setHeaderRenderer(headerRenderer);
        tcm.getColumn(i).setHeaderValue(tableHeaders[i]);
      }
     String[] colheader={"Results"};
     models=   new com.cloudone.hotels.models.CITableModel(colheader);
     
    jtbSearch=new JTable(models);
    jtbSearch.setRowHeight(30);
    jtbSearch.setTableHeader(null);
   
    }
    public void createEmptyReservationTable() 
    {
        String headers[]={"Guest Name","Room Name","Reserved Date","No of Nights","No of Guests","Total Price","Check In","Check Out Date","Status","Boolean"};
        mo=new com.cloudone.hotels.models.CITableModelEx(headers);
        tbr = new JTable(mo);     
        tbr.setRowHeight(20);
        tbr.setRowSelectionAllowed(true);
        tbr.setColumnSelectionAllowed(true);
        tbr.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tbr.setCellSelectionEnabled(true);
      
    } 
   
    public void freeUpMemoryCache()
    {
     // typestatus.clear();
       map.clear();
       roomstatus.clear();
       System.gc();
       
    }
    public void shutDown()
    {
      map.clear();
      roomstatus.clear();
      models.deleteAllRows();
     _model.deleteAllRows();
      mo.deleteAllRows();
    }
    public void setLogoutObject(ReservationDashBoard frame)
    {
      //com.cloudone.hotels.controllers.LogoutListener logout= new com.cloudone.hotels.controllers.LogoutListener(frame);
      //jb[1].addActionListener(logout);
      //setVisible(true);
    }
              
                    
}

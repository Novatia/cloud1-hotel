package com.cloudone.hotels.view;

import javax.swing.*;
import java.awt.Dimension;
import java.awt.Toolkit;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Akala G56
 */

public class AdminUserFrame extends javax.swing.JFrame {

    /**
     * Creates new form AdminUserFrame
     */
    public AdminUserFrame() {
        getContentPane().setBackground(java.awt.Color.WHITE);
        initComponents();
        setResizable(false);
        com.cloudone.hotels.util.Utilities.WindowUtilities.centerComponent(this);


    }
    private void addListener(){
        com.cloudone.hotels.controllers.AdminSetupListener  listener = new com.cloudone.hotels.controllers.AdminSetupListener(
            this,
            jbSubmit,
            jpPasswordField,
            jtFirstname,
            jtLastname,
            jtUsername,
            jtfCity,
            jpfConfirm,
            jcbCountry,
            jtfCurrency,
            jtfEmail,
            jtfHotel,
            jtfPhone
        );
       jbSubmit.addActionListener(listener);
        }



    private void initComponents() {

        jlFirstname = new javax.swing.JLabel();
        jtFirstname = new javax.swing.JTextField();
        jlLastname = new javax.swing.JLabel();
        jtLastname = new javax.swing.JTextField();
        jlUsername = new javax.swing.JLabel();
        jtUsername = new javax.swing.JTextField();
        jlPassword = new javax.swing.JLabel();
        jpPasswordField = new javax.swing.JPasswordField();
        jbSubmit = new javax.swing.JButton("Submit");
        jlConfirm = new javax.swing.JLabel();
        jlHotel = new javax.swing.JLabel();
        jlCity = new javax.swing.JLabel();
        jlCountry = new javax.swing.JLabel();
        jlCurrency = new javax.swing.JLabel();
        jlPhone = new javax.swing.JLabel();
        jlEmail = new javax.swing.JLabel();
        jpfConfirm = new javax.swing.JPasswordField();
        jtfHotel = new javax.swing.JTextField();
        jtfCity = new javax.swing.JTextField();

        String[] locales = java.util.Locale.getISOCountries();
        jcbCountry = new javax.swing.JComboBox();
        for (String countryCode : locales)
        {
            java.util.Locale obj = new java.util.Locale("", countryCode);
            jcbCountry.addItem(obj.getDisplayCountry());
        }


        jtfCurrency = new javax.swing.JTextField();
        jtfPhone = new javax.swing.JTextField();
        jtfEmail = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Administrative User Setup");
        setResizable(false);

        jlFirstname.setText("Firstname");
        jlLastname.setText("Lastname");
        jlUsername.setText("Username");
        jlPassword.setText("Password");
        jlConfirm.setText("Confirm Password");
        jlHotel.setText("Hotel Name");
        jlCity.setText("City");
        jlCountry.setText("Country");
        jlCurrency.setText("Currency");
        jlPhone.setText("Hotel Phone");
        jlEmail.setText("Hotel Email");
        addListener();
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jbSubmit)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jlUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(54, 54, 54))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jlLastname, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jlFirstname, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jlConfirm, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jlHotel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jlCurrency, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jlCountry, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                                        .addComponent(jlCity, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jlEmail, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jlPhone, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE))
                                    .addComponent(jlPassword, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(35, 35, 35)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jtFirstname, javax.swing.GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
                            .addComponent(jtLastname)
                            .addComponent(jtUsername)
                            .addComponent(jpPasswordField)
                            .addComponent(jpfConfirm)
                            .addComponent(jtfHotel)
                            .addComponent(jtfCity)
                            .addComponent(jcbCountry)
                            .addComponent(jtfCurrency)
                            .addComponent(jtfPhone)
                            .addComponent(jtfEmail))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jlFirstname, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)
                    .addComponent(jtFirstname))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jlLastname, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                    .addComponent(jtLastname))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jtUsername)
                    .addComponent(jlUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jpPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jpfConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jlHotel, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jtfHotel, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                        .addGap(4, 4, 4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jlCity, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jtfCity, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jlCountry, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jcbCountry, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jlCurrency, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfCurrency, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jlPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jtfPhone)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(jbSubmit)
                .addContainerGap(43, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jbSubmit;
    public javax.swing.JLabel jlCity;
    public javax.swing.JLabel jlConfirm;
    public javax.swing.JLabel jlCountry;
    public javax.swing.JLabel jlCurrency;
    public javax.swing.JLabel jlEmail;
    public javax.swing.JLabel jlFirstname;
    public javax.swing.JLabel jlHotel;
    public  javax.swing.JLabel jlLastname;
    public javax.swing.JLabel jlPassword;
    public javax.swing.JLabel jlPhone;
    public javax.swing.JLabel jlUsername;
    public javax.swing.JPasswordField jpPasswordField;
    public javax.swing.JTextField jtFirstname;
    public  javax.swing.JTextField jtLastname;
    public javax.swing.JTextField jtUsername;
    public javax.swing.JTextField jtfCity;
    public javax.swing.JPasswordField jpfConfirm;
    public javax.swing.JComboBox jcbCountry;
    public  javax.swing.JTextField jtfCurrency;
    public javax.swing.JTextField jtfEmail;
    public javax.swing.JTextField jtfHotel;
    public javax.swing.JTextField jtfPhone;
   // End of variables declaration
}


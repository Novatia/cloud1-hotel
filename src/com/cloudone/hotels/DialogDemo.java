/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels;

import com.cloudone.hotels.commons.DateHelper;
import com.cloudone.hotels.interfaces.TableStateController;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.google.common.base.CharMatcher;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import me.alvr.ideaKeygen.UI;
import sun.util.calendar.Gregorian;
/*
 * Copyright (c) 1995 - 2008 Sun Microsystems, Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - Neither the name of Sun Microsystems nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * DialogDemo.java requires these files: CustomDialog.java images/middle.gif
 */
public class DialogDemo {
  
 public static void main(String[] args){
   try
   {
    //SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy"); 
    //Date dateStart= format.parse("20/04/2014");   
   // Date dateEnd= format.parse("26/04/2014");
   // Date date= format.parse("27/04/2014");
    
    //java.lang.System.out.println(DateHelper.between(date, dateStart, dateEnd));
    
   // Calendar cal=Calendar.getInstance();
   // cal.set(2014, 04, 30);
    //java.lang.System.out.println(DateHelper.Add(cal, 2));
    com.cloudone.hotels.commons.alerts.Email busObj= new com.cloudone.hotels.commons.alerts.Email();
    com.cloudone.hotels.commons.alerts.EmailAlerts emailbusObj= new com.cloudone.hotels.commons.alerts.EmailAlerts(busObj);
    emailbusObj.sendEmail();
   }catch(Exception exp){exp.printStackTrace();}
  }
}


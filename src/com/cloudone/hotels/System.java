
package com.cloudone.hotels;
/*
 * Copyright (c) 2014, 
 * All rights reserved.
 * ALL RIGHTS TO THIS SOURCE FILE BELONGS TO CLOUDONE 
 * DATE CREATED: 24/03/2014 TIME STAMP:14:18
 * 
 * 
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, thi
 * s list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the TimingFramework project nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Calendar;
import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import com.j256.ormlite.support.DatabaseConnection;
import de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel;
import javax.swing.UIManager;

    


/**
 *
 * @author Akala Michael 
 */
public class System {
   private static String _databaseUrl=null;
   private static String _username=null;
   private static String _password=null;
   static JdbcPooledConnectionSource connectionSource=null;

    public static void InitializeApplication() {
      
       ConnectionString cString= ConnectionString.createInstance();
       try
       {

         _username =System.Security.decrypt(AppConstants.ENCRYPTION_KEY,cString.getSequelUserId());
         _password= System.Security.decrypt(AppConstants.ENCRYPTION_KEY,cString.getSequelPwd());
         _databaseUrl=System.Security.decrypt(AppConstants.ENCRYPTION_KEY,cString.getDbDriver());         
     
         createDatabaseTables();
         com.cloudone.hotels.commons.TableStateEvent.Init();
         com.cloudone.hotels.commons.Logging.logToFile("created database schema ");
       }catch(Exception e){e.printStackTrace();}
    }

    public static JdbcPooledConnectionSource getConnectionSource(){
      try
      {
       
       connectionSource= new JdbcPooledConnectionSource(_databaseUrl);
       connectionSource.setUsername(_username);
       connectionSource.setPassword(_password);     
       connectionSource.setMaxConnectionAgeMillis(5 * 60 * 1000);
       connectionSource.setCheckConnectionsEveryMillis(60 * 1000);
       connectionSource.setTestBeforeGet(true);
       
       
       return connectionSource; 
      }catch(Exception exp){exp.printStackTrace();}
      return null;
  }
  private static void createDatabaseTables (){

      try
      {
         connectionSource= getConnectionSource();
         if(TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.System.class)==0);
         {
          com.cloudone.hotels.commons.Logging.logToFile("database schema initialization started"); 
          connectionSource=com.cloudone.hotels.System.getConnectionSource();
          Dao<com.cloudone.hotels.data.System,Integer> SystemDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.System.class);
          com.cloudone.hotels.data.System   sys=new com.cloudone.hotels.data.System();
          sys.setIsFirstLogon(true);
          sys.setSystemLogonDate(Calendar.getInstance().getTime()); 
          SystemDao.create(sys);
         }
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.Facilities.class);
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.FutureReservation.class);
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.Guests.class);
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.Hotel.class);
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.Room.class);
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.RoomFacilities.class);
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.RoomReservation.class);
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.RoomTypes.class);
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.Settings.class);
         TableUtils.createTableIfNotExists(connectionSource,com.cloudone.hotels.data.User.class);
       
         
      }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}

  }

  private static class Resources{

  public static class Utilities{
    public static class ImageUtitlities{
         public static synchronized byte[] ToBytes(File file){
           try {
                 /*
                  * The usage requires only one thread at a time to aviod 
                  * a potential race condition 
                  */
	              byte[] ImageByteArray;
	              BufferedImage originalImage = ImageIO.read(file);
                  ByteArrayOutputStream ByteArrayStream = new ByteArrayOutputStream();
	              ImageIO.write(originalImage, "jpg", ByteArrayStream);
	              ByteArrayStream.flush();
		          ImageByteArray = ByteArrayStream.toByteArray();
		          ByteArrayStream.close();
                 return ImageByteArray;
	            } catch (IOException e) {com.cloudone.hotels.commons.Logging.logToFile(e.getMessage());}
          return null;
	}
        /*
         * The usage requires only one thread at a time to aviod
         * a potential race condition
         */
       public static synchronized BufferedImage ConvertToImage(byte[] ImageByteArray, String FileName){
             try
             {
                InputStream in = new ByteArrayInputStream(ImageByteArray);
                BufferedImage bImageFromConvert = ImageIO.read(in);
                ImageIO.write(bImageFromConvert, "jpg", new File(FileName));
             }catch(Exception e){com.cloudone.hotels.commons.Logging.logToFile(e.getMessage());}
           
           return null;
       }
        
      }
     }
     public static class Internet{
         private static boolean checkInternetConnectionStatus(){
             return true;
         }
         public static String sendInternetRequest(String domainAddress, String urlParameters){

             URL urlAddr;
             String line;
             HttpURLConnection httpconnection = null;

             if(!checkInternetConnectionStatus())
             {

             }

             try{
                  com.cloudone.hotels.commons.Logging.logToFile("Sending network request");                  
                  urlAddr = new URL(domainAddress);
                  httpconnection = (HttpURLConnection)urlAddr.openConnection();
                  httpconnection.setRequestMethod("POST");
                  httpconnection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
                  httpconnection.setRequestProperty("Content-Length", "" +Integer.toString(urlParameters.getBytes().length));
                  httpconnection.setRequestProperty("Content-Language", "en-US");
                  httpconnection.setUseCaches (false);
                  httpconnection.setDoInput(true);
                  httpconnection.setDoOutput(true);

                  DataOutputStream requestStream = new DataOutputStream (httpconnection.getOutputStream ());
                  requestStream.writeBytes (urlParameters);
                  requestStream.flush ();
                  requestStream.close ();

                  InputStream responseStream = httpconnection.getInputStream();
                  BufferedReader bufReader= new BufferedReader(new
                  InputStreamReader(responseStream));
                  StringBuffer response = new StringBuffer();
                  while((line = bufReader.readLine()) != null) {
                         response.append(line);
                         response.append('\r');
                        }
                  bufReader.close();
                  return response.toString();

               }catch (Exception e) {com.cloudone.hotels.commons.Logging.logToFile(e.getMessage());return null;}
               finally
               {
                 if(httpconnection != null)
                     {
                         httpconnection.disconnect();
                         com.cloudone.hotels.commons.Logging.logToFile("network request connection closed");
                     }
               }
         }
     }
  }
  public static class Security
  {
      static Cipher ecipher;
      static Cipher dcipher;
      // 8-byte Salt
      static byte[] salt = {
              (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
              (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
      };
      // Iteration count
      static int iterationCount = 19;

      /**
       *
       * @param secretKey Key used to encrypt data
       * @param plainText Text input to be encrypted
       * @return Returns encrypted text
       *
       */
      public static String encrypt(String secretKey, String plainText)
              throws NoSuchAlgorithmException,
              InvalidKeySpecException,
              NoSuchPaddingException,
              InvalidKeyException,
              InvalidAlgorithmParameterException,
              UnsupportedEncodingException,
              IllegalBlockSizeException,
              BadPaddingException{
          //Key generation for enc and desc
          KeySpec keySpec = new PBEKeySpec(secretKey.toCharArray(), salt, iterationCount);
          SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
          // Prepare the parameter to the ciphers
          AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

          //Enc process
          ecipher = Cipher.getInstance(key.getAlgorithm());
          ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
          String charSet="UTF-8";
          byte[] in = plainText.getBytes(charSet);
          byte[] out = ecipher.doFinal(in);
          String encStr=new sun.misc.BASE64Encoder().encode(out);
          return encStr;
      }
      /**
       * @param secretKey Key used to decrypt data
       * @param encryptedText encrypted text input to decrypt
       * @return Returns plain text after decryption
       */
      public static synchronized String decrypt(String secretKey, String encryptedText)
              throws NoSuchAlgorithmException,
              InvalidKeySpecException,
              NoSuchPaddingException,
              InvalidKeyException,
              InvalidAlgorithmParameterException,
              UnsupportedEncodingException,
              IllegalBlockSizeException,
              BadPaddingException,
              IOException{
          //Key generation for enc and desc

          KeySpec keySpec = new PBEKeySpec(secretKey.toCharArray(), salt, iterationCount);
          SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
          // Prepare the parameter to the ciphers
          AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
          //Decryption process; same key will be used for decr
          dcipher=Cipher.getInstance(key.getAlgorithm());
          dcipher.init(Cipher.DECRYPT_MODE, key,paramSpec);
          byte[] enc = new sun.misc.BASE64Decoder().decodeBuffer(encryptedText);
          byte[] utf8 = dcipher.doFinal(enc);
          String charSet="UTF-8";
          String plainStr = new String(utf8, charSet);
          return plainStr;
      }
  }
  public static void main(String args[]){

          com.cloudone.hotels.commons.Logging logger= com.cloudone.hotels.commons.Logging.createInstance();  
          com.cloudone.hotels.commons.Logging.logToFile("Application startup folder"+java.lang.System.getProperty("user.dir"));
          com.cloudone.hotels.commons.Logging.logToFile("System Initializing Application");
          System.InitializeApplication();
          try {
             // for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
             //   if ("Metal".equals(info.getName())) {
              //    javax.swing.UIManager.setLookAndFeel(info.getClassName());
              //  break;
              //}}
              //      try {
          //     UIManager.addAuxiliaryLookAndFeel(SyntheticaAluOxideLookAndFeel());
              
              UIManager.setLookAndFeel("de.javasoft.plaf.synthetica.SyntheticaAluOxideLookAndFeel");
         // } catch (Exception e) {
           //    e.printStackTrace();
         // }

              
          } catch (Exception ex) {com.cloudone.hotels.commons.Logging.logToFile(ex.getMessage());}
          //</editor-fold>
          
          
         
        try
        {
         
            Dao<com.cloudone.hotels.data.System,Integer> SystemDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.System.class);
            GenericRawResults<String[]> rawOutput =SystemDao.queryRaw("select _state from System");
            List<String[]>  resultset= rawOutput.getResults();
            String[] output = resultset.get(0);
            if(output[0]==null)
            {
               throw new Exception("system state could not be determined");
            }
            else
            {
               if(output[0].equals("1"))
               {
                 
                /* Create and display the form */
                  java.awt.EventQueue.invokeLater(new Runnable() {
                      public void run() {
                          new com.cloudone.hotels.view.JLicenseForm().setVisible(true);
                      }
                  });      
               }
               else if(output[0].equals("0"))
               {
                   /* Create and display the form */
                  java.awt.EventQueue.invokeLater(new Runnable() {
                      public void run() {
                         new com.cloudone.hotels.view.LoginFrame();          
                      }
                  });      
                  
               }
               else{}
            }

        }
        catch(Exception exp)
        {
            com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
       
          

      }

}

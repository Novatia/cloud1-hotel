/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.util;

/**
 *
 * @author Akala Michael
 */
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;


public class Utilities {

public static class WindowUtilities{

   public static void centerComponent(JFrame component){
     
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - component.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - component.getHeight()) / 2);
        component.setLocation(x, y);
        
   }
   
   public static void centerComponent(JDialog component){

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - component.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - component.getHeight()) / 2);
        component.setLocation(x, y);
   }

   public static void setWindowCompanyIcon(JFrame frame,String filename){
       frame.setIconImage(Toolkit.getDefaultToolkit().createImage(filename));
   }
    public static void setWindowCompanyIcon(JFrame frame,java.net.URL url){
       frame.setIconImage(Toolkit.getDefaultToolkit().createImage(url));
    }
    public static void setWindowCompanyIcon(JFrame frame,byte[] imageArray){
        frame.setIconImage(Toolkit.getDefaultToolkit().createImage(imageArray));
    }
    public static void setWindowCompanyIcon(JDialog dialog,String filename){
        dialog.setIconImage(Toolkit.getDefaultToolkit().createImage(filename));
    }
    public static void setWindowCompanyIcon(JDialog dialog,java.net.URL url){
        dialog.setIconImage(Toolkit.getDefaultToolkit().createImage(url));
    }
    public static void setWindowCompanyIcon(JDialog dialog,byte[] imageArray){
        dialog.setIconImage(Toolkit.getDefaultToolkit().createImage(imageArray));
    }

}
public static class TableUtilities {
   
   public static int calculateColumnWidth(JTable table,int columnIndex) {
         int width =0;          // The return value
         int rowCount = table.getRowCount();
          for (int i = 0; i < rowCount ; i++) {
           TableCellRenderer renderer =table.getCellRenderer(i, columnIndex);
          Component comp =renderer.getTableCellRendererComponent(table, table.getValueAt(i, columnIndex),
                                   false, false, i, columnIndex);
          int thisWidth = comp.getPreferredSize().width;
          if (thisWidth > width) {
             width = thisWidth;
          }
       }

      return width;
   }

   // Set the widths of every column in a table
   public static void setColumnWidths(JTable table,Insets insets, boolean setMinimum,boolean setMaximum) {
     int columnCount = table.getColumnCount();
     TableColumnModel tcm = table.getColumnModel();
     int spare = (insets == null ? 0 : insets.left +insets.right);

     for (int i = 0; i < columnCount; i++)
     {
         int width = calculateColumnWidth(table, i);
         width += spare;

         TableColumn column = tcm.getColumn(i);
         column.setPreferredWidth(width);
         if (setMinimum == true) {
            column.setMinWidth(width);
         }
         if (setMaximum == true) {
            column.setMaxWidth(width);
         }
      }
   }
}
    
}

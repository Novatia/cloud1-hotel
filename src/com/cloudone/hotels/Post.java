/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels;

/**
 *
 * @author Akala G56Nigga
 */

import com.cloudone.hotels.data.RoomTypes;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.table.TableUtils;
import com.cloudone.hotels.data.User;
import com.j256.ormlite.support.DatabaseConnection;

@DatabaseTable(tableName ="Post")
    public class Post 
    {
      @DatabaseField(id = true)
      private int id;
      @DatabaseField
       private String Title;
      @DatabaseField
       private String Content;
      @DatabaseField(foreign= true)
       private Category _category;
      
      public Post() {

      }
      public Post(String title, String content) {
      this.Title = title;
      this.Content = content;
      }
      public void setTitle(String title){
        Title= title;
      }
      public String getTitle(){
      
        return Title;
      }
      public void setContent(String content){
        Content= content;
      }
      public String getContent(){
      
        return Content;
      }
      public void setCategory(Category category){
        _category=category;
      }
      public Category getCategory(){
        return _category;
      }
      
}
 @DatabaseTable(tableName ="category")
class Category
{
      @DatabaseField(id = true)
      private int id;
      @DatabaseField
      private String Title;
      
      public Category(){
      
      }
      
      public void setTitle(String title){
      
        Title =title;
      }
      public String getTitle(){
        return Title;
      }
}
 @DatabaseTable(tableName ="PostTag")
class PostTag
{
      @DatabaseField(foreign = true)
      private Post _post;
      
      @DatabaseField(foreign = true)
      private Tag _tag;
      
      public PostTag(){
      }
      public void setPost(Post post){
      
      _post=post;
      }
      public Post getPost(){
      
        return _post;
      }
      public void setTag(Tag tag){
      
        _tag =tag;
      }
      public Tag getTag(){
        return _tag;
      }
}


 @DatabaseTable(tableName ="Tag")
class Tag
{
      @DatabaseField(id = true)
      private int id;
      @DatabaseField
      private String Title;
      
      public Tag(){
      
      }
      
      public void setTitle(String title){
      
        Title =title;
      }
      public String getTitle(){
        return Title;
      }
      public static void main(String args[]){
      String databaseUrl= "jdbc:mysql://localhost/cloud1_hotels";  

       try
       {
 
        JdbcPooledConnectionSource connectionSource = new JdbcPooledConnectionSource(databaseUrl);
        connectionSource.setMaxConnectionAgeMillis(5 * 60 * 1000);
        connectionSource.setCheckConnectionsEveryMillis(60 * 1000);
        connectionSource.setTestBeforeGet(true);
        connectionSource.setUsername("root");
        connectionSource.setPassword("123456");
        DatabaseConnection dbasecon=connectionSource.getSpecialConnection();
        //Dao<Post,String> PostDao= DaoManager.createDao(connectionSource, Post.class);
        //Dao<Category,String> CategoryDao= DaoManager.createDao(connectionSource, Category.class);
        //Dao<Tag,String> TagDao= DaoManager.createDao(connectionSource, Tag.class);
        Dao<Post,String> PostTagDao= DaoManager.createDao((JdbcPooledConnectionSource)dbasecon, Post.class);
        TableUtils.createTableIfNotExists(connectionSource,Post.class);
        
        
        
        connectionSource.close();

       }
       catch(Exception exp)
       {
         java.lang.System.out.println(exp.getMessage());
       }
      }
      
}


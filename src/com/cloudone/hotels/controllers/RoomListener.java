/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.controllers;

import com.cloudone.hotels.data.RoomTypes;
import com.cloudone.hotels.models.CITableModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import java.awt.event.ActionListener;

/**
 *
 * @author Akala G56
 */
public class RoomListener implements MouseListener,ActionListener {
     
    private CITableModel _modelc;
    private CITableModel _modelt;
    private JTable _tablec;
    private JTable _tablet;
    private JDialog _component;
    private JPanel _jp;
    private CITableModel umodel;
    int col=0;
    List<String> roomNames= new ArrayList();
    JdbcPooledConnectionSource connectionSource=null;
    public RoomListener(){}
    public RoomListener(JTable tablec, JTable tablet,CITableModel modelc,CITableModel modelt,JDialog component,JPanel jp)
    {
      _tablec=tablec;
      _tablet=tablet;
      _modelc=modelc;
      _modelt=modelt;
      _component=component;
      _jp=jp;
      connectionSource=com.cloudone.hotels.System.getConnectionSource();
    } 
    public void actionPerformed(ActionEvent evt)
    {
      
      if(evt.getActionCommand().equals("Add"))
      {
         new com.cloudone.hotels.view.RoomUpdateFrame();
      }
      
      if(evt.getActionCommand().equals("Remove"))
      {
         new com.cloudone.hotels.view.RoomRemovalFrame();
      }   
    }
    public void mouseClicked(MouseEvent e) {
       JTable target = (JTable)e.getSource();
       int row = target.getSelectedRow();
       int column = target.getSelectedColumn();
       addAllRoomNames(this.getRoomCategoryId(row));  
       _modelt.deleteAllRows();
       presentData();
       roomNames.clear();
       _modelt.fireTableStructureChanged();
       target.repaint();
       
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }
    private int findMultiple(int size)
    { 
       int i=0;
       for(;;)
        {
          int sum=size+i;    
          if((sum%5)==0)
          {
            return i;              
          }    
           i=i+1;
        }
       
    }
    private void addAllRoomNames(int categoryId)
    {
              
             try
             {
                 com.cloudone.hotels.commons.Logging.logToFile("accessing all room names");
                 
                 Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
                 GenericRawResults<String[]> rawOutput =RoomDao.queryRaw("select _name from room where _room_type_id ='"+String.valueOf(categoryId)+"'");
                 List<String[]>  resultset= rawOutput.getResults();
                 if(!resultset.isEmpty())
                 {
                 for(int i=0;i<resultset.size();i++)
                 {
                    String[] output = resultset.get(i);
                    if(output[0]=="")
                    {
                      throw new Exception("com.cloudone.hotels.exceptions.RoomNotFoundException");
                    }
                    else
                     {
                       roomNames.add(output[0]);  
                     }
                 }
                 }
             }                 
             catch(Exception exp)
             {
                          JOptionPane.showMessageDialog(_component,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); 
                          com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
             }
             
         
        
        
    }
    private int  getRoomCategoryId(int row)
    {
             
             try
             {
                 com.cloudone.hotels.commons.Logging.logToFile("Retrieving room category id");
             
                 Dao<com.cloudone.hotels.data.RoomTypes,Integer> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
                 GenericRawResults<String[]> rawOutput =RoomTypeDao.queryRaw("select _id from roomtypes where _name ='"+String.valueOf(_modelc.getValueAt(row, 0))+"'");
                 List<String[]>  resultset= rawOutput.getResults();
                 if(!resultset.isEmpty())
                 {
                   String[] output = resultset.get(0);
                  if(output[0]=="")
                  {
                   throw new Exception("Invalid User Identity");
                  }
               else
                {
                  return Integer.parseInt(output[0]);
                }
             }
             }
             catch(Exception exp)
             {
                 com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
             }
             
      return -1;
    }    
    
    
    private void presentData()
    {
      try
      {
         int j=0,i=0;
        if(roomNames.size()%5==0)
        {
          for(;;)
          {
            _modelt.addRow(Arrays.asList(roomNames.get(j),roomNames.get(++j),roomNames.get(++j),roomNames.get(++j),roomNames.get(++j)));
            if(roomNames.size()==j+1)
              {
                j=0;
                break;
              }
            ++j;
          }
        }
       if(roomNames.size()%5!=0)
        {
          int x=this.findMultiple(roomNames.size());
          for(int y=0;y<x;y++)
          {roomNames.add("");
          }
          for(;;)
          {
           _modelt.addRow(Arrays.asList(roomNames.get(i),roomNames.get(++i),roomNames.get(++i),roomNames.get(++i),roomNames.get(++i)));
           if(roomNames.size()==i+1)
           {
             i=0;
             break;
            }
           ++i;
          }
        }
      }
      catch(IndexOutOfBoundsException exp)
      {
       JOptionPane.showMessageDialog(_component,"There are no rooms added to this category yet", "Error", JOptionPane.ERROR_MESSAGE);  
       com.cloudone.hotels.commons.Logging.logToFile(exp.getClass().getCanonicalName()+""+exp.getLocalizedMessage());
        
      }
      catch(Exception exp)
      {
         JOptionPane.showMessageDialog(_component,"an error has occurred", "Error", JOptionPane.ERROR_MESSAGE);  
         com.cloudone.hotels.commons.Logging.logToFile(exp.getClass().getCanonicalName()+""+exp.getLocalizedMessage());
      
      }
    }
    
}
     
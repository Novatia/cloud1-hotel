package com.cloudone.hotels.controllers;
import com.cloudone.hotels.data.RoomTypes;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.Arrays;
import javax.swing.*;

/**
 * Created by Akala G56 on 02/04/14.
 */

public class UserSetupListener implements ActionListener{
    private JTextField _jtffirstname;
    private JTextField _jtflastname;
    private JTextField _jtfusername;
    private JPasswordField _jpfpassword;
    private JPasswordField _jpfconfirm;
    private JComboBox _jcbrole;
    private JDialog _component;
    JdbcPooledConnectionSource connectionSource=null;
    public UserSetupListener(JTextField jtffirstname,JTextField jtflastname,JTextField jtfusername,JPasswordField jpfpassword,JPasswordField jpfconfirm,JComboBox jcbrole,JDialog comp){
        _jtffirstname=jtffirstname;
        _jtflastname=jtflastname;
        _jpfpassword=jpfpassword;
        _jpfconfirm=jpfconfirm;
        _jcbrole=jcbrole;
        _component=comp;
        _jtfusername=jtfusername;
        connectionSource=com.cloudone.hotels.System.getConnectionSource();
    }
    public void actionPerformed(ActionEvent evt){

       if(TransactionEqualityCheck())
       {
              int pwd=String.valueOf(_jpfpassword.getPassword()).hashCode();
              int confirmpwd=String.valueOf(_jpfconfirm.getPassword()).hashCode();
               if(pwd==confirmpwd)
               {   
                   try
                   {
                       connectionSource=com.cloudone.hotels.System.getConnectionSource();
                       Dao<com.cloudone.hotels.data.User,String> UserDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.User.class);
                       com.cloudone.hotels.data.User user= new com.cloudone.hotels.data.User();
                       user.setUsername(_jtfusername.getText());
                       user.setFirstName(_jtffirstname.getText());
                       user.setLastName(_jtflastname.getText());
                       user.setPassword(pwd);
                       user.setRole((String)_jcbrole.getSelectedItem());
                       UserDao.create(user);
                       reset();
                       int input= JOptionPane.showOptionDialog(_component,"User created successesfully", "Operation Status",JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                       if(input==JOptionPane.OK_OPTION)
                       {
                           _component.dispose();
                       }
                   }
                   catch(Exception exp)
                   {
                       JOptionPane.showMessageDialog(_component,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                       
                   }
                   

               }
              else if(pwd!=confirmpwd){JOptionPane.showMessageDialog(_component,"Please check your password ", "Password Coherence Issue", JOptionPane.INFORMATION_MESSAGE);}
              else{JOptionPane.showMessageDialog(_component,"An error as occurred", "Error", JOptionPane.ERROR);}
       }
       else{JOptionPane.showMessageDialog(_component,getLastError(), "Information", JOptionPane.INFORMATION_MESSAGE);}
    }
    private void reset(){
      _jtffirstname.setText("");
      _jtflastname.setText("");
      _jpfpassword.setText("");
      _jpfconfirm.setText("");
      _jtfusername.setText("");
      _jcbrole.setSelectedIndex(0);
      
    }
    public boolean TransactionEqualityCheck()
    {
          if(_jtffirstname.getText().isEmpty())
          {
             setLastError("please enter your first name");
             return false;
          }
          
          if(_jtflastname.getText().isEmpty())
          {
             setLastError("please enter your last name");
             return false;
          }
          if(_jpfpassword.getPassword().length==0)
          {
             setLastError("please enter your password");
             return false;
          }
          if(_jpfconfirm.getPassword().length==0)
          {
             setLastError("please confirm your password");
             return false;
          }
          if(_jtfusername.getText().isEmpty())
          {
             setLastError("please enter your username");
             return false;
          }
          
          return true;
     }
    public void setLastError(String msg)
    {
      message=msg;
    }
    public String getLastError()
    {
      return message;
    }
    String message;
}

package com.cloudone.hotels.controllers;

/**
 * Created by Akala G56 on 01/04/14.
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
import javax.swing.JFrame;

public class LicenseFrameEventListener implements ActionListener{
    private JTextField _jtflicense;
    private JFrame _frame;
    public LicenseFrameEventListener(JFrame frame,JTextField jtflicense){
        _jtflicense=jtflicense;
        _frame=frame;
    }
    public void actionPerformed(ActionEvent evt){
        String tempstr=_jtflicense.getText();
        if(!tempstr.isEmpty()){
            com.cloudone.hotels.commons.Logging.logToFile("Authenticating User License Key");
           //String response=System.Internet.sendInternetRequest("");
             String response="true";
             if(response.equals("false"))
              {
                try
                {
                 throw new com.cloudone.hotels.exceptions.CloudoneWatsonException("Unable validate license key");
                }
                catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
                finally
                {
                 JOptionPane.showMessageDialog(_frame, "An invalid license key exception was caught please verify your license key","License Key Error ", JOptionPane.ERROR_MESSAGE);
                 _frame.dispose();
                }
             }
             if(response.equals("true"))
             {
                 _frame.dispose();
                 new com.cloudone.hotels.view.AdminUserFrame().setVisible(true);
             }
           
        }
    }
}

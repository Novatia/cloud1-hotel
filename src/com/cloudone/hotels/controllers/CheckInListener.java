/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.controllers;

import com.cloudone.hotels.interfaces.Observer;
import com.cloudone.hotels.models.CITableModelEx;
import com.cloudone.hotels.view.ReservationFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import com.cloudone.hotels.view.TransactionGlassPane;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.Cursor;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author Akala G56
 */
public class CheckInListener  implements ActionListener,MouseListener,TableModelListener,Observer{
    TransactionGlassPane _pane; 
    JFrame _frame;
    JTable _table;
    int _row;
    List <Integer>_Id;
    List<Integer> checkedRows= new ArrayList();
    CITableModelEx _model;
    int row;
    JdbcPooledConnectionSource connectionSource=null;
    public CheckInListener(JFrame frame){
    
    _frame=frame;
    connectionSource=com.cloudone.hotels.System.getConnectionSource();
    //com.cloudone.hotels.commons.TableStateEvent.subscribe(this);
    }
    public CheckInListener(JFrame frame,JTable table,List ID,CITableModelEx model){
    _frame=frame;
    _table=table;
    _Id=ID;
    _model=model;
    connectionSource=com.cloudone.hotels.System.getConnectionSource();
    com.cloudone.hotels.commons.TableStateEvent.subscribe(this);
    _model.addTableModelListener(this);
    
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("Check In"))
        {
          
          ReservationFrame frame=new ReservationFrame(_frame);
          frame.addWindowListener(new WindowAdapter(){
          public void windowClosing(WindowEvent e) {
          _frame.getGlassPane().setVisible(false);
          }
          });
          Thread thread= new Thread(frame);
          thread.start();
        }
        if(e.getActionCommand().equals("Check Out"))
        {
            // Cursor cursor=Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
            // _frame.setCursor(cursor);
        
              checkOutGuest();
              com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
              
            //  Cursor cursordefault=Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
            // _frame.setCursor(cursordefault);
        
        
         //   }
         // }); 
         // thread.start();
        }
        if(e.getActionCommand().equals("Cancel"))
        {
             cancelGuestReservation();
              com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
        }
        
        if(e.getActionCommand().equals("Check In Reservation"))
        {
          try
          {
            checkInGuest();
            com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
          }catch(Exception exp){exp.printStackTrace();}
         
        }
        
     }
    
    public void mouseClicked(MouseEvent e) 
    {
      if(e.getClickCount()==2)
      {
        int value=_table.getSelectedRow();  
        if(value<0)
        {
          
        }
        if(value>=0)
        {
           new com.cloudone.hotels.view.ViewReservation(_Id.get(value)).setVisible(true);            
           
        }
      }  
     
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }


    @Override
    public void tableChanged(TableModelEvent e) {
                   for(int i=0;i<_table.getModel().getRowCount();i++)
                      { 
                        if ((Boolean)_table.getModel().getValueAt(i,9)==Boolean.TRUE)
                        { row=_table.getSelectedRow();
                          this.checkedRows.add(row);   
                          break;
                        }
                        if ((Boolean)_table.getModel().getValueAt(i,9)==Boolean.FALSE)
                        { 
                           row =_table.getSelectedRow(); 
                           if(checkedRows.contains(row))
                           {
                            checkedRows.remove(checkedRows.indexOf(row));
                            break;
                           }
                           
                          
                        }
                     }     
                  }
    private void deleteGuest(int Id)
    {
      
     try
     {
    
        Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
        GuestDao.deleteById(Id);        
     }
     catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
      
    }        
    private void deleteReservation(int Id)
    {
    
     try
     {
    
        Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
        com.cloudone.hotels.data.RoomReservation roomreservation=ReservationDao.queryForId(Id);
        com.cloudone.hotels.data.Guests guest= roomreservation.getGuest();       
        this.deleteGuest(guest.getId());
        ReservationDao.deleteById(Id);
     }
     catch(Exception exp){exp.printStackTrace();}
      
    }
    
    private void checkInReservation(int Id)
    {
    
     try
     {
    
        Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
        com.cloudone.hotels.data.RoomReservation roomreservation=ReservationDao.queryForId(Id);
        com.cloudone.hotels.data.RoomReservation reserve=ReservationDao.queryForId(Id);
        int roomId=reserve.getRoom().getId();
        Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);        
        com.cloudone.hotels.data.Room room=RoomDao.queryForId(roomId);
        room.setStatus(true);
        reserve.setStatus("checked in");
        RoomDao.update(room);
        ReservationDao.update(reserve);
        
     }
     catch(Exception exp){exp.printStackTrace();}
      
    }                
    private void checkoutReservation(int Id)
    {
    
     try
     {
    
        Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
        com.cloudone.hotels.data.RoomReservation roomreservation=ReservationDao.queryForId(Id);
        com.cloudone.hotels.data.RoomReservation reserve=ReservationDao.queryForId(Id);
        int roomId=reserve.getRoom().getId();
        Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);        
        com.cloudone.hotels.data.Room room=RoomDao.queryForId(roomId);
        room.setStatus(true);
        reserve.setStatus("checked out");
        RoomDao.update(room);
        ReservationDao.update(reserve);
        
     }
     catch(Exception exp){exp.printStackTrace();}
      
    }        
    private boolean IsAuthorized(int Id)
    {
    
        try
        {
    
            Dao<com.cloudone.hotels.data.RoomReservation,String> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            GenericRawResults<String[]> rawOutput =ReservationDao.queryRaw("select _status from roomreservation where _id ='"+Id+"'");
            List<String[]>  resultset= rawOutput.getResults();
            String[] output = resultset.get(0);
            if(output[0].equals("reserved"))
            {
              return true;
            }  
            else
            {
               return false;
            }
        }
        catch(Exception exp)
        {
            com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
        
        return false;
    }
    private boolean IsCheckInAuthorized(int recordId)
    {
        
        try
        {
        
            Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            GenericRawResults<String[]> rawOutput =ReservationDao.queryRaw("select _status,_checkedIn from roomreservation where _id ='"+String.valueOf("1")+"'");
            List<String[]>  resultset= rawOutput.getResults();
            String[] output = resultset.get(0);
            
                if(output[0].equals("reserved"))
                {
                  try
                  {
                  Calendar today= Calendar.getInstance();
                  SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
                  com.cloudone.hotels.commons.Logging.logToFile(format.format(today.getTime()).substring(0, 10));
                  
                  if(output[1].substring(0,10).equals(format.format(today.getTime()).substring(0, 10)))
                  {
                    setLastError("cannot check into this date yet because its not today");  
                    return false;
                  }
                  else
                  {
                    return true;
                  }
                  }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage()); exp.printStackTrace();
                  }
                }  
                else
                {
                   setLastError("could not find record"); 
                   return false;
                }
            
        }
        catch(Exception exp)
        {
            com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
           //exp.printStackTrace(); 
            
        }
        
        return false;
    }
    private boolean IsCheckOutAuthorized(int recordId)
    {
        
        try
        {
        
            Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
            GenericRawResults<String[]> rawOutput =ReservationDao.queryRaw("select _status from roomreservation where _id ='"+String.valueOf(recordId)+"'");
            List<String[]>  resultset= rawOutput.getResults();
            String[] output = resultset.get(0);
            
                if(output[0].equals("checked in")||output[0].equals("reserved"))
                {
                  return true;
                }  
                else
                {
                   return false;
                }
            
        }
        catch(Exception exp)
        {
            com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
        
        return false;
    }
    private synchronized void checkInGuest()
    {
        try
       { 
        if(checkedRows.isEmpty())
             {
               throw new Exception("no actions have been selected");
             }
           for(int i=0;i<checkedRows.size();i++)
           {
             if(checkedRows.isEmpty())
             {
               throw new Exception("no actions have been selected");
             }
             if(!checkedRows.isEmpty())
             {
               int id=_Id.get(checkedRows.get(i));
               boolean state=IsCheckInAuthorized(id); 
               if(state)
               {
                 this.checkInReservation(id);
               }
               if(!state)
               {
                com.cloudone.hotels.commons.Logging.logToFile(getLastError());  
                throw new Exception(getLastError());
               }
               
             }
             
           }
           checkedRows.clear();
         }  catch(Exception exp)
           {
               JOptionPane.showMessageDialog(_frame,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
               com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
           }
    }        
    private synchronized void checkOutGuest()
    {
      try
      { 
        if(checkedRows.isEmpty())
             {
               throw new Exception("no actions have been selected");
             }
           for(int i=0;i<checkedRows.size();i++)
           {
             if(checkedRows.isEmpty())
             {
               throw new Exception("no actions have been selected");
             }
             if(!checkedRows.isEmpty())
             {
               int id=_Id.get(checkedRows.get(i));
        
               if(IsCheckOutAuthorized(id))
               {
                 this.checkoutReservation(id);
               }
               else
               {
                 throw new Exception("could not find selected records");
               }
               
             
             }
             
           }
                                 
        
           checkedRows.clear();
           
      }catch(Exception exp)
      {
        JOptionPane.showMessageDialog(_frame,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        
      }
    }
    private synchronized void cancelGuestReservation()
    {
      try{
          
           Cursor cursor=Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
           _frame.setCursor(cursor);
        
          for(int i=0;i<checkedRows.size();i++)
               {
                  if(checkedRows.isEmpty())
                   {
                     break;
                   }
                   if(!checkedRows.isEmpty())
                   {
                     int id=_Id.get(checkedRows.get(i));
                     boolean state= IsAuthorized(id);
                     if(state)
                       {
                         _model.deleteRow(checkedRows.get(i));
                         checkoutReservation(id);
                       }
                      if(!state)
                        {
                           throw new Exception("Unable to cancel reservation");
                        }
                    }
                 }
                com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
                com.cloudone.hotels.commons.Logging.logToFile("firing table state change");
                checkedRows.clear();
                
                Cursor cursordefault=Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
                _frame.setCursor(cursordefault);
        
              }
              catch(Exception exp)
              {
               
               Cursor cursordefault=Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
               _frame.setCursor(cursordefault);   
               JOptionPane.showMessageDialog(_frame,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
               com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
               com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
              }
    
    }

    @Override
    public void update() 
    {
        com.cloudone.hotels.commons.Logging.logToFile("Update Recieved");
        List<com.cloudone.hotels.data.RoomReservation> reserve=getAllReservations();
        List<Integer> Id= new ArrayList();
       if(!reserve.isEmpty())
        {
         for(int count=reserve.size()-1;count>=0;count--)
           {
             com.cloudone.hotels.data.RoomReservation  busObj=reserve.get(count);
              Id.add(busObj.getId());       
           }
         _Id=Id;
          reserve.clear();
        }
        else{com.cloudone.hotels.commons.Logging.logToFile("Empty Reservation List  On Update");}      
        
    }
     private List<com.cloudone.hotels.data.RoomReservation> getAllReservations()
    {
      
     try
     {
        
        Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
        return ReservationDao.queryForAll();
     }
     catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
     return null;
      
    }        
     public void setLastError(String msg)
     {
       message=msg;
     }
     public String getLastError()
     {
      
       return message;
     }
     String message;
}

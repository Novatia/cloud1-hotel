/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.controllers;

import com.cloudone.hotels.models.CITableModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Akala G56
 */
public class SearchCaretListener implements CaretListener,KeyListener{
 private JTextField _jtf;
 private boolean IsFirstTrigger=true;
 private CITableModel _model;
 private JTable _table;
 private List<String[]> resultset;
 private List<String> data= new ArrayList<String>();
 private CaretEvent evt;
 JdbcPooledConnectionSource connectionSource=null;
 
    public SearchCaretListener(JTextField jtf,JTable table,CITableModel model){
       connectionSource=com.cloudone.hotels.System.getConnectionSource();
        _jtf=jtf;
      _table=table;
      _model=model;
      resultset= getAllGuestNames();
      install();
     
      
    }
    public void install(){
            for(int count=0;count<resultset.size();count++)
            {
              String[] tsqlresult=resultset.get(count);  
              data.add(tsqlresult[0]);
            }
    }
    public List<String> findPossiblePatternMatches(String pattern)
    {
      Pattern r = Pattern.compile(pattern);
       List<String> results=new ArrayList(); 
       for(int count=0;count<data.size();count++){ 
            
            String string =data.get(count);
            Matcher m = r.matcher(string);
            if (m.find( )) 
            {
                results.add(string);
            }
            else{}
          }
          return results;
       
    }
    public void caretUpdate(CaretEvent e) {
        
       
        if(IsFirstTrigger)
        {
           evt=e;
           _jtf.setText("");
           IsFirstTrigger=false;
        }
        
     if(!IsFirstTrigger)
        {
          try
          {   
              
          if(!_jtf.getText(0, _jtf.getCaretPosition()).isEmpty())  
          {
            //System.out.println("Key used"+_jtf.getText(0, _jtf.getCaretPosition()));  
            List<String> search= findPossiblePatternMatches(_jtf.getText(0, _jtf.getCaretPosition()));
            _model.deleteAllRows();
            for(int i=0;i<search.size();i++)
            {
             
              _model.addRow(Arrays.asList(search.get(i)));
            }
      
          }
          if(_jtf.getText(0, _jtf.getCaretPosition()).isEmpty())  
          {
          
            _model.deleteAllRows();
            List<com.cloudone.hotels.data.Guests> selfList=getSearchTableData();
      

             for(int count=0;count<selfList.size();count++)
             {
               com.cloudone.hotels.data.Guests guests=selfList.get(count);
               _model.addRow(Arrays.asList(guests.getFirstName()));
             }
      
          }
          
          }catch(Exception ex){ex.printStackTrace();}
        } 
        
    }
    private List<String[]>  getAllGuestNames()
    {
     
        try
        {
 
          Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
          GenericRawResults<String[]> rawOutput =GuestDao.queryRaw("select _firstname from guests");
          return rawOutput.getResults();
        }
        catch(Exception exp)
        {
            java.lang.System.out.println(exp.getMessage());
        }
        
        return null;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
        
  }
    @Override
    public void keyPressed(KeyEvent e) {
       
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
    private  java.util.List<com.cloudone.hotels.data.Guests> getSearchTableData(){

     
     try
     {

        Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
        GuestDao=DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
        return GuestDao.queryForAll();

     }
     catch(Exception exp)
     {
         exp.printStackTrace();
     }
     
        return null;
    }

}

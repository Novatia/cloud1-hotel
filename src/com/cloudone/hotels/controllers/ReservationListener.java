package com.cloudone.hotels.controllers;

import com.cloudone.hotels.view.JTitlePane;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.cloudone.hotels.commons.Date;
import com.cloudone.hotels.commons.DateHelper;
import com.cloudone.hotels.exceptions.ReservationDateException;
import com.cloudone.hotels.view.TransactionGlassPane;
import javax.swing.*;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Akala G56 on 03/04/14.
 */
public class ReservationListener implements ActionListener{
    private JTitlePane _jtp;
    private JTextField _jtfGuestName;
    private JTextArea _jtaAddress;
    private JTextField _jtfadd2;
    private JComboBox _jcbCountry;
    private JTextField _jtfPhone;
    private JTextField _jtfEmail;
    private JTextField _jtfMobile;
    private JComboBox _jcbIdentityType;
    private JTextField _jtfIdentityNo;
    private JComboBox _jtfRoomNo;
    private JComboBox _jcbRoomType;
    private JTextField _jtfArrDate;
    private JTextField _jtfArrTime;
    private JTextField _jtfDayZone;
    private JComboBox _jcbDepartureZone;
    private JComboBox _jcbChildren;
    private JTextField _jtfDepartureDate;
    private JTextField _jtfDepartureTime;
    private JTextField _jtfPrice;
    private JComboBox _jcbTitle;
    private JDialog _component;
    private JCheckBox _jcbClose;
    private String[] days= new String[7];
    private String errorMessage;
    private JButton jbCheckIn,jbReserve;
    JdbcPooledConnectionSource connectionSource=null;
    TransactionGlassPane glasspane;
    public ReservationListener(
            TransactionGlassPane pane,        
            JComboBox jcbTitle,
            JTextField jtfGuestName,
            JTextArea  jtaAddress,
            JTextField jtfadd2,
            JComboBox  jcbCountry,
            JTextField jtfPhone,
            JTextField jtfEmail,
            JTextField jtfMobile,
            JComboBox  jcbIdentityType,
            JTextField jtfIdentityNo,
            JComboBox jtfRoomNo,
            JComboBox  jcbRoomType,
            JTextField jtfArrDate,
            JTextField jtfArrTime,
            JTextField jtfDayZone,
            JComboBox  jcbDepartureZone,
            JComboBox  jcbChildren,
            JTextField jtfDepartureDate,
            JTextField jtfDepartureTime,
            JTextField jtfPrice,
            JCheckBox jcbClose,
            JDialog component

    ){
            glasspane=pane;
           _jcbTitle=jcbTitle;
           _jtfGuestName=jtfGuestName;
           _jtaAddress=jtaAddress;
           _jtfadd2=jtfadd2;
           _jcbCountry=jcbCountry;
           _jtfPhone=jtfPhone;
           _jtfEmail=jtfEmail;
           _jtfMobile=jtfMobile;
           _jcbIdentityType=jcbIdentityType;
           _jtfIdentityNo=jtfIdentityNo;
           _jtfRoomNo=jtfRoomNo;
           _jcbRoomType=jcbRoomType;
           _jtfArrDate=jtfArrDate;
           _jtfArrTime=jtfArrTime;
           _jtfDayZone=jtfDayZone;
           _jcbDepartureZone=jcbDepartureZone;
           _jcbChildren=jcbChildren;
           _jtfDepartureDate=jtfDepartureDate;
           _jtfDepartureTime=jtfDepartureTime;
           _jtfPrice=jtfPrice ;
           _jcbClose=jcbClose;
           _component=component;
           
           connectionSource=com.cloudone.hotels.System.getConnectionSource();
    }
    @Override
    public void actionPerformed(ActionEvent evt){
     if(evt.getActionCommand().equals("Reserve"))
     {
       jbReserve=(JButton) evt.getSource();  
       Thread thread= new Thread(new Runnable(){

             @Override
             public void run() {

               reserveGuest();
              // com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
             }
       
       });
       thread.start();
     }
     if(evt.getActionCommand().equals("Check In"))
     {
         jbCheckIn=(JButton) evt.getSource();
         Thread thread = new Thread(new Runnable()
         {
             public void run() {
               checkInGuest();      
              // com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
             }
         
         });
         thread.start();
     }
     
    }
   private com.cloudone.hotels.data.Room getRoom(int Id)
    {
      
     try
     {
        
        Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
        return RoomDao.queryForId(Id);        
     }
     catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());  }
     
        return null;
      
    }        
    private int  getRoomTypeId(String name)
    {
     
        try
        {
     
            Dao<com.cloudone.hotels.data.RoomTypes,String> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
            GenericRawResults<String[]> rawOutput =RoomTypeDao.queryRaw("select _id from roomtypes where _name ='"+name+"'");
            List<String[]>  resultset= rawOutput.getResults();
            String[] output = resultset.get(0);
            return Integer.parseInt(output[0]);
        }
        catch(Exception exp)
        {
            java.lang.System.out.println(exp.getMessage());
        }
        
        return -1;
    }        
   
      
   private int  getRoomId(String name,int id)
    {
        
        try
        {
        
            Dao<com.cloudone.hotels.data.Room,String> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
            GenericRawResults<String[]> rawOutput =RoomDao.queryRaw("select _id from room where _name ='"+name+"'"+" AND _room_type_id='"+id+"'");
            List<String[]>  resultset= rawOutput.getResults();
            String[] output = resultset.get(0);
            return Integer.parseInt(output[0]);
        }
        catch(Exception exp)
        {
            java.lang.System.out.println(exp.getMessage());
        }
        
        return -1;
    }        
    private void checkInGuest()
    {     
      
       try
      {
          
          if(TransactionEqualityCheck())
          {

               if(_jtfDepartureDate.getText().equals(_jtfArrDate.getText()))
               {
                 com.cloudone.hotels .commons.Logging.logToFile("com.cloudone.hotels.exceptions.ReservationDateMatchException");
                 //JOptionPane.showMessageDialog(_component,"arrival date and departure cannnot be the same", "Error", JOptionPane.ERROR_MESSAGE);
                 throw new Exception("arrival date and departure cannnot be the same");
               }
              String arrivalx=_jtfArrDate.getText(); 
             
              SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
              Calendar nowex=Calendar.getInstance();
              
              if(!_jtfArrDate.getText().equals(format.format(nowex.getTime()).toString()))
              {
                  com.cloudone.hotels.commons.Logging.logToFile("cannot check in reservation to the future rather can only reserve");
                  throw new Exception("cannot check in to this for this date");       
              }
              FutureReservation obj= new FutureReservation(String.valueOf(_jtfRoomNo.getSelectedItem()),_jtfArrDate.getText(),_jtfDepartureDate.getText());
              boolean state=obj.IsCheckInPossible();
              if(state)
              {
                if(!IsRoomReserved())
                {
               
                  jbCheckIn.setEnabled(false);     
                Dao<com.cloudone.hotels.data.RoomReservation,Integer> Dao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
                Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
                com.cloudone.hotels.data.RoomReservation reserve= new com.cloudone.hotels.data.RoomReservation();
                Calendar datetime=Calendar.getInstance();
                SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
               
               java.util.Date date = formatter.parse(_jtfArrDate.getText());
               java.util.Date date2 = formatter.parse(_jtfDepartureDate.getText());
               reserve.setCheckInDate(date);
               reserve.setCheckOutDate(date2);
               reserve.setNights(Integer.parseInt(_jtfDayZone.getText()));
               reserve.setReservationDate(datetime.getTime());
               reserve.setTotalPrice(Integer.parseInt(_jtfPrice.getText()));
               reserve.setStatus("checked in");
               Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
               com.cloudone.hotels.data.Guests guests= new com.cloudone.hotels.data.Guests();
               guests.setFirstName(_jtfGuestName.getText());
               guests.setLastName( _jtfGuestName.getText());
               guests.setAddressLine1(_jtaAddress.getText());
               guests.setAddressLine2(_jtfadd2.getText());
               guests.setPhone1(_jtfPhone.getText());
               guests.setPhone2(_jtfMobile.getText());
               guests.setCountry(String.valueOf(_jcbCountry.getSelectedItem()));
               guests.setIdentityType(String.valueOf(_jcbIdentityType.getSelectedItem()));
               guests.setIdentityNo(_jtfIdentityNo.getText());
               guests.setEmail(_jtfEmail.getText());
               GuestDao.create(guests);
               reserve.setGuest(guests);
               
               
               int id=getRoomId((String)_jtfRoomNo.getSelectedItem(),getRoomTypeId(String.valueOf(_jcbRoomType.getSelectedItem())));
               com.cloudone.hotels.data.Room room=this.getRoom(id);
               
               room.setStatus(false);
               reserve.setRoom(room);
               Dao.create(reserve);
               RoomDao.update(room);
              // if(_jcbClose.isSelected())
               //{
                  JOptionPane.showMessageDialog(_component,"Reservation was successful", "Reservation Status", JOptionPane.INFORMATION_MESSAGE);     
                  com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent(); 
                  com.cloudone.hotels.commons.Logging.logToFile("guest check in complete");  
                  jbCheckIn.setEnabled(true);      
                  
                  _component.dispose();
                  glasspane.setVisible(false);
                  
                }
              else
              { 
                  //JOptionPane.showMessageDialog(_component,"room is already reserved", "Error", JOptionPane.INFORMATION_MESSAGE);
                  throw new Exception("room is already reserved"); 
              }
             }
             if(!state)
             {
               throw new Exception("cannot check into this room probably reserved for someone else");}
             }
          else
              {
               JOptionPane.showMessageDialog(_component,getLastError(), "Information", JOptionPane.INFORMATION_MESSAGE);
               com.cloudone.hotels.commons.Logging.logToFile(getLastError());  
              }
         }
     catch(Exception exp)
           {
            JOptionPane.showMessageDialog(_component,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());   
           }
           
     }   
     private boolean IsRoomReserved()
     {
       
       int Id=getRoomTypeId(String.valueOf(this._jcbRoomType.getSelectedItem()));
            try
            {

                Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
                GenericRawResults<String[]> rawOutput =RoomDao.queryRaw("select _id,_status from room where _name ='"+String.valueOf(_jtfRoomNo.getSelectedItem())+"' AND _room_type_id='"+Id+"'");
                
                String[] output= rawOutput.getFirstResult();


                    if(output[0].isEmpty())
                    {
                      com.cloudone.hotels.commons.Logging.logToFile("com.cloudone.hotels.exceptions.RoomStatusNotFoundException");
                      JOptionPane.showMessageDialog(_component, "room has been deleted", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    else
                    {
//                            if(String.valueOf(output[1]).equals("1"))
  //                          {
                              //return false;
  //                             SimpleDateFormat format =new SimpleDateFormat("yyyy-MM-dd");
//                               System.out.println(format.format(_jtfDepartureDate.getText()));                              
                              connectionSource=com.cloudone.hotels.System.getConnectionSource();
                             Dao<com.cloudone.hotels.data.RoomReservation,Integer> RoomReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
                             GenericRawResults<String[]> rawOutputs =RoomReservationDao.queryRaw("select _status from roomreservation where _checkedIn='"+_jtfArrDate.getText()+" 00:00:00' AND _checkedOut='"+_jtfDepartureDate.getText()+ " 00:00:00' AND _room_id='"+output[0]+"'");
                             List<String[]> resultset=rawOutputs.getResults();
                             if(resultset.isEmpty())
                             {
                               return false;
                             }
                             String[] vertdata=resultset.get(0);
                             
                             if(vertdata[0].equals("checked in")) 
                             {
                               return true;
                             } 
                             if(vertdata.length==0||vertdata[0].equals("checked out")||vertdata[0].equals(""))
                             {
                               return false;
                             }
                             
    //                        }
                            
                    }
                
            }
            catch(Exception exp)
            {
                JOptionPane.showMessageDialog(this._component,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
       //         com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
            }
            
            return true;
     }
     private void reserveGuest()
     {
       
           try
           {   
             if(TransactionEqualityCheck())
             {

               if(_jtfDepartureDate.getText().equals(_jtfArrDate.getText()))
               {
                 throw new com.cloudone.hotels.exceptions.ReservationDateMatchException("Arrival Date and Departure Date cannot be the same");
                        
               }
               Calendar today = Calendar.getInstance();
               SimpleDateFormat dateformatter= new SimpleDateFormat("yyyy-MM-dd");
               java.util.Date dateStart=dateformatter.parse(dateformatter.format(today.getTime()));
               java.util.Date datearrival = dateformatter.parse(_jtfArrDate.getText());
               java.util.Date datedeparture = dateformatter.parse(_jtfDepartureDate.getText());
               if(com.cloudone.hotels.commons.DateHelper.IsBefore(datearrival, dateStart)||com.cloudone.hotels.commons.DateHelper.IsBefore(datedeparture, dateStart))
               {
                   throw new Exception("its impossible to reserve rooms from yesterday");
               }
               FutureReservation obj= new FutureReservation(String.valueOf(_jtfRoomNo.getSelectedItem()),_jtfArrDate.getText(),_jtfDepartureDate.getText());
               boolean state=obj.createFutureReservation();
               if(state)
               {
                 jbReserve.setEnabled(false);    
               com.cloudone.hotels.commons.Logging.logToFile("reservation booking has started");
               Dao<com.cloudone.hotels.data.RoomReservation,Integer> Dao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
               com.cloudone.hotels.data.RoomReservation reserve= new com.cloudone.hotels.data.RoomReservation();
               
               Calendar now = Calendar.getInstance();
               SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
               
               java.util.Date date = formatter.parse(_jtfArrDate.getText());
               java.util.Date date2 = formatter.parse(_jtfDepartureDate.getText());
               reserve.setCheckInDate(date);
               reserve.setCheckOutDate(date2);
               reserve.setNights(Integer.parseInt(_jtfDayZone.getText()));
               reserve.setReservationDate(now.getTime());
               reserve.setTotalPrice(Integer.parseInt(_jtfPrice.getText()));
               reserve.setStatus("reserved");

               Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);
               com.cloudone.hotels.data.Guests guests= new com.cloudone.hotels.data.Guests();
               guests.setFirstName(_jtfGuestName.getText());
               guests.setLastName(_jtfGuestName.getText());
               guests.setAddressLine1(_jtaAddress.getText());
               guests.setAddressLine2(_jtfadd2.getText());
               guests.setPhone1(_jtfPhone.getText());
               guests.setPhone2(_jtfMobile.getText());
               guests.setCountry(String.valueOf(_jcbCountry.getSelectedItem()));
               guests.setIdentityType(String.valueOf(_jcbIdentityType.getSelectedItem()));
               guests.setIdentityNo(_jtfIdentityNo.getText());
               guests.setEmail(_jtfEmail.getText());
               GuestDao.create(guests);
               reserve.setGuest(guests);
               
               
               int id=getRoomId((String)_jtfRoomNo.getSelectedItem(),getRoomTypeId(String.valueOf(_jcbRoomType.getSelectedItem())));
               com.cloudone.hotels.data.Room room=this.getRoom(id);
               reserve.setRoom(room);
               Dao.create(reserve);
               JOptionPane.showMessageDialog(_component,"Reservation was successful", "Reservation Status", JOptionPane.INFORMATION_MESSAGE);     
               com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
               com.cloudone.hotels.commons.Logging.logToFile("reservation booking completed");  
               jbReserve.setEnabled(true);    
               _component.dispose();
               glasspane.setVisible(false);
               }
               if(!state)
               {
                 JOptionPane.showMessageDialog(_component,obj.getLastError(), "Error", JOptionPane.ERROR_MESSAGE);
               }
             }
             else
             {
                 JOptionPane.showMessageDialog(_component,getLastError(), "Information", JOptionPane.INFORMATION_MESSAGE);
                 com.cloudone.hotels.commons.Logging.logToFile(getLastError());
              
             }
          }catch(Exception exp)
           {
              
                JOptionPane.showMessageDialog(_component,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
              
              
           }
           
     } 
     private boolean IsBookingAllowed()
     {
       
         return false;
     }
    private void getAvailableDates()
    {

        Calendar now = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 1; //add 2 if your week start on monday
        now.add(Calendar.DAY_OF_MONTH, delta );
        for (int i = 0; i < days.length; i++)
        {
            days[i] = format.format(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
        }


    }
    private void setLastError(String Message)
    {
      errorMessage= Message;
      
    }
    private String getLastError()
    {
      return errorMessage;
    }    
    
    public boolean TransactionEqualityCheck()
    {
          if(_jtfGuestName.getText().isEmpty())
          {
             setLastError("please enter your guestname");
             return false;
          }
          if(_jtaAddress.getText().isEmpty())
          {
             setLastError("please enter your address");
             return false;
          }
          if(_jtfArrDate.getText().isEmpty())
          {
             setLastError("please enter the arrival date");
             return false;
          }
          if(_jtfDepartureDate.getText().isEmpty())
          {
             setLastError("please enter the departure date");
             return false;
          }
                 if(_jtfEmail.getText().isEmpty())
          {
             setLastError("please enter the email address");
             return false;
          }
          if(!com.cloudone.hotels.commons.EmailAddress.validate(_jtfEmail.getText()))
          {
             setLastError("please verify that the email address is correect");
             return false;
          }
          if(this._jtfMobile.getText().isEmpty())
          {
             setLastError("please enter your mobile number");
             return false;
          }
          if(this._jtfPhone.getText().isEmpty())
          {
             setLastError("please enter your phone number");
             return false;
          }
          if(!com.cloudone.hotels.commons.PhoneNumberVerifier.validate(_jtfMobile.getText()))
          {
             setLastError("please verify that your phone number is correct");
             return false;
          }
   
          return true;
     }
    
}



class FutureReservation
{
    private java.util.Date _checkn;
    private java.util.Date _departuredate;
    private String _checkIn;
    private String _checkOut;
    private SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
    private String _room;
    private int _id;
    private String errMessage;
    private String[] days= new String[7];
    private JdbcPooledConnectionSource connectionSource= null;    
    public FutureReservation(String room,String checkndate,String departuredate)
    {
      try
        {
            connectionSource= com.cloudone.hotels.System.getConnectionSource();
            _room=room;
            initializeAvailableDates();
            
           _checkIn=checkndate+" 00:00:00";
           _checkOut=departuredate+" 00:00:00";
          _checkn=formatter.parse(_checkIn.substring(0,10));
          _departuredate=formatter.parse(_checkOut.substring(0,10));
          
          _id=getRoomId();
          if(_id==-1)
          {
            throw new Exception("room not found");
          }
          
        }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile("constructor"+exp.getMessage());
      }
      
    }
    public int getRoomId()
    {
       try
       {
           Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
           GenericRawResults<String[]> rawOutput =RoomDao.queryRaw("select _id from room where _name='" +_room+ "'");
           List<String[]> raw=rawOutput.getResults();
           if(raw.isEmpty())
           {
             setLastError("room cannot be found");
             return -1;
           }
           else
           {
            String[] Id = raw.get(0);
            return Integer.parseInt(Id[0]);
           }
       }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
      return -1;
    }
    private boolean IsDateCheckedInto()
    {
      try
      {
        Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
        GenericRawResults<String[]> QueryOutput =ReservationDao.queryRaw("select _id,_status,_checkedIn,_checkedOut from roomreservation where (_room_id='"+String.valueOf(_id)+"') AND (_checkedIn='"+_checkIn+"') AND (_status='checked in' OR _status='checked out')");
        List<String[]> query=QueryOutput.getResults();
        if(query.isEmpty())
        {

                  
          GenericRawResults<String[]> QueryResult =ReservationDao.queryRaw("select _id,_status,_checkedIn,_checkedOut from roomreservation where (_room_id='"+String.valueOf(this._id)+"') AND (_checkedIn>='"+days[0]+" 00:00:00') AND (_checkedIn<='"+days[6]+" 00:00:00')");
          List<String[]> queryrel=QueryResult.getResults(); 
          if(queryrel.isEmpty())
             {
               return false;
             }
                if(!queryrel.isEmpty())
                  {
                    try
                      {    
                     for(int j=0;j<queryrel.size();j++) 
                     {
                     
                      
                       String[] rawdata= queryrel.get(j); 
                       String recordId= rawdata[0];
                       String status=rawdata[1];
                       String checkin=rawdata[2];
                       String  checkout=rawdata[3];
                       java.util.Date arrival= formatter.parse(checkin.substring(0, 10));
                       java.util.Date departure=formatter.parse(checkout.substring(0, 10));
                       com.cloudone.hotels.commons.Logging.logToFile(checkin);
                       com.cloudone.hotels.commons.Logging.logToFile(checkout);
                       
                       
                       
                       com.cloudone.hotels.commons.Logging.logToFile("loop it"); 
                      if(com.cloudone.hotels.commons.DateHelper.betweenEx(_checkn, arrival, departure)||com.cloudone.hotels.commons.DateHelper.betweenEx(_departuredate, arrival, departure))
                        {
                          if(!status.equals("checked out"))
                          {
                            setState(true);
                            throw new com.cloudone.hotels.exceptions.ReservationDateException("date already reserved");
                          }
                      
                        }
                      }
                     setState(false);
                     }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
                     finally{return getState();}
                     
          }
           return false;     
                
        }
        if(!query.isEmpty())
        {
          String[] rawdata= query.get(0); 
          String recordId= rawdata[0];
          String reservationStatus= rawdata[1];
          String checkin =rawdata[2];
          String checkout=rawdata[3];
          if(reservationStatus.equalsIgnoreCase("checked out") )          
           {
             try
             {
               java.util.Date departure=formatter.parse(checkout.substring(0,10)); 
               java.util.Date arrival= formatter.parse(checkin.substring(0,10));
               if(com.cloudone.hotels.commons.DateHelper.betweenEx(_checkn, arrival, departure)||com.cloudone.hotels.commons.DateHelper.betweenEx(_departuredate, arrival, departure))
               {
                 
                 return false;
               }
               else
               {
                 return true;
               }
               
              }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}      
             return true;     
           }
          if(reservationStatus.equalsIgnoreCase("checked in"))          
           {
              try
              {
               java.util.Date departure=formatter.parse(checkout.substring(0,10)); 
               java.util.Date arrival= formatter.parse(checkin.substring(0,10));
               if(com.cloudone.hotels.commons.DateHelper.betweenEx(_checkn, arrival, departure)||com.cloudone.hotels.commons.DateHelper.betweenEx(_departuredate, arrival, departure))
               {
                   
                 return true;
               }
               else
               {
                 return false;
               }
               
              }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}      
             return true;       
           
           }
          
        }
        
       }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());} 
      return false;
    }
    private boolean IsDateReserved()
    {
       boolean checkInState= IsDateCheckedInto(); 
       if(checkInState)
         {
           this.setLastError("Unable to make reservation on this date ");
           System.out.println(getLastError());
           return true;
         }
        if(!checkInState)
         {
          try
            {
             Dao<com.cloudone.hotels.data.RoomReservation,Integer> ReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
             GenericRawResults<String[]> QueryOutput =ReservationDao.queryRaw("select _id,_status,_checkedIn,_checkedOut from roomreservation where (_room_id='"+String.valueOf(_id)+"') AND (_checkedIn='"+_checkIn+"') AND (_status='reserved')");
             List<String[]> query=QueryOutput.getResults();
             if(query.isEmpty())
              {

                GenericRawResults<String[]> QueryResult =ReservationDao.queryRaw("select _id,_status,_checkedIn,_checkedOut from roomreservation where (_room_id='"+String.valueOf(this._id)+"') AND (_checkedIn>='"+days[0]+" 00:00:00') AND (_checkedIn<='"+days[6]+" 00:00:00') AND (_status='reserved' OR _status='checked in' )");
                List<String[]> queryrel=QueryResult.getResults(); 
                if(queryrel.isEmpty())
                  {

                    return false;
                  }
                if(!queryrel.isEmpty())
                  {
                     
                   try
                     {
                        
                     for(int count=0;count<queryrel.size();count++) 
                     {
                      
                      String[] rawdata= queryrel.get(count); 
                      String recordId= rawdata[0];
                      String status=rawdata[1];
                      String checkin=rawdata[2];
                      String checkout=rawdata[3];
                      java.util.Date departure=formatter.parse(checkout.substring(0, 10)); 
                      java.util.Date arrival= formatter.parse(checkin.substring(0, 10));
                      if(com.cloudone.hotels.commons.DateHelper.betweenEx(_checkn, arrival, departure)||com.cloudone.hotels.commons.DateHelper.betweenEx(_departuredate, arrival, departure))
                        {
                          if(!status.equals("checked out"))
                          { 
                           setState(true); 
                           throw new com.cloudone.hotels.exceptions.ReservationDateException("date already reserved2");
                          }
                        }
                     }
                     setState(false);
                    }
                    catch(com.cloudone.hotels.exceptions.ReservationDateException exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
                    finally{return getState();}
                     
                  }
                   return false;     
                   
                } 
                
                
              if(!query.isEmpty())
              {
                String[] rawdata= query.get(0); 
                String recordId= rawdata[0];
                String reservationStatus= rawdata[1];
                String checkin =rawdata[2];
                String checkout=rawdata[3];
                if(reservationStatus.equalsIgnoreCase("reserved") )          
                  {
                   try
                    {
                      java.util.Date departure=formatter.parse(checkout.substring(0,10)); 
                      java.util.Date arrival= formatter.parse(checkin.substring(0,10));
                      if(com.cloudone.hotels.commons.DateHelper.betweenEx(_checkn, arrival, departure)||com.cloudone.hotels.commons.DateHelper.betweenEx(_departuredate, arrival, departure))
                        {
                           return true;
                        }
                     else
                       {
                         return false;
                       }
               
                    }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}      
                  
                } 
                else{return true;}
              }
              
           }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());} 
        }
       return false;
    }
    public boolean IsCheckInPossible()
    {
      if(IsDateCheckedInto())
      {
      
          return false;
      }
      if(!IsDateCheckedInto())
      {
      
        return true;
      }
      return false;
    }
    public boolean createFutureReservation()
    {
      if(IsDateCheckedInto())
      {
        setLastError("unable to make reservation on this date someone is checked into this room");
        return false;
      }
      
      if(!IsDateCheckedInto())
      {
          if(IsDateReserved())
          {
           setLastError("this room has already been reserved by someone else");   
           return false;
          }
          
          if(!IsDateReserved())
          {
            return true;
          }
      }
      return false;
    
    }
    public void setLastError(String err)
    {
        errMessage=err;
    }
    public String getLastError()
    {
       return errMessage;
    }
    public void shutdown()
    {
     try
     {
       connectionSource.close();         
     }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
        
    }
 private void initializeAvailableDates()
    {

      Calendar now = Calendar.getInstance();
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 1; //add 2 if your week start on monday
      now.add(Calendar.DAY_OF_MONTH, delta );
      for (int i = 0; i < days.length; i++)
      {
         days[i] = format.format(now.getTime());
         now.add(Calendar.DAY_OF_MONTH, 1);
      }
    }
private void setState(boolean state )
 {
     
  _state=state;   
     
   
}   
private boolean getState()
{
  return _state;   
    
}
private boolean _state;
    }
 
 
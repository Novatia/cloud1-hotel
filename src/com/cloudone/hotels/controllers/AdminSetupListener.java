package com.cloudone.hotels.controllers;

/**
 * Created by Akala G56 on 01/04/14.
 */
import com.cloudone.hotels.data.Hotel;
import com.cloudone.hotels.data.User;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class AdminSetupListener implements ActionListener{
    private JFrame _component;
    private JButton _jbSubmit;
    private JPasswordField _jpPasswordField;
    private JTextField _jtFirstname;
    private JTextField _jtLastname;
    private JTextField _jtUsername;
    private JTextField _jtfCity;
    private JPasswordField _jpfConfirm;
    private JComboBox _jcbCountry;
    private JTextField _jtfCurrency;
    private JTextField _jtfEmail;
    private JTextField _jtfHotel;
    private JTextField _jtfPhone;
    JdbcPooledConnectionSource connectionSource=null;
    public AdminSetupListener(
            JFrame frame,
            JButton jbSubmit,
            JPasswordField jpPasswordField,
            JTextField jtFirstname,
            JTextField jtLastname,
            JTextField jtUsername,
            JTextField jtfCity,
            JPasswordField jpfConfirm,
            JComboBox jcbCountry,
            JTextField jtfCurrency,
            JTextField jtfEmail,
            JTextField jtfHotel,
            JTextField jtfPhone
    ){
     _component =frame;
     _jbSubmit=jbSubmit;
     _jpPasswordField=jpPasswordField;
     _jtFirstname=jtFirstname;
     _jtLastname=jtLastname;
     _jtUsername=jtUsername;
     _jtfCity=jtfCity;
     _jpfConfirm=jpfConfirm;
     _jcbCountry=jcbCountry;
     _jtfCurrency=jtfCurrency;
     _jtfEmail=jtfEmail;
     _jtfHotel=jtfHotel;
     _jtfPhone=jtfPhone;
     connectionSource=com.cloudone.hotels.System.getConnectionSource();
 
    }
    public void actionPerformed(ActionEvent evt){
        if(TransactionEqualityCheck())
        {
            int pwd=String.valueOf(_jpPasswordField.getPassword()).hashCode();
            int confirmpwd=String.valueOf(_jpfConfirm.getPassword()).hashCode();
            if(pwd==confirmpwd)
            {
                try
                {
    
                    Dao<Hotel,String> HotelDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Hotel.class);
                    com.cloudone.hotels.data.Hotel busObj= new com.cloudone.hotels.data.Hotel();
                    busObj.setName(this._jtfHotel.getText());
                    busObj.setCurrency(this._jtfCurrency.getText());
                    busObj.setCurrencyAbbr(this._jtfCurrency.getText());
                    busObj.setCurrencySymbol(this._jtfCurrency.getText());
                    busObj.setCity(this._jtfCity.getText());
                    busObj.setEmail(this._jtfEmail.getText());
                    busObj.setCountry((String)_jcbCountry.getSelectedItem());
                    busObj.setPhone(_jtfPhone.getText());
                    HotelDao.create(busObj);
                    
                    Dao<User,Integer> UserDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.User.class);                    
                    com.cloudone.hotels.data.User user= new com.cloudone.hotels.data.User();
                    user.setUsername(_jtUsername.getText());
                    user.setFirstName(_jtFirstname.getText());
                    user.setLastName(_jtLastname.getText());
                    user.setRole("administrator");
                    user.setPassword(pwd);
                    UserDao.create(user);
                   
                    reset();
                    int input= JOptionPane.showOptionDialog(_component, "Administrator created successesfully", "Operation Status", JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                    if(input==JOptionPane.OK_OPTION)
                    {
                        _component.dispose();
                        new com.cloudone.hotels.view.LoginFrame().setVisible(true);
                    }
                }
                catch(Exception exp)
                {
                   
                    JOptionPane.showMessageDialog(_component,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                }
                

            }
            else if(pwd!=confirmpwd){JOptionPane.showMessageDialog(_component,"Please check your password ", "Password Coherence Issue", JOptionPane.INFORMATION_MESSAGE);}
            else{JOptionPane.showMessageDialog(_component,"An error as occurred", "Error", JOptionPane.ERROR);}
        }
        else{JOptionPane.showMessageDialog(_component,getLastError(), "Warning", JOptionPane.INFORMATION_MESSAGE);}
    }
    private void reset(){
       _jtFirstname.setText("");
       _jtLastname.setText("");
       _jpPasswordField.setText("");
       _jpfConfirm.setText("");
       _jtUsername.setText("");
       _jtfCity.setText("");
       _jcbCountry.setSelectedIndex(0);
       _jtfCurrency.setText("");
       _jtfEmail.setText("");
       _jtfHotel.setText("");
       _jtfPhone.setText("");
    }
     public boolean TransactionEqualityCheck()
    {
          if(_jtFirstname.getText().isEmpty())
          {
             setLastError("please enter your firstname");
             return false;
          }
          if(_jtLastname.getText().isEmpty())
          {
             setLastError("please enter your lastname");
             return false;
          }
          if(_jpPasswordField.getPassword().length==0)
          {
             setLastError("password  cannot be left empty");
             return false;
          }
          if(_jpPasswordField.getPassword().length==0)
          {
             setLastError("password confirmation cannot be left empty");
             return false;
          }
          if(_jtUsername.getText().isEmpty())
          {
             setLastError("please enter your username");
             return false;
          }
          if(_jtfCity.getText().isEmpty())
          {
             setLastError("please enter your city");
             return false;
          }
          
          if(_jtfPhone.getText().isEmpty())
          {
             setLastError("please  your phone number");
             return false;
          }
          if(!com.cloudone.hotels.commons.PhoneNumberVerifier.validate(_jtfPhone.getText()))
          {
             setLastError("please validate your phone number");
             return false;
          }
          if(_jtfEmail.getText().isEmpty())
          {
             setLastError("please enter your email address");
             return false;
          }
          if(!com.cloudone.hotels.commons.EmailAddress.validate(_jtfEmail.getText()))
          {
             setLastError("please validate your email address");
             return false;
          }
          if(_jtfHotel.getText().isEmpty())
          {
             setLastError("please enter hotel credentials");
             return false;
          }
          if(_jtfCurrency.getText().isEmpty())
          {
             setLastError("please enter your currency");
             return false;
          }
          return true;
     }
     private void setLastError(String error)
     {
       message=error; 
     }
     private String getLastError()
     {
       return message;
     }
     private String message;
}

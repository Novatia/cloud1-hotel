/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.controllers;

import com.cloudone.hotels.view.ReservationDashBoard;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.List;
import javax.swing.JFrame;

/**
 *
 * @author Akala G56
 */
public class LogoutListener implements ActionListener{
    private ReservationDashBoard _parentComponent;
    JdbcPooledConnectionSource connectionSource=null;
    public LogoutListener(ReservationDashBoard parentComponent)
    {
      _parentComponent=parentComponent;
       connectionSource=com.cloudone.hotels.System.getConnectionSource();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
           
     com.cloudone.hotels.commons.Logging.logToFile("Authenticating User Id");
        try
        {
            com.cloudone.hotels.commons.Logging.logToFile("logging out user");
           
            Dao<com.cloudone.hotels.data.System,Integer> SystemDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.System.class);
            com.cloudone.hotels.data.System sys=SystemDao.queryForId(1);
            Calendar now=Calendar.getInstance();
            sys.setSystemLogoutDate(now.getTime());
            SystemDao.update(sys);
            com.cloudone.hotels.commons.Logging.logToFile("logout finished");
            System.gc();
            _parentComponent.dispose();
            
            new com.cloudone.hotels.view.LoginFrame();
        }
        catch(Exception exp)
        {
          com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
        }
        

    }
    
    
    
}

package com.cloudone.hotels.controllers;

/**
 * Created by Akala G56 on 01/04/14.
 */
import com.cloudone.hotels.data.Room;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;

import com.cloudone.hotels.data.RoomTypes;
import com.cloudone.hotels.models.CITableModel;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class RoomTypeListener  implements ActionListener,MouseListener{
    private JTextField _jtfRoomName;
    private JTextField _jtfRoomPrice;
    private JTextField _jtfMaxGuest;
    private CITableModel _model;
    private JTable _table;
    private JDialog _component;
    JdbcPooledConnectionSource connectionSource=null;
    public RoomTypeListener(JTextField jtfMaxGuest,JTextField jtfRoomName,JTextField jtfRoomPrice,CITableModel model,JTable table,JDialog component){
      _jtfRoomName=jtfRoomName;
      _jtfRoomPrice=jtfRoomPrice;
      _jtfMaxGuest= jtfMaxGuest;
      _model=model;
      _table=table;
      _component=component;
      connectionSource=com.cloudone.hotels.System.getConnectionSource(); 
    
    } 
    public void actionPerformed(ActionEvent e) {
        
       Thread thread = new Thread(new Runnable(){
         public void run(){
           addRoomType();
         }
       });        
       thread.start();
    }
    @Override
    public void mouseClicked(MouseEvent e) {
    if (e.getClickCount() == 2) {
         
       Thread thread = new Thread(new Runnable(){
         public void run(){
           deleteRoomType();
         }
       });        
      thread.start();
      }
    }
    public void reset(){
      _jtfRoomName.setText("");
      _jtfRoomPrice.setText("");
      _jtfMaxGuest.setText("");
    }
    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
    }
    private void addRoomType()
    {
       String temp=_jtfRoomName.getText();
        String temp2=_jtfRoomPrice.getText();
        String temp3=_jtfMaxGuest.getText();
       if(!temp.isEmpty()&&!temp2.isEmpty()&&!temp3.isEmpty())
       {
           
      
           try
           {
      
               
                 Dao<RoomTypes,Integer> RoomTypesDao= DaoManager.createDao(connectionSource, RoomTypes.class);
                 GenericRawResults<String[]> rawOutput =RoomTypesDao.queryRaw("select _name from roomtypes where _name='"+temp+"'");
                  String[] arr=rawOutput.getFirstResult();
                 if(arr==null)
                 {
                   com.cloudone.hotels.data.RoomTypes roomtype= new com.cloudone.hotels.data.RoomTypes();
                   roomtype.setName(temp);
                   roomtype.setPrice(Integer.parseInt(temp2));
                   roomtype.setMaxGuestNumber(Integer.parseInt(temp3));
                   RoomTypesDao.create(roomtype);
                  _model.addRow(Arrays.asList(temp3,temp, temp2));
                  JOptionPane.showMessageDialog(_component,"transaction successfull", "Error", JOptionPane.INFORMATION_MESSAGE);
                  
                  reset();
                  com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
                
                 } 
                 else if(arr!=null)
                 {
                   JOptionPane.showMessageDialog(_component,"room category exists already", "Error", JOptionPane.ERROR_MESSAGE);
                 }
                 else{}
               
           }
           catch(Exception exp)
           {
               exp.printStackTrace();
           }
           
       }
    }
    private void deleteRoomType()
    {
           int row= _table.getSelectedRow();
         int input= JOptionPane.showOptionDialog(_component,"Are you sure you want to delete this row", "Delete Row",JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);  
         if(input==JOptionPane.YES_OPTION)
         {
           
             try
             {
                 
           
                 Dao<RoomTypes,Integer> RoomTypesDao= DaoManager.createDao(connectionSource, RoomTypes.class);
                 GenericRawResults<String[]> rawOutput =RoomTypesDao.queryRaw("select _id from roomtypes where _name='"+String.valueOf(_model.getValueAt(row, 1)) +"'");
                 List<String[]>  resultset= rawOutput.getResults();
                 String[] output = resultset.get(0);
                 if(output[0]==null)
                 {
                   throw new Exception("Invalid Room Type");
                 }
                 if(output[0]!=null)
                 {
                 
                 Dao<Room,Integer> RoomDao= DaoManager.createDao(connectionSource, Room.class);
                 GenericRawResults<String[]> raw =RoomDao.queryRaw("select _id from room where _room_type_id='"+output[0]+"'");
                 List<String[]>  result= raw.getResults();
                 if(result.isEmpty())
                 {
                 
                   //throw new com.cloudone.hotels.exceptions.ForeignKeyConstraintException("ForeignKeyConstraintException");
                   
                   RoomTypesDao.deleteById(Integer.parseInt(output[0]));
                  _model.deleteRow(row);  
                  }
                 else
                 {
                    for(int count=0;count<result.size();count++)
                    {
                      String[] res = result.get(count);
                      if(res[0]==null)
                      {
                        throw new Exception();
                      }
                      if(res[0]!=null)
                      {
                        RoomDao.deleteById(Integer.parseInt(res[0]));
                      }

                    }

                      RoomTypesDao.deleteById(Integer.parseInt(output[0]));
                      _model.deleteRow(row);
                      com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();

                 } 
                 }
            
             }
             catch(Exception exp)
             {
             
              JOptionPane.showMessageDialog(_component,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
             }
             
         }     
    }
        
}




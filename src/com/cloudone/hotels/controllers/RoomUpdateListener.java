package com.cloudone.hotels.controllers;

/**
 * Created by Akala G56 on 04/04/14.
 */
import com.cloudone.hotels.data.User;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

import java.awt.event.*;
import java.util.List;
import javax.swing.*;
public class RoomUpdateListener implements ActionListener {
    private JTextField _jtf;
    private JTextField _jtfp;
    private JComboBox _jcb;
    private JDialog _jd;
    JdbcPooledConnectionSource connectionSource=null;
    public RoomUpdateListener(JTextField jtf,JComboBox jcb,JDialog dialog)
    {
        _jtf=jtf;
        _jcb=jcb;
        _jd=dialog;
        connectionSource=connectionSource=com.cloudone.hotels.System.getConnectionSource();
        
    }
    
    public RoomUpdateListener(JTextField jtf,JTextField jtfp,JComboBox jcb,JDialog dialog)
    {
        _jtf=jtf;
        _jcb=jcb;
        _jd=dialog;
        _jtfp=jtfp; 
        connectionSource=connectionSource=com.cloudone.hotels.System.getConnectionSource();
    }
    public void actionPerformed(ActionEvent evt){
     
     if(evt.getActionCommand().equals("Save"))
      {
          String roomName=_jtf.getText();
          String Fstr= _jtfp.getText();
          String[] pattern=Fstr.split(",");
      

        if(!roomName.isEmpty()&&!_jtfp.getText().isEmpty())
        {
            
            try
            {
                
                Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
                Dao<com.cloudone.hotels.data.RoomTypes,Integer> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
                
                System.out.println(_jcb.getSelectedItem());
                GenericRawResults<String[]> rawOutput =RoomTypeDao.queryRaw("select _id from roomtypes where _name ='" + String.valueOf(_jcb.getSelectedItem()) + "'");
                List<String[]>  resultset= rawOutput.getResults();
                String[] output = resultset.get(0);
                
                GenericRawResults<String[]> raw =RoomDao.queryRaw("select _id from room where _name='"+this._jtf.getText()+"'");
                List<String[]>  resultsetdata= raw.getResults();
                if(!resultsetdata.isEmpty())
                {
                  
                  com.cloudone.hotels.commons.Logging.logToFile("This room already exists ");
                  JOptionPane.showMessageDialog(_jd,"This room already exists ", "Warning", JOptionPane.WARNING_MESSAGE); 
                } 
                
                else
                {
                
                    com.cloudone.hotels.data.RoomTypes roomtype=RoomTypeDao.queryForId(Integer.parseInt(output[0]));
                    com.cloudone.hotels.data.Room room=new com.cloudone.hotels.data.Room();
                    room.setName(roomName);
                    room.setRoomType(roomtype);
                    room.setStatus(true);
                    RoomDao.create(room);
                    
                    Dao<com.cloudone.hotels.data.Facilities,Integer> FacilityDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Facilities.class);  
                    com.cloudone.hotels.data.Facilities facility=new com.cloudone.hotels.data.Facilities();
                    Dao<com.cloudone.hotels.data.RoomFacilities,Integer> RoomFacilityDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomFacilities.class);  
                    com.cloudone.hotels.data.RoomFacilities facilities=new com.cloudone.hotels.data.RoomFacilities();
                    facilities.setRoom(room);
                    for(int count=0;count<pattern.length;count++)
                    {
                      facility.setName(pattern[count]);
                      FacilityDao.create(facility);
                      facilities.setFacility(facility);
                      RoomFacilityDao.create(facilities);
                    }
                    
                    _jtf.setText("");
                    _jcb.setSelectedIndex(0);
                    com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
                    _jd.dispose();

                }
            }
            catch(Exception exp)
            {
              JOptionPane.showMessageDialog(_jd,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); 
            }
            
        }
    }
   if(evt.getActionCommand().equals("Remove"))  
   {
        String roomName=_jtf.getText();
        
            try
            {
        
                Dao<com.cloudone.hotels.data.Room,Integer> RoomDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Room.class);
                Dao<com.cloudone.hotels.data.RoomTypes,Integer> RoomTypeDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomTypes.class);
                Dao<com.cloudone.hotels.data.RoomReservation,Integer> RoomReservationDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomReservation.class);
                Dao<com.cloudone.hotels.data.Guests,Integer> GuestDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Guests.class);                
                
                GenericRawResults<String[]> rawOutput =RoomTypeDao.queryRaw("select _id from roomtypes where _name ='" + String.valueOf(_jcb.getSelectedItem()) + "'");
                List<String[]>  resultset= rawOutput.getResults();
                if(resultset.isEmpty())
                {
                   
                  com.cloudone.hotels.commons.Logging.logToFile("Unable validate room type");
                  JOptionPane.showMessageDialog(_jd,"Unable validate room type", "Warning", JOptionPane.WARNING_MESSAGE); 
                }
                else
                {
                    String[] output = resultset.get(0);

                    GenericRawResults<String[]> rawOutputs =RoomDao.queryRaw("select _id from room where _name ='" + roomName + "' AND _room_type_id='"+output[0]+"'");
                    List<String[]>  resultsets= rawOutputs.getResults();
                    if(resultsets.isEmpty())
                    {
                      
                      com.cloudone.hotels.commons.Logging.logToFile("no valid room in the category");
                      JOptionPane.showMessageDialog(_jd,"no valid room in the category", "Error", JOptionPane.ERROR_MESSAGE); 

                    }
                    else
                    {
                          String[] outputs = resultsets.get(0);                        
                          GenericRawResults<String[]> data =RoomReservationDao.queryRaw("select _id,_status from roomreservation where _room_id='"+outputs[0]+"'");
                          List<String[]>  result= data.getResults();
                          Dao<com.cloudone.hotels.data.RoomFacilities,Integer> RoomFacilityDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.RoomFacilities.class);  
                          Dao<com.cloudone.hotels.data.Facilities,Integer> FacilityDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.Facilities.class);  
                          com.cloudone.hotels.data.RoomTypes roomtype=RoomTypeDao.queryForId(Integer.parseInt(outputs[0]));
                          com.cloudone.hotels.data.Room room=RoomDao.queryForId(Integer.parseInt(outputs[0]));

                          if(result.isEmpty())
                             {
                                 GenericRawResults<String[]> raw =RoomFacilityDao.queryRaw("select _facility_id from roomfacilities where _room_id='"+String.valueOf(room.getId())+"'");
                                  List<String[]>  rel= raw.getResults();
                                  if(rel.isEmpty())
                                    {
                                       com.cloudone.hotels.commons.Logging.logToFile("foreign key constraint in database from roomfacilities");
                                       JOptionPane.showMessageDialog(_jd,"constraint error", "Error", JOptionPane.ERROR_MESSAGE);                                         
                                    } 
                                else
                                   {
                                     String id="";
                                     for(int i=0;i<rel.size();i++)
                                      {
                                        
                                        try
                                        {
                                          String[] Id= rel.get(i);  
                                          id=Id[0];
                                          FacilityDao.deleteById(Integer.parseInt(id));
                                        }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
                                        
                                      }
                                   
                                      
                                      //if(room.getId()!=roomtype.getId())
                                       //{
                                        //   com.cloudone.hotels.commons.Logging.logToFile("foreign key constraint in database from roomfacilitiesxxxxxx");
                                       //JOptionPane.showMessageDialog(_jd,"constraint errorxxxxxxxxx", "Error", JOptionPane.ERROR_MESSAGE);                                         
                                      // }
                                        GenericRawResults<String[]> Query =RoomFacilityDao.queryRaw("select _id from roomfacilities where _room_id='"+String.valueOf(room.getId())+"'");
                                        List<String[]>  relQuery= Query.getResults();
                                        if(!relQuery.isEmpty())
                                        {
                                          String[] dataOutput;  
                                          for(int j=0;j<relQuery.size();j++)
                                          {
                                              dataOutput=relQuery.get(j);
                                              com.cloudone.hotels.data.RoomFacilities busObj=RoomFacilityDao.queryForId(Integer.parseInt(dataOutput[0]));
                                              RoomFacilityDao.delete(busObj);
                                          }
                          //                com.cloudone.hotels.data.RoomReservation  reservatn=RoomReservationDao.queryForId(Integer.parseInt(Id[0]));
                           //               com.cloudone.hotels.data.Guests  guest=GuestDao.queryForId(reservatn.getGuest().getId());
                           //               GuestDao.delete(guest);
                            //              RoomReservationDao.delete(reservatn);
                                          RoomDao.delete(room);
                                        }
                                        com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
                                                                         
                                     }

                             }
                            else
                             {
                                String[] tSQL=result.get(0);
                                if(tSQL[1].equals("checked out")||tSQL[1].isEmpty())
                                {
                                  RoomReservationDao.deleteById(Integer.parseInt(tSQL[0]));
                                  GenericRawResults<String[]> raw =RoomFacilityDao.queryRaw("select _facility_id from roomfacilities where _room_id='"+String.valueOf(room.getId())+"'");
                                  List<String[]>  rel= raw.getResults();
                                  if(rel.isEmpty())
                                    {
                                            com.cloudone.hotels.commons.Logging.logToFile("foreign key constraint in database from _facility_id roomfacilities");
                                            JOptionPane.showMessageDialog(_jd,"foreign key constraint in database from _facility_id roomfacilities", "Error", JOptionPane.ERROR_MESSAGE);                                                                                
                                    } 
                                else
                                   {
                                     
                                     for(int i=0;i<rel.size();i++)
                                      {
                                        String[]  Id= rel.get(i);  
                                        
                                        com.cloudone.hotels.data.Facilities facility=FacilityDao.queryForId(Integer.parseInt(Id[0]));
                                        FacilityDao.delete(facility);
                                      }
//                                      if(room.getId()!=roomtype.getId())
  //                                     {
    //                                        com.cloudone.hotels.commons.Logging.logToFile("foreign key constraint in database from _facility_id roomfacilitiesyyyy");
//      //                                      JOptionPane.showMessageDialog(_jd,"foreign key constraint in database from _facility_id roomfacilitiesyyyyyyy", "Error", JOptionPane.ERROR_MESSAGE);                                                                                
          //                             }
                                        
                                          com.cloudone.hotels.data.RoomReservation  reservatn=RoomReservationDao.queryForId(Integer.parseInt(tSQL[0]));
                                          com.cloudone.hotels.data.Guests  guest=GuestDao.queryForId(reservatn.getGuest().getId());
                                          GuestDao.delete(guest);
                                          RoomReservationDao.delete(reservatn);
                                           RoomDao.delete(room);
                                          com.cloudone.hotels.commons.TableStateEvent.fireTableStateEvent();
                                          
                                           _jtf.setText("");
                                           _jcb.setSelectedIndex(0);
                                            this._jd.dispose();
                                   
                                }

                                
                                }
                                else
                                {
                                  com.cloudone.hotels.commons.Logging.logToFile("this room has already been checked into");
                                  JOptionPane.showMessageDialog(_jd,"this room has already been checked into", "Error", JOptionPane.ERROR_MESSAGE);                                                                                
                                }
                             }                     
                    }
                }
            }
            catch(Exception exp)
            {
              com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
              JOptionPane.showMessageDialog(_jd,"an error occured", "Error", JOptionPane.ERROR_MESSAGE);                                                                                
            }
            
         _jtf.setText("");
         _jcb.setSelectedIndex(0);
         this._jd.dispose();
   }
  }
  
    
}

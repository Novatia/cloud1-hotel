package com.cloudone.hotels.controllers;

/**
 * Created by Akala G56 on 02/04/14.
 */
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import com.cloudone.hotels.data.User;
import com.j256.ormlite.dao.GenericRawResults;
import com.cloudone.hotels.view.ReservationDashBoard;
import java.util.List;
import javax.swing.JOptionPane;
public class LoginListener implements ActionListener{
private JTextField _jtfUsername;
private JPasswordField _jpfPassword;
private JFrame _component;
JdbcPooledConnectionSource connectionSource=null;
    public LoginListener(JTextField jtfUsername,JPasswordField jpfPassword,JFrame component){
        _jtfUsername=jtfUsername;
        _jpfPassword=jpfPassword;
        _component= component;
        connectionSource=com.cloudone.hotels.System.getConnectionSource();
        
    }
    public void actionPerformed(ActionEvent evt){

     String uid=_jtfUsername.getText();
     int pwd=String.valueOf(_jpfPassword.getPassword()).hashCode();

     com.cloudone.hotels.commons.Logging.logToFile("Authenticating User Id");
        try
        {

            Dao<com.cloudone.hotels.data.User,String> UserDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.User.class);
            GenericRawResults<String[]> rawOutput =UserDao.queryRaw("select _password from User where _username ='"+uid+"'");
            List<String[]>  resultset= rawOutput.getResults();
            String[] output = resultset.get(0);
            if(resultset.isEmpty())
            {
              throw new Exception("Incorrect user details");
            }
            if(output[0]==null)
            {
               throw new Exception("incorrect authentication credentials please check");
            }
            else
            {
               if(pwd!=Integer.parseInt(output[0]))
               {
                  throw new Exception("incorrect authentication credentials please check");
               }
               else if(pwd==Integer.parseInt(output[0]))
               {
                   try
                   {
                     
              
                     com.cloudone.hotels.AppConstants.UID=1;
                     com.cloudone.hotels.AppConstants.PASSWORD=pwd;
                    _component.dispose();
                    Dao<com.cloudone.hotels.data.System,Integer> SystemDao= DaoManager.createDao(connectionSource, com.cloudone.hotels.data.System.class);
                    com.cloudone.hotels.data.System sys=SystemDao.queryForId(1);
                    sys.setIsFirstLogon(false);
                    SystemDao.update(sys);
                    com.cloudone.hotels.commons.Logging.logToFile("User logged in with user id:"+uid);
                    com.cloudone.hotels.view.ReservationDashBoard dash=new com.cloudone.hotels.view.ReservationDashBoard();
                    
                    
                    //new com.cloudone.hotels.controllers.LogoutListener(dash);
                   }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
               }
               else{}
            }

        }
        catch(Exception exp)
        {
          
            //com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());
            JOptionPane.showMessageDialog(_component,exp.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            
        }
        
    }
}



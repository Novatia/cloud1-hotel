/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.commons;

/**
 *
 * @author Akala G56
 */
import com.cloudone.hotels.interfaces.TableStateController;
public class TableStateEvent {
    private static TableStateController controller;
    
    private TableStateEvent(){
      controller= new TableStateController();
    }
    public static void Init(){
       new TableStateEvent();
    }
    public static void subscribe(com.cloudone.hotels.interfaces.Observer o){
              controller.register(o);
    }
    public static void fireTableStateEvent(){
      controller.notifyObserver();
    }
}


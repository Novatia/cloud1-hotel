/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.commons;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Akala G56
 */
public class EmailAddress {
public static final Pattern EMAIL_REGEX = 
    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

public static boolean validate(String emailStr) {
        Matcher matcher = EMAIL_REGEX .matcher(emailStr);
        return matcher.find();
}    
}

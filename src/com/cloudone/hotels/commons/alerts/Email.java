/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.commons.alerts;

/**
 *
 * @author Akala G56
 */
import java.util.Properties;
 
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {
    
    private String sendersAddr;
    private String recipientAddr;
    private String messageSubject;
    private Session session;
    public void setSendersAddress(String sender)
    {
      sendersAddr= sender;
    }
    public String getSendersAddress()
    {
      return sendersAddr;
    }
    public void setRecipientAddress(String recipient)
    {
      recipientAddr=recipient;
    }
    public String getRecipientAddress()
    {
      return recipientAddr;
    }
    public void setSubject(String subject)
    {
      messageSubject=subject;
    }
    
}

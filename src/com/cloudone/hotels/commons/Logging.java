/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.commons;

import com.cloudone.hotels.AppConstants;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;

/**
 *
 * @author Akala G56
 */
public class Logging {
    
    private static File logger;
    private Logging()
    {
      try
      {
       logger=new File(AppConstants.APP_DEFAULT);       
      }catch(Exception exp){exp.printStackTrace();}    
    } 
    public static Logging createInstance()
    {
      return new Logging();
    }
    public static void logToFile(String logMessage) 
    {
        try
        {
         String dateformat=String.format("%tc", Calendar.getInstance());   
         String msgformat= "[ "+dateformat+" | ] "+ logMessage; 
         FileWriter logFileWriter = new FileWriter(logger.getAbsoluteFile(), true);
         BufferedWriter logBufferedWriter = new BufferedWriter(logFileWriter);
         logBufferedWriter.write(msgformat+"\n"); 
         logBufferedWriter.close();
       
        }catch(Exception exp){exp.printStackTrace();}
    }      
}

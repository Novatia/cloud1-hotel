/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.commons;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

        public static final String DATE_FORMAT = "dd-MM-yyyy";	//See Java DOCS for different date formats
        
        public static long daysBetween2Dates(Calendar c1,Calendar c2){
            //System.out.println("Days Between "+c1.getTime()+" and "+ c2.getTime()+" is");
            return (c2.getTime().getTime() - c1.getTime().getTime())/(24*3600*1000);
        }

        public static void daysInMonth(Calendar c1) {

            //Calendar c1 = Calendar.getInstance(); 	//new GregorianCalendar();
            //c1.set(1999, 6 , 20);
            int year = c1.get(Calendar.YEAR);
            int month = c1.get(Calendar.MONTH);
//	    int days = c1.get(Calendar.DATE);
            int [] daysInMonths = {31,28,31,30,31,30,31,31,30,31,30,31};
            daysInMonths[1] += DateHelper.isLeapYear(year) ? 1 : 0;
            System.out.println("Days in "+month+"th month for year "+year+" is "+ daysInMonths[c1.get(Calendar.MONTH)]);
            System.out.println();
            System.out.println("-------------------------------------------------------");
        }

        public static int compare2Dates(Calendar c1,Calendar c2)
        {
            System.out.println("6. Comparision of 2 dates\n");
            SimpleDateFormat fm = new SimpleDateFormat("dd/MM/yyyy");
            
            System.out.print(fm.format(c1.getTime())+" is ");
            if(c1.before(c2)){
                
                return com.cloudone.hotels.commons.Date.DATE_LESS_THAN;
            }else if(c1.after(c2)){
                return com.cloudone.hotels.commons.Date.DATE_GREATER;
            }else if(c1.equals(c2)){
                return com.cloudone.hotels.commons.Date.DATE_EQUAL;
            }
            return -2;
        }
	public static String CovertToParseableInt(String var)
        {
           return var.substring(1,2); 
        }
        //Utility Method to find whether an Year is a Leap year or Not
        public static boolean isLeapYear(int year){
            if((year%100 != 0) || (year%400 == 0)){
                return true;
            }
            return false;
        }
        public static boolean between(Date date, Date dateStart, Date dateEnd) {
       if (date != null && dateStart != null && dateEnd != null) {
        if (date.after(dateStart) && date.before(dateEnd)) {
            return true;
        }
        else {
            return false;
        }
    }
    return false;
}
   public static boolean betweenEx(Date date, Date dateStart, Date dateEnd) {
       if (date != null && dateStart != null && dateEnd != null) {
        if ((date.after(dateStart)||date.equals(dateStart)) && (date.before(dateEnd)|| date.equals(dateEnd))) {
            return true;
        }
        else {
            return false;
        }
    }
    return false;
}
public static boolean IsBefore(Date date,Date dateStart)
{
   if(date.before(dateStart))
     {
       return true;
     }
   
     return false;
   
}
public static boolean IsToday(Date todaysdate,Date date)
{
   if(todaysdate.equals(date))
   {
     return true;
   }
  return false;
}

    
}

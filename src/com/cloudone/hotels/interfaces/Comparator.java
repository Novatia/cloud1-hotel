/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.interfaces;

import javax.swing.JTable;

/**
 *
 * @author Akala G56
 */
public interface Comparator {
   public abstract boolean shouldHighlight (JTable tbl,Object value, int row, int column);
   public String getState();
   public void setState(String str);
}


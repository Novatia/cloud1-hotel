/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.interfaces;

/**
 *
 * @author Akala G56
 */
public interface TableStateListener {
    public void register(Observer o);
    public void unregister(Observer o);
    public void notifyObserver();
    public void notifydashboardObserver();
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cloudone.hotels.interfaces;

import com.cloudone.hotels.interfaces.TableStateListener;
import java.util.ArrayList;

/**
 *
 * @author Akala G56
 */
public class TableStateController implements TableStateListener{

    ArrayList<Observer> observers;
    
    public TableStateController(){
    
      observers=new ArrayList<Observer>();
    }
    @Override
    public void register(Observer o) {
        observers.add(o);
    }

    @Override
    public void unregister(Observer o) {
        int observerIndex= observers.indexOf(o);
        observers.remove(observerIndex);
    }

    @Override
    public void notifyObserver() {
     try
     {
        for(Observer o: observers)
        {
           try
         {  
           o.update();
           }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage()); 
        }
       
        }
     }catch(Exception exp){com.cloudone.hotels.commons.Logging.logToFile(exp.getMessage());}
    }
    public void notifydashboardObserver(){
        Observer o=observers.get(1);
        o.update();
    }
    
}
